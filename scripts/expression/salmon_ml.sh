#!/bin/bash
#SBATCH --job-name=salmon_ml
#SBATCH -c 8
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-7
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o salmon_ml_%A_%a.out
#SBATCH -e salmon_ml_%A_%a.err

export PATH=$PATH:~/local/apps/salmon-latest_linux_x86_64/bin/

file=(trimmed_LF10_10FB_ATCACG_L003 trimmed_LF10_15FB_CGATGT_L003 trimmed_LF10_15mm_Corolla trimmed_LF10_15NG_GTGAAA_L001 trimmed_LF10_15PL_GTCCGC_L001 trimmed_LF10_20FB_TTAGGC_L003 trimmed_LF10_2FB_ACTGAT_L002 trimmed_LF10L_CGATGT_L008)

dir=/home/CAM/qlin/LF10_Genome/10_RNAseq_reads_trimming/t1

salmon quant -i LF10_index -l A \
         -1 $dir/${file[$SLURM_ARRAY_TASK_ID]}_1_paired.fq.gz \
         -2 $dir/${file[$SLURM_ARRAY_TASK_ID]}_2_paired.fq.gz \
         -p 8 --gcBias --validateMappings --seqBias -o ${file[$SLURM_ARRAY_TASK_ID]}_quant


