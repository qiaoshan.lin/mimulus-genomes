#!/bin/bash
#SBATCH --job-name=omega
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=5G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

orthogroup=/home/CAM/qlin/Mimulus_Genomes/results/orthofinder_2/Results_Jun18/Orthogroups.csv

while read line; do line+=($line); done < /home/CAM/qlin/Mimulus_Genomes/results/orthofinder_2/Results_Jun18/SingleCopyOrthogroups.txt

cd ~/Mimulus_Genomes/results/selection/

for og in ${line[@]}
do
	grep $og $orthogroup >> og.tmp
	grep 'omega' ~/Mimulus_Genomes/results/selection/codeml-M0/$og\/$og\-M0.mlc|awk '{print $4}' >> omega.tmp
done

paste omega.tmp og.tmp > omega.txt
rm omega.tmp og.tmp




