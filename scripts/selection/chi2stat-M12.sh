#!/bin/bash
#SBATCH --job-name=chi2stat-M12
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=5G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load paml

while read line; do line+=($line); done < /home/CAM/qlin/Mimulus_Genomes/results/orthofinder_2/Results_Jun18/SingleCopyOrthogroups.txt

cd ~/Mimulus_Genomes/results/selection/

for og in ${line[@]}
do
	s=`grep 'lnL' ~/Mimulus_Genomes/results/selection/codeml-M12/$og\/$og\-M12.mlc|awk '{print $5}'|awk 'NR == 1{null=$0} NR == 2 {s=($0-null)*2} END {print s}'`
	prob=`chi2 2 $s | grep 'prob'|awk '{print $6}'`
	echo "$og $prob"|tr ' ' '\t' >> chi2stat-M12.txt
done

awk '$2<0.05{print $1}' chi2stat-M12.txt > chi2stat-M12_p05.tmp

orthogroup=/home/CAM/qlin/Mimulus_Genomes/results/orthofinder_2/Results_Jun18/Orthogroups.csv

while read line; do   array+=($line);   done < chi2stat-M12_p05.tmp
for og in ${array[@]}
do
	grep $og $orthogroup >> chi2stat-M12_p05.txt
done

cut -f2 chi2stat-M12_p05.txt |cut -f1 -d '.' > chi2stat-M12_p05_MLgenes.txt

rm chi2stat-M12_p05.tmp

