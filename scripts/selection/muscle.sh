#!/bin/bash
#SBATCH --job-name=muscle
#SBATCH -a 0-545
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=1G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o muscle_%A_%a.out
#SBATCH -e muscle_%A_%a.err

module load muscle
module load seqkit
module load samtools

# run these commands before submitting the script
# mkdir /home/CAM/qlin/Mimulus_Genomes/results/selection
# cd /home/CAM/qlin/Mimulus_Genomes/results/selection
# the last line of SingleCopyOrthogroups.txt doesn't have a regular end, need to open the file and add a 'return' to the end
# split -l 20 /home/CAM/qlin/Mimulus_Genomes/results/orthofinder_2/Results_Jun18/SingleCopyOrthogroups.txt singleOG
# mgcds=/home/CAM/qlin/resource/Mgut/v2.0/annotation/Mguttatus_256_v2.0.cds.fa
# sed -r 's/ pacid=.+$/.p/' $mgcds > mg.cds.fa

cd /home/CAM/qlin/Mimulus_Genomes/results/selection

orthogroup=/home/CAM/qlin/Mimulus_Genomes/results/orthofinder_2/Results_Jun18/Orthogroups.csv
mcpro=/home/CAM/qlin/resource/CE10/CE10g_v2.0.protein.fa
mppro=/home/CAM/qlin/resource/Mpar/Mparg_v2.0.protein.fa
mlpro=/home/CAM/qlin/resource/LF10/LF10g_v2.0.protein.fa
mvpro=/home/CAM/qlin/resource/MvBL/MvBLg_v2.0.protein.fa
mgpro=/home/CAM/qlin/resource/Mgut/v2.0/annotation/Mguttatus_256_v2.0.protein.fa

mccds=/home/CAM/qlin/resource/CE10/CE10g_v2.0.codingseq.fa
mpcds=/home/CAM/qlin/resource/Mpar/Mparg_v2.0.codingseq.fa
mlcds=/home/CAM/qlin/resource/LF10/LF10g_v2.0.codingseq.fa
mvcds=/home/CAM/qlin/resource/MvBL/MvBLg_v2.0.codingseq.fa
mgcds=mg.cds.fa

file=(singleOG*)
while read line; do   array+=($line);   done < ${file[$SLURM_ARRAY_TASK_ID]}
for og in ${array[@]}
do
	mc=`grep $og $orthogroup | cut -f2`
	ml=`grep $og $orthogroup | cut -f3`
	mg=`grep $og $orthogroup | cut -f4`
	mp=`grep $og $orthogroup | cut -f5`
	mv=`grep $og $orthogroup | cut -f6 | sed -r 's/\s*$//'`

	samtools faidx $mlpro $ml >> $og\.prot.fa
	samtools faidx $mcpro $mc >> $og\.prot.fa
	samtools faidx $mppro $mp >> $og\.prot.fa
	samtools faidx $mvpro $mv >> $og\.prot.fa
	samtools faidx $mgpro $mg >> $og\.prot.fa
	muscle -in $og\.prot.fa -out $og\.prot.aln

	samtools faidx $mlcds $ml >> $og\.nuc.fa
	samtools faidx $mccds $mc >> $og\.nuc.fa 
	samtools faidx $mpcds $mp >> $og\.nuc.fa
	samtools faidx $mvcds $mv >> $og\.nuc.fa 
	samtools faidx $mgcds $mg >> $og\.nuc.fa 

	/isg/shared/apps/seqkit/0.10.0/seqkit sort -n $og\.prot.aln -o $og\.prot.sort.aln
	/isg/shared/apps/seqkit/0.10.0/seqkit sort -n $og\.nuc.fa -o $og\.nuc.sort.fa

	perl ~/local/apps/pal2nal.v14/pal2nal.pl $og\.prot.sort.aln $og\.nuc.sort.fa -nogap -output paml > $og\.paml
	perl ~/local/apps/pal2nal.v14/pal2nal.pl $og\.prot.sort.aln $og\.nuc.sort.fa -nogap -output fasta > $og\.fa

	rm $og\.prot.fa $og\.prot.aln $og\.nuc.fa $og\.prot.sort.aln $og\.nuc.sort.fa
done

#rm mg.cds.fa*

