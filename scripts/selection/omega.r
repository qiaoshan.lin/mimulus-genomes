library(ggplot2)
dir<-"/Volumes/GoogleDrive/My Drive/Qiaoshan/Genome_assembly_annotation/selection/"
setwd(dir)

myTheme1 = theme(
  plot.tag = element_text(size = rel(1.3)),
  panel.grid = element_blank(),
  axis.line = element_line(colour = "black"), 
  panel.background = element_blank(),
  axis.text = element_text(size = rel(1.2)), 
  axis.text.y = element_text(angle = 45, hjust = 0.5),
  axis.title = element_text(size = rel(1.2)),
  legend.text = element_text(size = rel(1.2)), 
  legend.title = element_text(size = rel(1.3))
)

omega<-read.table(file = "omega.txt", sep = "\t", header = FALSE)

omegaFig<-ggplot(data = omega)+
  geom_histogram(aes(V1), binwidth = 0.01, color="black", fill="white")+
  xlab("dN/dS")+
  myTheme1

ggsave(filename = "omega.png", plot = omegaFig, device = png(), width = 15, height = 10, units = "cm")
