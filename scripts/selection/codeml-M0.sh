#!/bin/bash
#SBATCH --job-name=codeml-M0
#SBATCH -a 0-545
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=2G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load paml

#mkdir /home/CAM/qlin/Mimulus_Genomes/results/selection/codeml-M0
cd /home/CAM/qlin/Mimulus_Genomes/results/selection/codeml-M0
file=(../singleOG*)
while read line; do   array+=($line);   done < ${file[$SLURM_ARRAY_TASK_ID]}
for og in ${array[@]}
do
	mkdir $og
	cd $og
	sed "s/orthogroup/$og/" /home/CAM/qlin/Mimulus_Genomes/scripts/selection/codeml-M0_template.ctl > $og\-M0.ctl
	codeml $og\-M0.ctl
	rm $og\-M0.ctl
	cd ..
done


