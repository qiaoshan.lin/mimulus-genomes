library(clusterProfiler)
library(export)
dir<-"/Volumes/GoogleDrive/My Drive/Qiaoshan/Genome_assembly_annotation/selection/"
setwd(dir)

go <- read.table("GO_for_clusterProfiler.txt",sep="\t",quote="")
kegg <- read.table("KEGG_for_clusterProfiler.txt",sep="\t",quote="")
gene <- read.table("gene_for_clusterProfiler.txt")

go_term2gene <- data.frame(go$V2,go$V1)
go_term2name <- data.frame(go$V2,go$V3)
names(go_term2gene) <- c("go_term","gene")
names(go_term2name) <- c("go_term","name")

kegg_term2gene <- data.frame(kegg$V3,kegg$V1)
kegg_term2name <- data.frame(kegg$V3,kegg$V2)
names(kegg_term2gene) <- c("ko_term","gene")
names(kegg_term2name) <- c("ko_term","name")

gene <- as.vector(gene$V1)

gene2test <- read.table("chi2stat-M12_p05_MLgenes.txt")# gene IDs without annotation
gene2test <- as.vector(gene2test$V1)

#################
# GO enrichment #
#################
go_enrich <- enricher(gene=gene2test,pvalueCutoff = 0.05,pAdjustMethod = "BH",TERM2GENE = go_term2gene,TERM2NAME = go_term2name)
head(as.data.frame(go_enrich))
# write.csv(as.data.frame(go_enrich),"GO_enrichment_early.csv",row.names = F)

barplot(go_enrich, showCategory=30)
# dev.copy(png, paste0(outputPrefix, "-GOenrich_barplot_early.png"))
# dev.off()
dotplot(go_enrich)
## categorySize can be scaled by 'pvalue' or 'geneNum'
cnetplot(go_enrich, categorySize="pvalue")
# graph2ppt(file=paste0(outputPrefix, "-GOenrich-cnetplot-early.png"),width=10,height=10)

###################
# KEGG enrichment #
###################
kegg_enrich <- enricher(gene=gene2test,pvalueCutoff = 0.05,pAdjustMethod = "BH",TERM2GENE = kegg_term2gene,TERM2NAME = kegg_term2name)
head(as.data.frame(kegg_enrich))
write.csv(as.data.frame(kegg_enrich),"KEGG_enrichment_p05.csv",row.names = F)

barplot(kegg_enrich, showCategory=50)
# dev.copy(png, paste0(outputPrefix, "-KEGGenrich_barplot_early.png"))
# dev.off()
dotplot(kegg_enrich)
## categorySize can be scaled by 'pvalue' or 'geneNum'
cnetplot(kegg_enrich, categorySize="pvalue")
graph2ppt(file="KEGG_enrichment_cnetplot_p05.png",width=8,height=8)
emapplot(kegg_enrich)
# dev.copy(png, paste0(outputPrefix, "-KEGGenrich_emapplot_early.png"))
# dev.off()
