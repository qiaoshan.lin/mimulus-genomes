#!/bin/bash
#SBATCH --job-name=orthofinder_t3
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load OrthoFinder/2.2.7

workingdir=~/Mimulus_Genomes/results/orthofinder/t3
mkdir $workingdir
cp ~/Mimulus_Genomes/results/annotation/????/????g_v2.0beta.longest_isoform.protein.fa $workingdir/
cp ~/resource/Mgut/v2.0/annotation/Mguttatus_256_v2.0.protein_primaryTranscriptOnly.fa $workingdir/
cp ~/resource/Amajus/Amajus.IGDBv3.pros.long.fasta $workingdir/
cp ~/resource/Athaliana/TAIR10_pep_20110103_representative_gene_model.fasta $workingdir/
cp ~/resource/tomato/ITAG4.0_proteins.fasta $workingdir/

/isg/shared/apps/OrthoFinder/2.2.7/orthofinder -f $workingdir -t 16 -a 4


