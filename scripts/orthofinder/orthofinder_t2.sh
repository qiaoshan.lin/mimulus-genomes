#!/bin/bash
#SBATCH --job-name=orthofinder_t2
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load OrthoFinder/2.2.7

workingdir=~/Mimulus_Genomes/results/orthofinder/t2
mkdir $workingdir
cp ~/Mimulus_Genomes/results/annotation/????/????g_v2.0.longest_isoform.protein.fa $workingdir/
cp ~/resource/Mgut/v2.0/annotation/Mguttatus_256_v2.0.protein_primaryTranscriptOnly.fa $workingdir/

/isg/shared/apps/OrthoFinder/2.2.7/orthofinder -f $workingdir -t 16

awk '$2>0{print $1}' $workingdir/Results*/Orthogroups.GeneCount.csv |grep '^OG' > $workingdir/CE10.orthogroups.txt
awk '$3>0{print $1}' $workingdir/Results*/Orthogroups.GeneCount.csv |grep '^OG' > $workingdir/LF10.orthogroups.txt
awk '$4>0{print $1}' $workingdir/Results*/Orthogroups.GeneCount.csv |grep '^OG' > $workingdir/Mgut.orthogroups.txt
awk '$5>0{print $1}' $workingdir/Results*/Orthogroups.GeneCount.csv |grep '^OG' > $workingdir/Mpar.orthogroups.txt
awk '$6>0{print $1}' $workingdir/Results*/Orthogroups.GeneCount.csv |grep '^OG' > $workingdir/MvBL.orthogroups.txt


