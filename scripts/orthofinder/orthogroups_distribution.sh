cd ~/Mimulus_Genomes/results/orthofinder/
GeneCount=/home/CAM/qlin/Mimulus_Genomes/results/orthofinder/t1/Results*/Orthogroups.GeneCount.csv

perl -e '{print "Species\torthologNum\torthogroupNum\n"}' > orthogroups_distribution.txt
cat $GeneCount|awk '$3>1&&$2*$4*$5==1{print}'|cut -f3|sort|uniq -c|awk '{print "LF10\t"$2"\t"$1}' >> orthogroups_distribution.txt
cat $GeneCount|awk '$2>1&&$4*$3*$5==1{print}'|cut -f2|sort|uniq -c|awk '{print "CE10\t"$2"\t"$1}' >> orthogroups_distribution.txt
cat $GeneCount|awk '$4>1&&$2*$3*$5==1{print}'|cut -f4|sort|uniq -c|awk '{print "Mpar\t"$2"\t"$1}' >> orthogroups_distribution.txt
cat $GeneCount|awk '$5>1&&$2*$3*$4==1{print}'|cut -f5|sort|uniq -c|awk '{print "MvBL\t"$2"\t"$1}' >> orthogroups_distribution.txt


