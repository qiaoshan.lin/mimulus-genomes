#!/bin/bash
#SBATCH --job-name=orthofinder
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

module load OrthoFinder/2.2.7

workingdir=~/Mimulus_Genomes/results/orthofinder/t1
mkdir $workingdir
cp ~/Mimulus_Genomes/results/annotation/????/????g_v2.0beta.longest_isoform.protein.fa $workingdir/

/isg/shared/apps/OrthoFinder/2.2.7/orthofinder -f $workingdir -t 16

awk '$2>0{print $1}' $workingdir/Results*/Orthogroups.GeneCount.csv |grep '^OG' > $workingdir/CE10.orthogroups.txt
awk '$3>0{print $1}' $workingdir/Results*/Orthogroups.GeneCount.csv |grep '^OG' > $workingdir/LF10.orthogroups.txt
awk '$4>0{print $1}' $workingdir/Results*/Orthogroups.GeneCount.csv |grep '^OG' > $workingdir/Mpar.orthogroups.txt
awk '$5>0{print $1}' $workingdir/Results*/Orthogroups.GeneCount.csv |grep '^OG' > $workingdir/MvBL.orthogroups.txt


