library(VennDiagram)
dir <- "/Volumes/GoogleDrive/My Drive/Qiaoshan/Mimulus_Genomes/orthofinder/orthofinder_1/"
setwd(dir)
A<-read.table(file = "LF10.orthogroups.txt",header = FALSE)
B<-read.table(file = "CE10.orthogroups.txt",header = FALSE)
C<-read.table(file = "MVBL.orthogroups.txt",header = FALSE)
D<-read.table(file = "Mpar.orthogroups.txt",header = FALSE)
venn.diagram(
  x =list("M. lewisii" = A[,1],"M. parishii" = D[,1],"M. cardinalis  " = B[,1],"    M. verbenaceus" = C[,1]), 
  filename = "orthogroups.png", 
  height = 2400, 
  width= 2400, 
  resolution =600, 
  imagetype="png", 
  fontfamily = "Arial",
  cat.fontfamily = "Arial",
  cat.fontface = "italic",
  lwd=1, 
  fill =c("#FF9519","#5DC500","#C06DEB","#41A3F0"),
  cex=1, 
  cat.cex=1.1,
  margin=0.06
)

dir <- "/Volumes/GoogleDrive/My Drive/Qiaoshan/Mimulus_Genomes/orthofinder/orthofinder_2/"
setwd(dir)
A<-read.table(file = "LF10.orthogroups.txt",header = FALSE)
B<-read.table(file = "CE10.orthogroups.txt",header = FALSE)
C<-read.table(file = "MVBL.orthogroups.txt",header = FALSE)
D<-read.table(file = "Mpar.orthogroups.txt",header = FALSE)
E<-read.table(file = "Mgut.orthogroups.txt",header = FALSE)
venn.diagram(
  x =list("M. lewisii" = A[,1],"M. parishii" = D[,1],"M. cardinalis" = B[,1],"M. verbenaceus" = C[,1],"M. gutattus" = E[,1]), 
  filename = "orthogroups.png", 
  height = 3000, 
  width= 3000, 
  resolution =600, 
  imagetype="png", 
  fontfamily = "Arial",
  cat.fontfamily = "Arial",
  cat.fontface = "italic",
  lwd=1, 
  fill =c("#FF9519","#5DC500","#C06DEB","#41A3F0","coral2"),
  cex=0.6, 
  cat.cex=0.8,
  cat.dist = c(0.2,0.3,0.22,0.22,0.3),
  margin = 0.15
)

dir <- "/Volumes/GoogleDrive/My Drive/Qiaoshan/Mimulus_Genomes/orthofinder/orthofinder_4/"
setwd(dir)
A<-read.table(file = "LF10.orthogroups.txt",header = FALSE)
B<-read.table(file = "Mgut.orthogroups.txt",header = FALSE)
C<-read.table(file = "Amajus.orthogroups.txt",header = FALSE)
D<-read.table(file = "tomato.orthogroups.txt",header = FALSE)
E<-read.table(file = "Arabidopsis.orthogroups.txt",header = FALSE)
venn.diagram(
  x =list("M. lewisii" = A[,1],"A. majus" = C[,1],"S. lycopersicum" = D[,1],"A. thaliana" = E[,1],"M. gutattus" = B[,1]), 
  filename = "orthogroups.png", 
  height = 3000, 
  width= 3000, 
  resolution =600, 
  imagetype="png", 
  fontfamily = "Arial",
  cat.fontfamily = "Arial",
  cat.fontface = "italic",
  lwd=1, 
  fill =c("#FF9519","#5DC500","#C06DEB","#41A3F0","coral2"),
  cex=0.6, 
  cat.cex=0.8,
  cat.dist = c(0.2,0.3,0.22,0.22,0.3),
  margin = 0.15
)
