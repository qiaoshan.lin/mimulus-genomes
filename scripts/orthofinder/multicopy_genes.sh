cd ~/Mimulus_Genomes/results/orthofinder/

GeneCount=/home/CAM/qlin/Mimulus_Genomes/results/orthofinder/t1/Results*/Orthogroups.GeneCount.csv
Orthogroups=/home/CAM/qlin/Mimulus_Genomes/results/orthofinder/t1/Results*/Orthogroups.csv

# Notes for checking genome assembly redundency

# Method 1: use multi-copy orthologs

# 1) find the orthogroup where there is only one copy in each of the other species while more than one in the target species

awk '$2==1&&$3==1&&$5==1&&$4>1{print $1}' $GeneCount > Mpar_dup.txt
awk '$2==1&&$3==1&&$4==1&&$5>1{print $1}' $GeneCount > MvBL_dup.txt
awk '$2==1&&$4==1&&$5==1&&$3>1{print $1}' $GeneCount > LF10_dup.txt
awk '$3==1&&$4==1&&$5==1&&$2>1{print $1}' $GeneCount > CE10_dup.txt

# 2) find the genes in these orthogroups

og=()
while read line; do og+=("$line"); done < Mpar_dup.txt 
for o in ${og[@]};do grep $o $Orthogroups;done|cut -f4|grep -o -P "(M.{9}\.[0-9])" > Mpar_dup_genes.txt
og=()
while read line; do og+=("$line"); done < MvBL_dup.txt 
for o in ${og[@]};do grep $o $Orthogroups;done|cut -f5|grep -o -P "(M.{9}\.[0-9])" > MvBL_dup_genes.txt
og=()
while read line; do og+=("$line"); done < LF10_dup.txt 
for o in ${og[@]};do grep $o $Orthogroups;done|cut -f3|grep -o -P "(M.{9}\.[0-9])" > LF10_dup_genes.txt
og=()
while read line; do og+=("$line"); done < CE10_dup.txt 
for o in ${og[@]};do grep $o $Orthogroups;done|cut -f2|grep -o -P "(M.{9}\.[0-9])" > CE10_dup_genes.txt

rm ????_dup.txt

# 3) create a bed file for these genes

gene=()
while read line; do gene+=("$line"); done < Mpar_dup_genes.txt
gtf=~/resource/Mpar/Mparg_v2.0.gtf 
for g in ${gene[@]};do awk -v g="$g" '$3~/transcript/&&$10~g{print $1"\t"$4"\t"$5}' $gtf;done > Mpar_dup_genes.bed

gene=()
while read line; do gene+=("$line"); done < MvBL_dup_genes.txt
gtf=~/resource/MvBL/MvBLg_v2.0.gtf 
for g in ${gene[@]};do awk -v g="$g" '$3~/transcript/&&$10~g{print $1"\t"$4"\t"$5}' $gtf;done > MvBL_dup_genes.bed

gene=()
while read line; do gene+=("$line"); done < LF10_dup_genes.txt
gtf=~/resource/LF10/LF10g_v2.0.gtf
for g in ${gene[@]};do awk -v g="$g" '$3~/transcript/&&$10~g{print $1"\t"$4"\t"$5}' $gtf;done > LF10_dup_genes.bed

gene=()
while read line; do gene+=("$line"); done < CE10_dup_genes.txt
gtf=~/resource/CE10/CE10g_v2.0.gtf
for g in ${gene[@]};do awk -v g="$g" '$3~/transcript/&&$10~g{print $1"\t"$4"\t"$5}' $gtf;done > CE10_dup_genes.bed

paste LF10_dup_genes.bed LF10_dup_genes.txt > dup_genes.tmp
mv dup_genes.tmp LF10_dup_genes.bed
paste CE10_dup_genes.bed CE10_dup_genes.txt > dup_genes.tmp
mv dup_genes.tmp CE10_dup_genes.bed
paste Mpar_dup_genes.bed Mpar_dup_genes.txt > dup_genes.tmp
mv dup_genes.tmp Mpar_dup_genes.bed
paste MvBL_dup_genes.bed MvBL_dup_genes.txt > dup_genes.tmp
mv dup_genes.tmp MvBL_dup_genes.bed

rm ????_dup_genes.txt

