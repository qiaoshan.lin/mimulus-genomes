#!/usr/bin/perl -s

if (!$ARGV[0]) {
	print "---------------------------------------------------------------------------------\n";
	print "This script is used for finding tandem dupliacted genes from orthofinder results.\n";
	print "---------------------------------------------------------------------------------\n";
	print "\nUsage:\n";
	print "perl ~/scripts/find_tandem_dups.pl [Orthogroups.csv]\n\n";
 	exit;
} 

$input = $ARGV[0];
open IN, "$input" or die "Can't open $input: $!";

while (<IN>) {
	next if $. < 2;
	chomp($_);
	@species = split(/\t/,$_);
	@tandem = ();
	@count = ();
	for (my $s = 1; $s < scalar(@species); $s++) {
		($tandem, $count) = find_tandem($species[$s]);
		push @tandem, $tandem;
		push @count, $count;
	}
	$tandem = join "\t", @tandem;
	$count = join "\t", @count;
	print $tandem,"\n";
	print $count,"\n";
}

sub find_tandem {
	my @genes = split(/, /, @_[0]);
	my @chr = ();
	my @pos = ();
	my @tandem = ();
	for (my $g = 0; $g < scalar(@genes); $g++) {
		if ( $genes[$g] =~ /\w\w(\w)G([0-9]+)\.[0-9]/) {
			push @chr, $1;
			push @pos, $2;
		}
	}
	for (my $g = 1; $g < scalar(@genes); $g++) {
		if ( $chr[$g-1] eq $chr[$g] and abs($pos[$g]-$pos[$g-1])<500 ) {
			push @tandem, $genes[$g-1];
			push @tandem, $genes[$g];
		}else{
			next;
		}
	}
	my @uniq_tandem = uniq(@tandem);
	my $tandem = join ',', @uniq_tandem;
	return ($tandem, scalar(@uniq_tandem));
}

sub uniq {
    my %seen;
    grep !$seen{$_}++, @_;
}

close IN;

