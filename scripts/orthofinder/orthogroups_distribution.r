library(ggplot2)
library(export)
dir <- "/Volumes/GoogleDrive/My Drive/Qiaoshan/Mimulus_Genomes/orthofinder/"
setwd(dir)

p <- read.table(file = "orthogroups_distribution.txt", sep = "\t", header = TRUE)
p$Species <- factor(p$Species, levels = c("LF10","CE10","Mpar","MvBL"))
pFig <-ggplot(data = p, aes(orthologNum, orthogroupNum, color=Species))+
  geom_line()+
  theme_minimal()+
  xlab("Number of Orthologues")+
  ylab("Number of Orthogroups")+
  theme(panel.grid = element_blank(),axis.line.y = element_line(colour = "black"),axis.line.x = element_line(colour = "black"),panel.grid.major.y = element_line(color = "gray",linetype = 3))
pFig
ggsave(filename = "orthogroups_distribution.png", device = png(), width = 20, height = 10, units = "cm")
