#!/bin/bash
#SBATCH --job-name=braker2
#SBATCH -n 8
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=200G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o braker2_%j.out
#SBATCH -e braker2_%j.err

module load BRAKER/2.0.5
module unload augustus/3.2.3
PATH=/home/CAM/qlin/augustus/3.2.3/bin:/home/CAM/qlin/augustus/3.2.3/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/augustus/3.2.3/config/
export GENEMARK_PATH=/isg/shared/apps/GeneMark-ET/4.38/
export BLAST_PATH=/isg/shared/apps/blast/ncbi-blast-2.7.1+/bin/

module unload perl/5.28.1
module load perl/5.24.0
export PERL5LIB=/labs/Wegrzyn/perl5/lib/perl5/

braker.pl \
 --cores=8 \
 --softmasking 1 \
 --genome=/home/CAM/qlin/parisii_Genome/versions/chromosomes/repeatMasker_20190215_soft/Mparg_chr.fa.masked \
 --bam=/home/CAM/qlin/parisii_Genome/11_HISAT2/t2/trimmed_LF10_10FB_ATCACG_L003.sort.bam,/home/CAM/qlin/parisii_Genome/11_HISAT2/t2/trimmed_LF10_15FB_CGATGT_L003.sort.bam,/home/CAM/qlin/parisii_Genome/11_HISAT2/t2/trimmed_LF10_15mm_Corolla.sort.bam,/home/CAM/qlin/parisii_Genome/11_HISAT2/t2/trimmed_LF10_15NG_GTGAAA_L001.sort.bam,/home/CAM/qlin/parisii_Genome/11_HISAT2/t2/trimmed_LF10_15PL_GTCCGC_L001.sort.bam,/home/CAM/qlin/parisii_Genome/11_HISAT2/t2/trimmed_LF10_20FB_TTAGGC_L003.sort.bam,/home/CAM/qlin/parisii_Genome/11_HISAT2/t2/trimmed_LF10_2FB_ACTGAT_L002.sort.bam,/home/CAM/qlin/parisii_Genome/11_HISAT2/t2/trimmed_LF10_FB13A.sort.bam,/home/CAM/qlin/parisii_Genome/11_HISAT2/t2/trimmed_LF10_FB13B.sort.bam,/home/CAM/qlin/parisii_Genome/11_HISAT2/t2/trimmed_LF10_FB13C.sort.bam,/home/CAM/qlin/parisii_Genome/11_HISAT2/t2/trimmed_LF10_FB5A.sort.bam,/home/CAM/qlin/parisii_Genome/11_HISAT2/t2/trimmed_LF10_FB5B.sort.bam,/home/CAM/qlin/parisii_Genome/11_HISAT2/t2/trimmed_LF10_FB5C.sort.bam,/home/CAM/qlin/parisii_Genome/11_HISAT2/t2/trimmed_LF10L_CGATGT_L008.sort.bam \
 --prot_seq=/home/CAM/qlin/resource/guttatus/v2.0/annotation/Mguttatus_256_v2.0.protein.fa \
 --prg=gth \
 --gth2traingenes

