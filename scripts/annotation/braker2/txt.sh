#!/bin/bash
#SBATCH --job-name=txt
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-3
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

label=(LF10 CE10 Mpar MvBL)
workingDir=~/Mimulus_Genomes/results/annotation/${label[$SLURM_ARRAY_TASK_ID]}/braker/Sp*/
cd $workingDir

sed -E 's/>[a-zA-Z0-9_]+\./>/' augustus.hints.codingseq > $label.codingseq.fa
perl ~/scripts/fasta_format.pl $label.codingseq.fa tmp.fa 100
mv tmp.fa $label.codingseq.fa

grep '>' augustus.hints.aa |tr -d '>' > txt/augustus.hints.txt
grep '# % of transcript supported by hints (any source): ' augustus.hints.gff |awk '{print $10}' |paste txt/augustus.hints.txt - |awk '$2==0{print $1}' > txt/transcript_not_supported_by_hints.txt

sed -r 's/\.t[0-9]$//' txt/augustus.hints.txt |sort|uniq -c|awk '$1>1{print $2}' > txt/alternative_splicing_genes.txt
array=()
while read line; do   array+=("$line");   done < txt/alternative_splicing_genes.txt 
for a in ${array[@]}; do b=$a.t; grep $b txt/augustus.hints.txt; done > txt/alternative_splicing_transcripts.txt 

perl ~/scripts/fetchSeq4fa2.pl augustus.hints.aa txt/alternative_splicing_transcripts.txt tmp.fa
perl ~/scripts/lengthFilter4fa.pl tmp.fa 0 tmp2.fa |tr -d '>' > txt/alternative_splicing_length.txt
rm tmp*.fa

gene=()
while read line; do   gene+=("$line");   done < txt/alternative_splicing_genes.txt
for g in "${gene[@]}"; do grep -P "$g.t" txt/alternative_splicing_length.txt|sed -r ':x /.+/ {N;s/\n/ /;bx}' >> txt/alternative_splicing_length_diff.txt; done

cut -f1 -d '.' txt/augustus.hints.txt|sort -V|uniq > txt/augustus.genes.txt 

gene=()
while read line; do   gene+=("$line");   done < txt/alternative_splicing_genes.txt
for g in "${gene[@]}"
do 
	grep -P "$g.t" txt/alternative_splicing_length.txt|sort -k2 -nr|head -1|cut -f1 >> txt/alternative_splicing_transcripts_longest_isoform.txt
done

cat txt/alternative_splicing_transcripts.txt txt/augustus.hints.txt |sort|uniq -c|awk '$1==1{print $2}' > txt/no_alternative_splicing_transcripts.txt

grep 'stop_codon' augustus.hints.gtf |cut -f9|cut -f2 -d '"' > txt/transcripts_with_stop_codon.txt

grep 'start_codon' augustus.hints.gtf |cut -f9|cut -f2 -d '"' > txt/transcripts_with_start_codon.txt

cat txt/transcripts_with_start_codon.txt txt/transcripts_with_stop_codon.txt |sort|uniq -c|awk '$1==2{print $2}' > txt/transcripts_with_start_and_stop_codon.txt 

awk '$3~/intron/{print $5-$4+1}' augustus.hints.gtf |sort -n|uniq -c|awk '{print $2"\t"$1}' > txt/intron_length_distribution.txt
sed -i '1s/^/intron_length\tcount\n/' txt/intron_length_distribution.txt 

awk '$3~/exon/{print $5-$4+1}' augustus.hints.gtf |sort -n|uniq -c|awk '{print $2"\t"$1}' > txt/exon_length_distribution.txt
sed -i '1s/^/exon_length\tcount\n/' txt/exon_length_distribution.txt

perl ~/scripts/lengthFilter4fa.pl $label.codingseq.fa 0 tmp.fa |tr -d '>' > txt/transcript_length.txt
rm tmp.fa
cut -f2 txt/transcript_length.txt |sort -n|uniq -c|awk '{print $2"\t"$1}' > txt/transcript_length_distribution.txt 
sed -i '1s/^/transcript_length\tcount\n/' txt/transcript_length_distribution.txt

awk '$3~/transcript/{print $1"\t"$4"\t"$5"\t"$9}' augustus.hints.gtf > txt/transcripts.bed
bedtools coverage -a txt/transcripts.bed -b ~/$label\_Genome/circos/$label.gap.bed |awk '$5>0{print $4}' > txt/transcripts_between_contigs.txt


