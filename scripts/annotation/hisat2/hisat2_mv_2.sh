#!/bin/bash
#SBATCH --job-name=hisat2-2
#SBATCH -n 8
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o hisat2-2_%j.out
#SBATCH -e hisat2-2_%j.err

module load hisat2/2.0.5
module load samtools 

ref=/home/CAM/qlin/MVBL_Genome/versions/chromosomes/MvBLg_chr.fa

hisat2 -q --phred33 -p 8 \
 -x ./MvBLg_chr \
 -U /home/CAM/qlin/MVBL_Genome/10_RNAseq_reads_trimming/t1/trimmed_MvBL_FB10_R1_unpaired.fq.gz,/home/CAM/qlin/MVBL_Genome/trimmomatic/t1/trimmed_MvBL_FB10_R2_unpaired.fq.gz,/home/CAM/qlin/MVBL_Genome/trimmomatic/t1/trimmed_MvBL_FB16_R1_unpaired.fq.gz,/home/CAM/qlin/MVBL_Genome/trimmomatic/t1/trimmed_MvBL_FB16_R2_unpaired.fq.gz,/home/CAM/qlin/MVBL_Genome/trimmomatic/t1/trimmed_MvBL_FB30_R1_unpaired.fq.gz,/home/CAM/qlin/MVBL_Genome/trimmomatic/t1/trimmed_MvBL_FB30_R2_unpaired.fq.gz,/home/CAM/qlin/MVBL_Genome/trimmomatic/t1/trimmed_MvBL_FB5_R1_unpaired.fq.gz,/home/CAM/qlin/MVBL_Genome/trimmomatic/t1/trimmed_MvBL_FB5_R2_unpaired.fq.gz,/home/CAM/qlin/MVBL_Genome/trimmomatic/t1/trimmed_MvBL_TinyFB_R1_unpaired.fq.gz,/home/CAM/qlin/MVBL_Genome/trimmomatic/t1/trimmed_MvBL_TinyFB_R2_unpaired.fq.gz,/home/CAM/qlin/MVBL_Genome/trimmomatic/t1/trimmed_MvBL_YoungLeaf_R1_unpaired.fq.gz,/home/CAM/qlin/MVBL_Genome/trimmomatic/t1/trimmed_MvBL_YoungLeaf_R2_unpaired.fq.gz \
 -S unpaired.sam

samtools view -bST $ref -@ 8 unpaired.sam > unpaired.bam
samtools sort -o unpaired.sort.bam -T tmp -@ 8 unpaired.bam
samtools index unpaired.sort.bam


