#!/bin/bash
#SBATCH --job-name=hisat2-1
#SBATCH -c 8
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-7
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o hisat2-1_%A_%a.out
#SBATCH -e hisat2-1_%A_%a.err

module load hisat2/2.0.5
module load samtools

file=(trimmed_LF10_10FB_ATCACG_L003 trimmed_LF10_15FB_CGATGT_L003 trimmed_LF10_15mm_Corolla trimmed_LF10_15NG_GTGAAA_L001 trimmed_LF10_15PL_GTCCGC_L001 trimmed_LF10_20FB_TTAGGC_L003 trimmed_LF10_2FB_ACTGAT_L002 trimmed_LF10L_CGATGT_L008)

ref=/home/CAM/qlin/CE10_Genome/versions/chromosomes/CE10g_chr.fa

hisat2 -q --phred33 -p 8 \
 -x ./CE10g_chr \
 -1 /home/CAM/qlin/LF10_Genome/10_RNAseq_reads_trimming/t1/${file[$SLURM_ARRAY_TASK_ID]}_1_paired.fq.gz \
 -2 /home/CAM/qlin/LF10_Genome/10_RNAseq_reads_trimming/t1/${file[$SLURM_ARRAY_TASK_ID]}_2_paired.fq.gz \
 -S ${file[$SLURM_ARRAY_TASK_ID]}.sam

samtools view -bST $ref -@ 8 ${file[$SLURM_ARRAY_TASK_ID]}.sam > ${file[$SLURM_ARRAY_TASK_ID]}.bam
samtools sort -o ${file[$SLURM_ARRAY_TASK_ID]}.sort.bam -T ${file[$SLURM_ARRAY_TASK_ID]}_tmp -O bam -@ 8 ${file[$SLURM_ARRAY_TASK_ID]}.bam
samtools index ${file[$SLURM_ARRAY_TASK_ID]}.sort.bam


