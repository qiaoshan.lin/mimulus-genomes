#!/bin/bash
#SBATCH --job-name=hisat2-1
#SBATCH -c 8
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-5
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o hisat2-1_%A_%a.out
#SBATCH -e hisat2-1_%A_%a.err

module load hisat2/2.0.5
module load samtools

file=(trimmed_MvBL_FB10 trimmed_MvBL_FB16 trimmed_MvBL_FB30 trimmed_MvBL_FB5 trimmed_MvBL_TinyFB trimmed_MvBL_YoungLeaf)

ref=/home/CAM/qlin/MVBL_Genome/versions/chromosomes/MvBLg_chr.fa

hisat2 -q --phred33 -p 8 \
 -x ./MvBLg_chr \
 -1 /home/CAM/qlin/MVBL_Genome/10_RNAseq_reads_trimming/t1/${file[$SLURM_ARRAY_TASK_ID]}_R1_paired.fq.gz \
 -2 /home/CAM/qlin/MVBL_Genome/10_RNAseq_reads_trimming/t1/${file[$SLURM_ARRAY_TASK_ID]}_R2_paired.fq.gz \
 -S ${file[$SLURM_ARRAY_TASK_ID]}.sam

samtools view -bST $ref -@ 8 ${file[$SLURM_ARRAY_TASK_ID]}.sam > ${file[$SLURM_ARRAY_TASK_ID]}.bam
samtools sort -o ${file[$SLURM_ARRAY_TASK_ID]}.sort.bam -T ${file[$SLURM_ARRAY_TASK_ID]}_tmp -O bam -@ 8 ${file[$SLURM_ARRAY_TASK_ID]}.bam
samtools index ${file[$SLURM_ARRAY_TASK_ID]}.sort.bam



