#!/bin/bash
#SBATCH --job-name=index
#SBATCH -c 8 
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-3 
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o genome_index_%A_%a.out
#SBATCH -e genome_index_%A_%a.err

module load hisat2/2.0.5

mc=/home/CAM/qlin/resource/CE10/CE10g_v2.0.fa 
mp=/home/CAM/qlin/resource/Mpar/Mparg_v2.0.fa
ml=/home/CAM/qlin/resource/LF10/LF10g_v2.0.fa
mv=/home/CAM/qlin/resource/MvBL/MvBLg_v2.0.fa

ref=($ml $mc $mp $mv)
label=(LF10 CE10 Mpar MvBL)

workingDir=~/Mimulus_Genomes/results/annotation

cd $workingDir/${label[$SLURM_ARRAY_TASK_ID]}

hisat2-build -f -p 8 ${ref[$SLURM_ARRAY_TASK_ID]} ${label[$SLURM_ARRAY_TASK_ID]}


