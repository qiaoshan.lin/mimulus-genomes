#!/bin/bash
#SBATCH --job-name=hisat2-2
#SBATCH -c 8
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-5
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o hisat2-2_%A_%a.out
#SBATCH -e hisat2-2_%A_%a.err

module load hisat2/2.0.5
module load samtools

file=(trimmed_LF10_FB13A trimmed_LF10_FB13B trimmed_LF10_FB13C trimmed_LF10_FB5A trimmed_LF10_FB5B trimmed_LF10_FB5C)

ref=/home/CAM/qlin/parisii_Genome/versions/chromosomes/Mparg_chr.fa

hisat2 -q --phred33 -p 8 \
 -x ./Mparg_chr \
 -1 /home/CAM/qlin/RCP1_OX_Analysis/trimmomatic/t3/${file[$SLURM_ARRAY_TASK_ID]}_1_paired.fq.gz \
 -2 /home/CAM/qlin/RCP1_OX_Analysis/trimmomatic/t3/${file[$SLURM_ARRAY_TASK_ID]}_2_paired.fq.gz \
 -S ${file[$SLURM_ARRAY_TASK_ID]}.sam

samtools view -bST $ref -@ 8 ${file[$SLURM_ARRAY_TASK_ID]}.sam > ${file[$SLURM_ARRAY_TASK_ID]}.bam
samtools sort -o ${file[$SLURM_ARRAY_TASK_ID]}.sort.bam -T ${file[$SLURM_ARRAY_TASK_ID]}_tmp -O bam -@ 8 ${file[$SLURM_ARRAY_TASK_ID]}.bam
samtools index ${file[$SLURM_ARRAY_TASK_ID]}.sort.bam


