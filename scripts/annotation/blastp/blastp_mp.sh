#!/bin/bash
#SBATCH --job-name=blastp_mp
#SBATCH -a 0-92
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=1G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load blast
module load samtools 

#run before submit the job:
#cd ~/Mimulus_Genomes/results/annotation/Mpar/blastp
#grep '>' /home/CAM/qlin/Mimulus_Genomes/results/annotation/Mpar/Mparg_v2.0beta.longest_isoform.protein.fa|tr -d '>' > Mparg_v2.0beta.longest_isoform.txt
#split -l 300 Mparg_v2.0beta.longest_isoform.txt prot
#modify the array number based on the number of prot file
#cat /home/CAM/qlin/resource/Mgut/NONTOL_v4.0/MguttatusNONTOL_553_v4.0.protein.fa /home/CAM/qlin/resource/Mgut/TOL_v5.0/MguttatusTOL_551_v5.0.protein.fa > Mguttatus.protein.fa

cd ~/Mimulus_Genomes/results/annotation/Mpar/blastp

query=/home/CAM/qlin/Mimulus_Genomes/results/annotation/Mpar/Mparg_v2.0beta.longest_isoform.protein.fa
subject=Mguttatus.protein.fa

file=(prot*)
readarray -t protein < ${file[$SLURM_ARRAY_TASK_ID]}
for p in "${protein[@]}"
do
	samtools faidx $query $p > tmp.$SLURM_ARRAY_TASK_ID.fa
	/isg/shared/apps/blast/ncbi-blast-2.7.1+/bin/blastp -subject $subject -query tmp.$SLURM_ARRAY_TASK_ID.fa -outfmt "6 qseqid sseqid pident" |head -1 >> Mparg_v2.0.protein.blastp_best_hit.$SLURM_ARRAY_TASK_ID.txt
done

rm tmp.$SLURM_ARRAY_TASK_ID.fa

#run after the job:
#cat Mparg_v2.0.protein.blastp_best_hit.*.txt |sort -k1,1 > Mparg_v2.0.protein.blastp_best_hit.txt
#rm Mparg_v2.0.protein.blastp_best_hit.*.txt
#awk '$3>=35{print $1,$2}' OFS="\t" Mparg_v2.0.protein.blastp_best_hit.txt > Mparg_v2.0.protein.blastp_best_hit.flt.txt

#add annotations based on guttatus's annotations
#cat /home/CAM/qlin/resource/Mgut/NONTOL_v4.0/MguttatusNONTOL_553_v4.0.annotation_info.txt /home/CAM/qlin/resource/Mgut/TOL_v5.0/MguttatusTOL_551_v5.0.annotation_info.txt|cut -f4,7-11|sort -k1,1|join -1 2 -2 1 -t $'\t' -o 1.1,1.2,2.6,2.2,2.3,2.4,2.5 <(sort -k2,2 Mparg_v2.0.protein.blastp_best_hit.flt.txt) - | perl -lane '@_=split /\t/,$_; $_[2]=~s/\s*/NA/g if $_[2]=~/^\s*$/; $_=join "\t",@_; print' |sort -k1 > Mparg_v2.0.protein.annotation.tmp1

#add Arabidopsis gene names based on gene ID in guttatus's annotations
#grep '>'  ~/resource/Athaliana/TAIR10_pep_20110103_representative_gene_model.fasta | perl -lane '@_=/^>(.+?)\..+Symbols:(.*?)\|/;$_[1]=~s/\s//g;$_[1]=~/^$/?print "$_[0]\tNA":print "$_[0]\t$_[1]"' | sort -k1,1 | join -1 3 -2 1 -a 1 -t $'\t' -o 1.1,1.2,1.3,2.2,1.4,1.5,1.6,1.7 <(sort -k3,3 Mparg_v2.0.protein.annotation.tmp1) - |sort -k1 > Mparg_v2.0.protein.annotation.tmp2

#modify the known_gene_map.txt file
#cut -f1 -d '.' Mparg_v2.0.protein.annotation.tmp2 > tmp
#paste tmp Mparg_v2.0.protein.annotation.tmp2 | join -j 1 -t $'\t' -a 1 -o 1.2,1.3,1.4,1.5,2.2,1.6,1.7,1.8,1.9 <(sort -k1,1 -) <(sort -k1,1 ~/resource/Mpar/known_gene_map.txt) > Mparg_v2.0.protein.annotation.tmp3

#edit the header file
#cat header Mparg_v2.0.protein.annotation.tmp3 > Mparg_v2.0.protein.annotation.txt
#rm Mparg_v2.0.protein.annotation.tmp* tmp

#prepare files for clusterProfiler analysis
#perl -wnE '@F=split(/\t/,$_);$F[0] =~ s/\.\d$//;while (/(GO:\d{7})/g){print "$F[0]\t$1\n"}' Mparg_v2.0.protein.annotation.txt | sort -k2,2 | join -a 1 -1 2 -2 1 -t $'\t' -o 1.1,1.2,2.2 - <(sort -k1,1 ~/resource/go.txt) |sort -k1 > Mparg_v2.0.GO.txt
#perl -wnE '@F=split(/\t/,$_);$F[0] =~ s/\.\d$//;while (/(K\d{5})/g){print "$F[0]\t$1\n"}' Mparg_v2.0.protein.annotation.txt > Mparg_v2.0.KO.txt
#perl -wnE '@F=split(/\t/,$_);$F[0] =~ s/\.\d$//;while (/(KOG\d{4})/g){print "$F[0]\t$1\n"}' Mparg_v2.0.protein.annotation.txt > Mparg_v2.0.KOG.txt




