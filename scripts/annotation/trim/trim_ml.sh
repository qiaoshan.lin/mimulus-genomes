#!/bin/bash
#SBATCH --job-name=trim_ml
#SBATCH -n 8
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o t1_%j.out
#SBATCH -e t1_%j.err

module load Trimmomatic/0.36

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10_10FB_ATCACG_L003.txt \
 ../../../Mimulus_raw_data/Transcriptome/LF10/LF10_10FB_ATCACG_L003_R1_001.fastq.gz ../../../Mimulus_raw_data/Transcriptome/LF10/LF10_10FB_ATCACG_L003_R2_001.fastq.gz \
 trimmed_LF10_10FB_ATCACG_L003_1_paired.fq.gz trimmed_LF10_10FB_ATCACG_L003_1_unpaired.fq.gz \
 trimmed_LF10_10FB_ATCACG_L003_2_paired.fq.gz trimmed_LF10_10FB_ATCACG_L003_2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:25 \
 MINLEN:50

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10_15FB_CGATGT_L003.txt \
 ../../../Mimulus_raw_data/Transcriptome/LF10/LF10_15FB_CGATGT_L003_R1_001.fastq.gz ../../../Mimulus_raw_data/Transcriptome/LF10/LF10_15FB_CGATGT_L003_R2_001.fastq.gz \
 trimmed_LF10_15FB_CGATGT_L003_1_paired.fq.gz trimmed_LF10_15FB_CGATGT_L003_1_unpaired.fq.gz \
 trimmed_LF10_15FB_CGATGT_L003_2_paired.fq.gz trimmed_LF10_15FB_CGATGT_L003_2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:25 \
 MINLEN:50

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10_15mm_Corolla.txt \
 ../../../Mimulus_raw_data/Transcriptome/LF10/LF10_15mm_Corolla_R1_001.fastq.gz ../../../Mimulus_raw_data/Transcriptome/LF10/LF10_15mm_Corolla_R2_001.fastq.gz \
 trimmed_LF10_15mm_Corolla_1_paired.fq.gz trimmed_LF10_15mm_Corolla_1_unpaired.fq.gz \
 trimmed_LF10_15mm_Corolla_2_paired.fq.gz trimmed_LF10_15mm_Corolla_2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:25 \
 MINLEN:50

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10_15NG_GTGAAA_L001.txt \
 ../../../Mimulus_raw_data/Transcriptome/LF10/LF10_15NG_GTGAAA_L001_R1_001.fastq.gz ../../../Mimulus_raw_data/Transcriptome/LF10/LF10_15NG_GTGAAA_L001_R2_001.fastq.gz \
 trimmed_LF10_15NG_GTGAAA_L001_1_paired.fq.gz trimmed_LF10_15NG_GTGAAA_L001_1_unpaired.fq.gz \
 trimmed_LF10_15NG_GTGAAA_L001_2_paired.fq.gz trimmed_LF10_15NG_GTGAAA_L001_2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:25 \
 MINLEN:50 

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10_15PL_GTCCGC_L001.txt \
 ../../../Mimulus_raw_data/Transcriptome/LF10/LF10_15PL_GTCCGC_L001_R1_001.fastq.gz ../../../Mimulus_raw_data/Transcriptome/LF10/LF10_15PL_GTCCGC_L001_R2_001.fastq.gz \
 trimmed_LF10_15PL_GTCCGC_L001_1_paired.fq.gz trimmed_LF10_15PL_GTCCGC_L001_1_unpaired.fq.gz \
 trimmed_LF10_15PL_GTCCGC_L001_2_paired.fq.gz trimmed_LF10_15PL_GTCCGC_L001_2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:25 \
 MINLEN:50

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10_20FB_TTAGGC_L003.txt \
 ../../../Mimulus_raw_data/Transcriptome/LF10/LF10_20FB_TTAGGC_L003_R1_001.fastq.gz ../../../Mimulus_raw_data/Transcriptome/LF10/LF10_20FB_TTAGGC_L003_R2_001.fastq.gz \
 trimmed_LF10_20FB_TTAGGC_L003_1_paired.fq.gz trimmed_LF10_20FB_TTAGGC_L003_1_unpaired.fq.gz \
 trimmed_LF10_20FB_TTAGGC_L003_2_paired.fq.gz trimmed_LF10_20FB_TTAGGC_L003_2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:25 \
 MINLEN:50 

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10_2FB_ACTGAT_L002.txt \
 ../../../Mimulus_raw_data/Transcriptome/LF10/LF10_2FB_ACTGAT_L002_R1_001.fastq.gz ../../../Mimulus_raw_data/Transcriptome/LF10/LF10_2FB_ACTGAT_L002_R2_001.fastq.gz \
 trimmed_LF10_2FB_ACTGAT_L002_1_paired.fq.gz trimmed_LF10_2FB_ACTGAT_L002_1_unpaired.fq.gz \
 trimmed_LF10_2FB_ACTGAT_L002_2_paired.fq.gz trimmed_LF10_2FB_ACTGAT_L002_2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:25 \
 MINLEN:50

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10_5FB_ATTCCT_L002.txt \ 
 ../../../Mimulus_raw_data/Transcriptome/LF10/LF10_5FB_ATTCCT_L002_R1_001.fastq.gz ../../../Mimulus_raw_data/Transcriptome/LF10/LF10_5FB_ATTCCT_L002_R2_001.fastq.gz \
 trimmed_LF10_5FB_ATTCCT_L002_1_paired.fq.gz trimmed_LF10_5FB_ATTCCT_L002_1_unpaired.fq.gz \
 trimmed_LF10_5FB_ATTCCT_L002_2_paired.fq.gz trimmed_LF10_5FB_ATTCCT_L002_2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:25 \
 MINLEN:50 

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10L_CGATGT_L008.txt \
 ../../../Mimulus_raw_data/Transcriptome/LF10/LF10L_CGATGT_L008_R1_001.fastq.gz ../../../Mimulus_raw_data/Transcriptome/LF10/LF10L_CGATGT_L008_R2_001.fastq.gz \
 trimmed_LF10L_CGATGT_L008_1_paired.fq.gz trimmed_LF10L_CGATGT_L008_1_unpaired.fq.gz \
 trimmed_LF10L_CGATGT_L008_2_paired.fq.gz trimmed_LF10L_CGATGT_L008_2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:25 \
 MINLEN:50

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10-5-6mm-FB-A_S11_L001.txt \
 ../../../Mimulus_raw_data/Transcriptome/LF10/LF10_FB5A_replicate/LF10-5-6mm-FB-A_S11_L001_R1_001.fastq.gz ../../../Mimulus_raw_data/Transcriptome/LF10/LF10_FB5A_replicate/LF10-5-6mm-FB-A_S11_L001_R2_001.fastq.gz \
 trimmed_LF10-5-6mm-FB-A_S11_L001_1_paired.fq.gz trimmed_LF10-5-6mm-FB-A_S11_L001_1_unpaired.fq.gz \
 trimmed_LF10-5-6mm-FB-A_S11_L001_2_paired.fq.gz trimmed_LF10-5-6mm-FB-A_S11_L001_2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:25 \
 MINLEN:50

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10-5-6mm-FB-A_S11_L002.txt \
 ../../../Mimulus_raw_data/Transcriptome/LF10/LF10_FB5A_replicate/LF10-5-6mm-FB-A_S11_L002_R1_001.fastq.gz ../../../Mimulus_raw_data/Transcriptome/LF10/LF10_FB5A_replicate/LF10-5-6mm-FB-A_S11_L002_R2_001.fastq.gz \
 trimmed_LF10-5-6mm-FB-A_S11_L002_1_paired.fq.gz trimmed_LF10-5-6mm-FB-A_S11_L002_1_unpaired.fq.gz \
 trimmed_LF10-5-6mm-FB-A_S11_L002_2_paired.fq.gz trimmed_LF10-5-6mm-FB-A_S11_L002_2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:25 \
 MINLEN:50

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10-5-6mm-FB-A_S11_L003.txt \ 
 ../../../Mimulus_raw_data/Transcriptome/LF10/LF10_FB5A_replicate/LF10-5-6mm-FB-A_S11_L003_R1_001.fastq.gz ../../../Mimulus_raw_data/Transcriptome/LF10/LF10_FB5A_replicate/LF10-5-6mm-FB-A_S11_L003_R2_001.fastq.gz \
 trimmed_LF10-5-6mm-FB-A_S11_L003_1_paired.fq.gz trimmed_LF10-5-6mm-FB-A_S11_L003_1_unpaired.fq.gz \
 trimmed_LF10-5-6mm-FB-A_S11_L003_2_paired.fq.gz trimmed_LF10-5-6mm-FB-A_S11_L003_2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:25 \
 MINLEN:50

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10-5-6mm-FB-A_S11_L004.txt \  
 ../../../Mimulus_raw_data/Transcriptome/LF10/LF10_FB5A_replicate/LF10-5-6mm-FB-A_S11_L004_R1_001.fastq.gz ../../../Mimulus_raw_data/Transcriptome/LF10/LF10_FB5A_replicate/LF10-5-6mm-FB-A_S11_L004_R2_001.fastq.gz \
 trimmed_LF10-5-6mm-FB-A_S11_L004_1_paired.fq.gz trimmed_LF10-5-6mm-FB-A_S11_L004_1_unpaired.fq.gz \
 trimmed_LF10-5-6mm-FB-A_S11_L004_2_paired.fq.gz trimmed_LF10-5-6mm-FB-A_S11_L004_2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:25 \
 MINLEN:50

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10_FB13A.txt \
 ../../raw_reads/LF10_FB13A_1.fastq.gz ../../raw_reads/LF10_FB13A_2.fastq.gz \
 trimmed_LF10_FB13A_1_paired.fq.gz trimmed_LF10_FB13A_1_unpaired.fq.gz \
 trimmed_LF10_FB13A_2_paired.fq.gz trimmed_LF10_FB13A_2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:20 \
 MINLEN:50

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10_FB13B.txt \
 ../../raw_reads/LF10_FB13B_1.fastq.gz ../../raw_reads/LF10_FB13B_2.fastq.gz \
 trimmed_LF10_FB13B_1_paired.fq.gz trimmed_LF10_FB13B_1_unpaired.fq.gz \
 trimmed_LF10_FB13B_2_paired.fq.gz trimmed_LF10_FB13B_2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:20 \
 MINLEN:50

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10_FB13C.txt \
 ../../raw_reads/LF10_FB13C_1.fastq.gz ../../raw_reads/LF10_FB13C_2.fastq.gz \
 trimmed_LF10_FB13C_1_paired.fq.gz trimmed_LF10_FB13C_1_unpaired.fq.gz \
 trimmed_LF10_FB13C_2_paired.fq.gz trimmed_LF10_FB13C_2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:20 \
 MINLEN:50

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10_FB5A.txt \
 ../../raw_reads/LF10_FB5A_1.fastq.gz ../../raw_reads/LF10_FB5A_2.fastq.gz \
 trimmed_LF10_FB5A_1_paired.fq.gz trimmed_LF10_FB5A_1_unpaired.fq.gz \
 trimmed_LF10_FB5A_2_paired.fq.gz trimmed_LF10_FB5A_2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:20 \
 MINLEN:50

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10_FB5B.txt \
 ../../raw_reads/LF10_FB5B_1.fastq.gz ../../raw_reads/LF10_FB5B_2.fastq.gz \
 trimmed_LF10_FB5B_1_paired.fq.gz trimmed_LF10_FB5B_1_unpaired.fq.gz \
 trimmed_LF10_FB5B_2_paired.fq.gz trimmed_LF10_FB5B_2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:20 \
 MINLEN:50

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 -trimlog log_LF10_FB5C.txt \
 ../../raw_reads/LF10_FB5C_1.fastq.gz ../../raw_reads/LF10_FB5C_2.fastq.gz \
 trimmed_LF10_FB5C_1_paired.fq.gz trimmed_LF10_FB5C_1_unpaired.fq.gz \
 trimmed_LF10_FB5C_2_paired.fq.gz trimmed_LF10_FB5C_2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:20 \
 MINLEN:50


