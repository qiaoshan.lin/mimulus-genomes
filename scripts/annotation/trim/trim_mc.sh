#!/bin/bash
#SBATCH --job-name=trim_mc
#SBATCH -n 4
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=25G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o trim_%j.out
#SBATCH -e trim_%j.err

module load Trimmomatic/0.36

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 4 \
 /home/CAM/qlin/raw/RNA/CE10/CE10C15_ACAGTG_L008_R1_001.fastq.gz  /home/CAM/qlin/raw/RNA/CE10/CE10C15_ACAGTG_L008_R2_001.fastq.gz \
 trimmed_CE10C15_ACAGTG_L008_R2_001_1_paired.fq.gz trimmed_CE10C15_ACAGTG_L008_R2_001_1_unpaired.fq.gz \
 trimmed_CE10C15_ACAGTG_L008_R2_001_2_paired.fq.gz trimmed_CE10C15_ACAGTG_L008_R2_001_2_unpaired.fq.gz \
 ILLUMINACLIP:/home/CAM/qlin/resource/adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:25 \
 MINLEN:50


