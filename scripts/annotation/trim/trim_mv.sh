#!/bin/bash
#SBATCH --job-name=trim
#SBATCH -n 8
#SBATCH -N 1
#SBATCH --qos=general
#SBATCH --partition=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o trim_%j.out
#SBATCH -e trim_%j.err

module load Trimmomatic/0.36

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 /home/CAM/qlin/raw/RNA/MvBL/MvBL_FB10_R1.fastq.gz /home/CAM/qlin/raw/RNA/MvBL/MvBL_FB10_R2.fastq.gz \
 trimmed_MvBL_FB10_R1_paired.fq.gz trimmed_MvBL_FB10_R1_unpaired.fq.gz trimmed_MvBL_FB10_R2_paired.fq.gz trimmed_MvBL_FB10_R2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:20 \
 MINLEN:50

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 /home/CAM/qlin/raw/RNA/MvBL/MvBL_FB16_R1.fastq.gz /home/CAM/qlin/raw/RNA/MvBL/MvBL_FB16_R2.fastq.gz \
 trimmed_MvBL_FB16_R1_paired.fq.gz trimmed_MvBL_FB16_R1_unpaired.fq.gz trimmed_MvBL_FB16_R2_paired.fq.gz trimmed_MvBL_FB16_R2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:20 \
 MINLEN:50

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 /home/CAM/qlin/raw/RNA/MvBL/MvBL_FB30_R1.fastq.gz /home/CAM/qlin/raw/RNA/MvBL/MvBL_FB30_R2.fastq.gz \
 trimmed_MvBL_FB30_R1_paired.fq.gz trimmed_MvBL_FB30_R1_unpaired.fq.gz trimmed_MvBL_FB30_R2_paired.fq.gz trimmed_MvBL_FB30_R2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:20 \
 MINLEN:50

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 /home/CAM/qlin/raw/RNA/MvBL/MvBL_FB5_R1.fastq.gz /home/CAM/qlin/raw/RNA/MvBL/MvBL_FB5_R2.fastq.gz \
 trimmed_MvBL_FB5_R1_paired.fq.gz trimmed_MvBL_FB5_R1_unpaired.fq.gz trimmed_MvBL_FB5_R2_paired.fq.gz trimmed_MvBL_FB5_R2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:20 \
 MINLEN:50

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 /home/CAM/qlin/raw/RNA/MvBL/MvBL_TinyFB_R1.fastq.gz /home/CAM/qlin/raw/RNA/MvBL/MvBL_TinyFB_R2.fastq.gz \
 trimmed_MvBL_TinyFB_R1_paired.fq.gz trimmed_MvBL_TinyFB_R1_unpaired.fq.gz trimmed_MvBL_TinyFB_R2_paired.fq.gz trimmed_MvBL_TinyFB_R2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:20 \
 MINLEN:50

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 8 \
 /home/CAM/qlin/raw/RNA/MvBL/MvBL_YoungLeaf_R1.fastq.gz /home/CAM/qlin/raw/RNA/MvBL/MvBL_YoungLeaf_R2.fastq.gz \
 trimmed_MvBL_YoungLeaf_R1_paired.fq.gz trimmed_MvBL_YoungLeaf_R1_unpaired.fq.gz trimmed_MvBL_YoungLeaf_R2_paired.fq.gz trimmed_MvBL_YoungLeaf_R2_unpaired.fq.gz \
 ILLUMINACLIP:../adapters.fa:2:30:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:20 \
 MINLEN:50



