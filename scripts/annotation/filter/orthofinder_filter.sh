#!/bin/bash
#SBATCH --job-name=orthofinder_filter
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%j.out
#SBATCH -e %x_%j.err

orthofinderDir=/scratch/qlin/orthofinder/t1/Results_Oct09/
workingDir=~/Mimulus_Genomes/results/annotation

mlcds=/home/CAM/qlin/resource/LF10/LF10g_v2.0.codingseq.fa
mccds=/home/CAM/qlin/resource/CE10/CE10g_v2.0.codingseq.fa
mpcds=/home/CAM/qlin/resource/Mpar/Mparg_v2.0.codingseq.fa
mvcds=/home/CAM/qlin/resource/MvBL/MvBLg_v2.0.codingseq.fa

mlgtf=~/resource/LF10/LF10g_v2.0.gtf
mcgtf=~/resource/CE10/CE10g_v2.0.gtf
mpgtf=~/resource/Mpar/Mparg_v2.0.gtf
mvgtf=~/resource/MvBL/MvBLg_v2.0.gtf

cd $orthofinderDir

awk '$2+$3+$4==0&&$5>0{print}' Orthogroups.GeneCount.csv |cut -f1 | grep 'OG' > 1y3n.txt
awk '$2+$3+$5==0&&$4>0{print}' Orthogroups.GeneCount.csv |cut -f1 | grep 'OG' >> 1y3n.txt
awk '$2+$4+$5==0&&$3>0{print}' Orthogroups.GeneCount.csv |cut -f1 | grep 'OG' >> 1y3n.txt
awk '$3+$4+$5==0&&$2>0{print}' Orthogroups.GeneCount.csv |cut -f1 | grep 'OG' >> 1y3n.txt

group=()
while read line; do   group+=("$line");   done < 1y3n.txt
for g in "${group[@]}"; do grep $g Orthogroups.csv; done > Orthogroups.1y3n.csv

awk '$2*$3>0&&$4+$5==0{print}' Orthogroups.GeneCount.csv |cut -f1 > 2y2n.txt
awk '$2*$4>0&&$3+$5==0{print}' Orthogroups.GeneCount.csv |cut -f1 >> 2y2n.txt
awk '$2*$5>0&&$3+$4==0{print}' Orthogroups.GeneCount.csv |cut -f1 >> 2y2n.txt
awk '$3*$4>0&&$2+$5==0{print}' Orthogroups.GeneCount.csv |cut -f1 >> 2y2n.txt
awk '$3*$5>0&&$2+$4==0{print}' Orthogroups.GeneCount.csv |cut -f1 >> 2y2n.txt
awk '$4*$5>0&&$2+$3==0{print}' Orthogroups.GeneCount.csv |cut -f1 >> 2y2n.txt

group=()
while read line; do   group+=("$line");   done < 2y2n.txt 
for g in "${group[@]}"; do grep $g Orthogroups.csv; done > Orthogroups.2y2n.csv

awk '$2*$3*$4>0&&$5==0{print}' Orthogroups.GeneCount.csv |cut -f1 > 3y1n.txt
awk '$2*$3*$5>0&&$4==0{print}' Orthogroups.GeneCount.csv |cut -f1 >> 3y1n.txt
awk '$2*$4*$5>0&&$3==0{print}' Orthogroups.GeneCount.csv |cut -f1 >> 3y1n.txt
awk '$3*$4*$5>0&&$2==0{print}' Orthogroups.GeneCount.csv |cut -f1 >> 3y1n.txt

group=()
while read line; do   group+=("$line");   done < 3y1n.txt 
for g in "${group[@]}"; do grep $g Orthogroups.csv; done > Orthogroups.3y1n.csv

awk '$2*$3*$4*$5>0{print}' Orthogroups.GeneCount.csv |cut -f1 > 4y.txt

cat 1y3n.txt 2y2n.txt > filter.txt
cat 3y1n.txt 4y.txt > keep.txt

rm 1y3n.txt 2y2n.txt 3y1n.txt 4y.txt

group=()
while read line; do   group+=("$line");   done < filter.txt 
for g in "${group[@]}"; do grep $g Orthogroups.csv; done > tmp

cut -f2 tmp > Orthogroups.filter.CE10.csv
cut -f3 tmp > Orthogroups.filter.LF10.csv
cut -f4 tmp > Orthogroups.filter.Mpar.csv
cut -f5 tmp > Orthogroups.filter.MvBL.csv

rm filter.txt tmp

perl -lane 'print "$&" while /M\w+G\d+\.\d+/g' Orthogroups.filter.LF10.csv > LF10.genes.filter.txt
perl -lane 'print "$&" while /M\w+G\d+\.\d+/g' Orthogroups.filter.CE10.csv > CE10.genes.filter.txt
perl -lane 'print "$&" while /M\w+G\d+\.\d+/g' Orthogroups.filter.Mpar.csv > Mpar.genes.filter.txt
perl -lane 'print "$&" while /M\w+G\d+\.\d+/g' Orthogroups.filter.MvBL.csv > MvBL.genes.filter.txt

rm Orthogroups.filter.????.csv

group=()
while read line; do   group+=("$line");   done < keep.txt
for g in "${group[@]}"; do grep $g Orthogroups.csv; done > tmp

cut -f2 tmp > Orthogroups.keep.CE10.csv
cut -f3 tmp > Orthogroups.keep.LF10.csv
cut -f4 tmp > Orthogroups.keep.Mpar.csv
cut -f5 tmp > Orthogroups.keep.MvBL.csv

rm keep.txt tmp

perl -lane 'print "$&" while /M\w+G\d+\.\d+/g' Orthogroups.keep.LF10.csv > LF10.genes.keep.txt
perl -lane 'print "$&" while /M\w+G\d+\.\d+/g' Orthogroups.keep.CE10.csv > CE10.genes.keep.txt
perl -lane 'print "$&" while /M\w+G\d+\.\d+/g' Orthogroups.keep.Mpar.csv > Mpar.genes.keep.txt
perl -lane 'print "$&" while /M\w+G\d+\.\d+/g' Orthogroups.keep.MvBL.csv > MvBL.genes.keep.txt

rm Orthogroups.keep.????.csv

module load seqtk

seqtk subseq $mlcds LF10.genes.filter.txt > LF10.codingseq.filter.fa
seqtk subseq $mccds CE10.genes.filter.txt > CE10.codingseq.filter.fa
seqtk subseq $mpcds Mpar.genes.filter.txt > Mpar.codingseq.filter.fa
seqtk subseq $mvcds MvBL.genes.filter.txt > MvBL.codingseq.filter.fa

perl ~/scripts/lengthFilter4fa.pl LF10.codingseq.filter.fa 450 | cut -f1 > LF10.filter2.txt
perl ~/scripts/lengthFilter4fa.pl CE10.codingseq.filter.fa 450 | cut -f1 > CE10.filter2.txt
perl ~/scripts/lengthFilter4fa.pl Mpar.codingseq.filter.fa 450 | cut -f1 > Mpar.filter2.txt
perl ~/scripts/lengthFilter4fa.pl MvBL.codingseq.filter.fa 450 | cut -f1 > MvBL.filter2.txt

rm ????.codingseq.filter.fa

awk '$3~/intron/{print $10}' $mlgtf |tr -d '"'|tr -d ';'| sort |uniq > LF10.with.intron.txt
awk '$3~/intron/{print $10}' $mcgtf |tr -d '"'|tr -d ';'| sort |uniq > CE10.with.intron.txt
awk '$3~/intron/{print $10}' $mpgtf |tr -d '"'|tr -d ';'| sort |uniq > Mpar.with.intron.txt
awk '$3~/intron/{print $10}' $mvgtf |tr -d '"'|tr -d ';'| sort |uniq > MvBL.with.intron.txt

cat LF10.with.intron.txt LF10.genes.filter.txt | sort | uniq -c | awk '$1==2{print $2}' > LF10.filter3.txt
cat CE10.with.intron.txt CE10.genes.filter.txt | sort | uniq -c | awk '$1==2{print $2}' > CE10.filter3.txt
cat Mpar.with.intron.txt Mpar.genes.filter.txt | sort | uniq -c | awk '$1==2{print $2}' > Mpar.filter3.txt
cat MvBL.with.intron.txt MvBL.genes.filter.txt | sort | uniq -c | awk '$1==2{print $2}' > MvBL.filter3.txt

rm ????.genes.filter.txt
rm ????.with.intron.txt

cat LF10.filter2.txt LF10.filter3.txt | sort | uniq > LF10.filter4.txt
cat CE10.filter2.txt CE10.filter3.txt | sort | uniq > CE10.filter4.txt
cat Mpar.filter2.txt Mpar.filter3.txt | sort | uniq > Mpar.filter4.txt
cat MvBL.filter2.txt MvBL.filter3.txt | sort | uniq > MvBL.filter4.txt

rm ????.filter2.txt
rm ????.filter3.txt

cat LF10.genes.keep.txt LF10.filter4.txt | sort > LF10.txt
cat CE10.genes.keep.txt CE10.filter4.txt | sort > CE10.txt
cat Mpar.genes.keep.txt Mpar.filter4.txt | sort > Mpar.txt
cat MvBL.genes.keep.txt MvBL.filter4.txt | sort > MvBL.txt

rm ????.genes.keep.txt 
rm ????.filter4.txt

cut -f1 -d '.' LF10.txt > LF10.gene.txt
cut -f1 -d '.' CE10.txt > CE10.gene.txt
cut -f1 -d '.' Mpar.txt > Mpar.gene.txt
cut -f1 -d '.' MvBL.txt > MvBL.gene.txt

rm ????.txt

grep '>' $mlcds |tr -d '>' > LF10.all.transcripts.txt
grep '>' $mccds |tr -d '>' > CE10.all.transcripts.txt
grep '>' $mpcds |tr -d '>' > Mpar.all.transcripts.txt
grep '>' $mvcds |tr -d '>' > MvBL.all.transcripts.txt

gene=()
while read line; do   gene+=("$line");   done < LF10.gene.txt
for g in "${gene[@]}"; do grep $g LF10.all.transcripts.txt; done > $workingDir/LF10/transcripts.txt

gene=()
while read line; do   gene+=("$line");   done < CE10.gene.txt
for g in "${gene[@]}"; do grep $g CE10.all.transcripts.txt; done > $workingDir/CE10/transcripts.txt

gene=()
while read line; do   gene+=("$line");   done < Mpar.gene.txt
for g in "${gene[@]}"; do grep $g Mpar.all.transcripts.txt; done > $workingDir/Mpar/transcripts.txt

gene=()
while read line; do   gene+=("$line");   done < MvBL.gene.txt
for g in "${gene[@]}"; do grep $g MvBL.all.transcripts.txt; done > $workingDir/MvBL/transcripts.txt

rm ????.gene.txt
rm ????.all.transcripts.txt


