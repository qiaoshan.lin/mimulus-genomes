#!/bin/bash
#SBATCH --job-name=gtf_filter
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-3
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=5G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

workingDir=/home/CAM/qlin/Mimulus_Genomes/results/annotation
label=(LF10 CE10 Mpar MvBL)

cd $workingDir/${label[$SLURM_ARRAY_TASK_ID]}

gtf=braker/Sp*/augustus.hints.gtf

cut -f1 -d '.' transcripts.txt |uniq > genes.txt
gene=()
while read line; do   gene+=("$line");   done < genes.txt
for g in "${gene[@]}"; do grep $g"$" $gtf -n >> gene.line; done
transcript=()
while read line; do   transcript+=("$line");   done < transcripts.txt
for t in "${transcript[@]}"; do grep $t $gtf -n >> transcript.line; done

cat gene.line transcript.line |sed 's/:/\t/' |sort -k1 -n |cut -f2- > ${label[$SLURM_ARRAY_TASK_ID]}.gtf

rm *line


