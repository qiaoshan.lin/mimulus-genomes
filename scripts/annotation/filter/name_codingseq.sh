#!/bin/bash
#SBATCH --job-name=name_codingseq
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-3
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=5G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

workingDir=/home/CAM/qlin/Mimulus_Genomes/results/annotation
label=(LF10 CE10 Mpar MvBL)

cd $workingDir/${label[$SLURM_ARRAY_TASK_ID]}

array=()
while read line; do   array+=("$line");   done < gene_name.map

for a in "${array[@]}"
do
        pattern=`echo $a|awk '{print "s/"$1".t/"$2"./"}'`
        sed -i $pattern ${label[$SLURM_ARRAY_TASK_ID]}.codingseq.fa
done

