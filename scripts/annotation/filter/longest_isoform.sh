#!/bin/bash
#SBATCH --job-name=longest_isoform
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-3
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load seqtk

# copy the codingseq.fa and protein.fa files into the same directory
workingDir=/home/CAM/qlin/Mimulus_Genomes/results/annotation
label=(LF10 CE10 Mpar MvBL)

protein=(/home/CAM/qlin/resource/LF10/LF10g_v2.0.protein.fa /home/CAM/qlin/resource/CE10/CE10g_v2.0.protein.fa /home/CAM/qlin/resource/Mpar/Mparg_v2.0.protein.fa /home/CAM/qlin/resource/MvBL/MvBLg_v2.0.protein.fa)
codingseq=(/home/CAM/qlin/resource/LF10/LF10g_v2.0.codingseq.fa /home/CAM/qlin/resource/CE10/CE10g_v2.0.codingseq.fa /home/CAM/qlin/resource/Mpar/Mparg_v2.0.codingseq.fa /home/CAM/qlin/resource/MvBL/MvBLg_v2.0.codingseq.fa)

proteinOutSuffix=g_v2.0.longest_isoform.protein.fa
codingseqOutSuffix=g_v2.0.longest_isoform.codingseq.fa

cd $workingDir/${label[$SLURM_ARRAY_TASK_ID]}

grep '>' ${protein[$SLURM_ARRAY_TASK_ID]} |tr -d '>' > ${label[$SLURM_ARRAY_TASK_ID]}.protein.txt
sed -r 's/\.[0-9]$//' ${label[$SLURM_ARRAY_TASK_ID]}.protein.txt|sort|uniq -c|awk '$1>1{print $2}' > ${label[$SLURM_ARRAY_TASK_ID]}.alternative_splicing_genes.txt
array=()
while read line; do   array+=("$line");   done < ${label[$SLURM_ARRAY_TASK_ID]}.alternative_splicing_genes.txt
for a in ${array[@]}; do b=$a.; grep $b ${label[$SLURM_ARRAY_TASK_ID]}.protein.txt; done > ${label[$SLURM_ARRAY_TASK_ID]}.alternative_splicing_transcripts.txt
perl ~/scripts/fetchSeq4fa2.pl ${protein[$SLURM_ARRAY_TASK_ID]} ${label[$SLURM_ARRAY_TASK_ID]}.alternative_splicing_transcripts.txt ${label[$SLURM_ARRAY_TASK_ID]}.tmp.fa
perl ~/scripts/lengthFilter4fa.pl ${label[$SLURM_ARRAY_TASK_ID]}.tmp.fa 0 ${label[$SLURM_ARRAY_TASK_ID]}.tmp2.fa |tr -d '>' > ${label[$SLURM_ARRAY_TASK_ID]}.alternative_splicing_length.txt
rm ${label[$SLURM_ARRAY_TASK_ID]}.tmp*.fa
gene=()
while read line; do   gene+=("$line");   done < ${label[$SLURM_ARRAY_TASK_ID]}.alternative_splicing_genes.txt
for g in "${gene[@]}"
do
        grep -P "$g." ${label[$SLURM_ARRAY_TASK_ID]}.alternative_splicing_length.txt|sort -k2 -nr|head -1|cut -f1 >> ${label[$SLURM_ARRAY_TASK_ID]}.alternative_splicing_transcripts_longest_isoform.txt
done
cat ${label[$SLURM_ARRAY_TASK_ID]}.alternative_splicing_transcripts.txt ${label[$SLURM_ARRAY_TASK_ID]}.protein.txt |sort|uniq -c|awk '$1==1{print $2}' > ${label[$SLURM_ARRAY_TASK_ID]}.no_alternative_splicing_transcripts.txt
cat ${label[$SLURM_ARRAY_TASK_ID]}.no_alternative_splicing_transcripts.txt ${label[$SLURM_ARRAY_TASK_ID]}.alternative_splicing_transcripts_longest_isoform.txt|sort -V > ${label[$SLURM_ARRAY_TASK_ID]}.longest_isoform.txt
seqtk subseq ${protein[$SLURM_ARRAY_TASK_ID]} ${label[$SLURM_ARRAY_TASK_ID]}.longest_isoform.txt > ${label[$SLURM_ARRAY_TASK_ID]}$proteinOutSuffix
seqtk subseq ${codingseq[$SLURM_ARRAY_TASK_ID]} ${label[$SLURM_ARRAY_TASK_ID]}.longest_isoform.txt > ${label[$SLURM_ARRAY_TASK_ID]}$codingseqOutSuffix

rm ${label[$SLURM_ARRAY_TASK_ID]}.*txt
