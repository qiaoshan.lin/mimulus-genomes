#!/bin/bash
#SBATCH --job-name=gtf_filter_beta
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-3
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=5G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

workingDir=/home/CAM/qlin/Mimulus_Genomes/results/annotation
label=(LF10 CE10 Mpar MvBL)

gtf=(/home/CAM/qlin/resource/LF10/LF10g_v2.0.gtf /home/CAM/qlin/resource/CE10/CE10g_v2.0.gtf /home/CAM/qlin/resource/Mpar/Mparg_v2.0.gtf /home/CAM/qlin/resource/MvBL/MvBLg_v2.0.gtf)

cd $workingDir/${label[$SLURM_ARRAY_TASK_ID]}

cut -f1 -d '.' keep.txt |uniq > genes.txt

gene=()
while read line; do   gene+=("$line");   done < genes.txt
for g in "${gene[@]}"; do grep $g ${gtf[$SLURM_ARRAY_TASK_ID]} >> ${label[$SLURM_ARRAY_TASK_ID]}g_v2.0beta.gtf; done

rm genes.txt keep.txt

