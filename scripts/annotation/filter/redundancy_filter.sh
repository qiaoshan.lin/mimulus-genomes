#!/bin/bash
#SBATCH --job-name=redundancy_filter
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-3
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load blast
module load samtools

workingDir=/home/CAM/qlin/Mimulus_Genomes/results/annotation
label=(LF10 CE10 Mpar MvBL)

cd $workingDir/${label[$SLURM_ARRAY_TASK_ID]}

cat N_percent_0.5_1.txt braker/Sp*/txt/alternative_splicing_transcripts_longest_isoform.txt |sort|uniq -c|awk '$1==2{print $2}'|cat - braker/Sp*/txt/alternative_splicing_transcripts_longest_isoform.txt |sort|uniq -c|awk '$1==1{print $2}' |cut -f1 -d "." > alternative_splicing_genes.N_percent_0_0.5.txt

gene=()
while read line; do   gene+=("$line");   done < alternative_splicing_genes.N_percent_0_0.5.txt
for g in "${gene[@]}"
do 
        transcript=`grep -P "$g.t" longest_isoform.txt`
	samtools faidx ${label[$SLURM_ARRAY_TASK_ID]}.longest_isoform.codingseq.fa $transcript > ${label[$SLURM_ARRAY_TASK_ID]}_tmp.fa
	perl ~/scripts/fetchSeq4fa.pl braker/Sp*/${label[$SLURM_ARRAY_TASK_ID]}.codingseq.fa $g\.t > query.fa
	/isg/shared/apps/blast/ncbi-blast-2.7.1+/bin/blastn -subject ${label[$SLURM_ARRAY_TASK_ID]}_tmp.fa -query query.fa -outfmt "6 qseqid qlen qstart qend sseqid slen sstart send pident mismatch length sstrand" > $g\.blast
done

rm query.fa ${label[$SLURM_ARRAY_TASK_ID]}_tmp.fa 
cat *blast > blast.txt
rm *blast
awk '$1!~$5{print}' blast.txt|awk '!($6-$2<=6&&$6==$11){print}'|cut -f1|sort|uniq > ${label[$SLURM_ARRAY_TASK_ID]}_tmp.txt
cat ${label[$SLURM_ARRAY_TASK_ID]}_tmp.txt N_percent_0.5_1.txt |sort|uniq -c|awk '$1==2{print $2}'|cat - ${label[$SLURM_ARRAY_TASK_ID]}_tmp.txt |sort|uniq -c|awk '$1==1{print $2}'> alternative_splicing_transcripts.N_percent_0_0.5.txt 
cat longest_isoform.N_percent_0_0.5.txt alternative_splicing_transcripts.N_percent_0_0.5.txt > transcripts.N_percent_0_0.5.txt

cat transcripts.N_percent_0_0.5.txt braker/Sp*/txt/transcripts_with_start_and_stop_codon.txt |sort|uniq -c|awk '$1==2{print $2}'|sort -V > transcripts.txt
seqtk subseq braker/Sp*/augustus.hints.aa transcripts.txt > ${label[$SLURM_ARRAY_TASK_ID]}.protein.fa
seqtk subseq braker/Sp*/${label[$SLURM_ARRAY_TASK_ID]}.codingseq.fa transcripts.txt > ${label[$SLURM_ARRAY_TASK_ID]}.codingseq.fa

rm alternative_splicing_genes.N_percent_0_0.5.txt blast.txt ${label[$SLURM_ARRAY_TASK_ID]}_tmp.txt alternative_splicing_transcripts.N_percent_0_0.5.txt longest_isoform.N_percent_0_0.5.txt transcripts.N_percent_0_0.5.txt 

