#!/bin/bash
#SBATCH --job-name=repeatMasker
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 4
#SBATCH -a 0-3 
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load RepeatMasker/4.0.6
module unload perl
module load seqtk/1.2

workingDir=/home/CAM/qlin/Mimulus_Genomes/results/annotation
label=(LF10 CE10 Mpar MvBL)

cd $workingDir/${label[$SLURM_ARRAY_TASK_ID]}

mkdir repeatMasker_20191010

RepeatMasker /home/CAM/qlin/resource/${label[$SLURM_ARRAY_TASK_ID]}/${label[$SLURM_ARRAY_TASK_ID]}g_v2.0.codingseq.fa -dir ./repeatMasker_20191010 -lib /home/CAM/qlin/Mimulus_Genomes/results/TE/Ml_all_repeats_20191010.fasta -pa 4 -nolow

perl ~/scripts/Nstat.pl repeatMasker_20191010/${label[$SLURM_ARRAY_TASK_ID]}g_v2.0.codingseq.fa.masked > Nstat.out
awk '$2<0.5{print $1}' Nstat.out > N_percent_0_0.5.txt

rm Nstat.out

cat N_percent_0_0.5.txt transcripts.txt |sort|uniq -c|awk '$1==2{print $2}' > keep.txt

seqtk subseq /home/CAM/qlin/resource/${label[$SLURM_ARRAY_TASK_ID]}/${label[$SLURM_ARRAY_TASK_ID]}g_v2.0.protein.fa keep.txt > ${label[$SLURM_ARRAY_TASK_ID]}g_v2.0beta.protein.fa
seqtk subseq /home/CAM/qlin/resource/${label[$SLURM_ARRAY_TASK_ID]}/${label[$SLURM_ARRAY_TASK_ID]}g_v2.0.codingseq.fa keep.txt > ${label[$SLURM_ARRAY_TASK_ID]}g_v2.0beta.codingseq.fa

rm N_percent_0_0.5.txt transcripts.txt


