#!/bin/bash
#SBATCH --job-name=name_gtf
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-3
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=5G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

workingDir=/home/CAM/qlin/Mimulus_Genomes/results/annotation
label=(LF10 CE10 Mpar MvBL)
prefix=(ML MC MP MV)

cd $workingDir/${label[$SLURM_ARRAY_TASK_ID]}

awk -v prefix=${prefix[$SLURM_ARRAY_TASK_ID]} '{if($1~/chr1/&&$3~/gene/){n+=1;printf $9"\t"$prefix"1G%06d\n", n*100}}' ${label[$SLURM_ARRAY_TASK_ID]}.gtf > gene_name.map
awk -v prefix=${prefix[$SLURM_ARRAY_TASK_ID]} '{if($1~/chr2/&&$3~/gene/){n+=1;printf $9"\t"$prefix"2G%06d\n", n*100}}' ${label[$SLURM_ARRAY_TASK_ID]}.gtf >> gene_name.map
awk -v prefix=${prefix[$SLURM_ARRAY_TASK_ID]} '{if($1~/chr3/&&$3~/gene/){n+=1;printf $9"\t"$prefix"3G%06d\n", n*100}}' ${label[$SLURM_ARRAY_TASK_ID]}.gtf >> gene_name.map
awk -v prefix=${prefix[$SLURM_ARRAY_TASK_ID]} '{if($1~/chr4/&&$3~/gene/){n+=1;printf $9"\t"$prefix"4G%06d\n", n*100}}' ${label[$SLURM_ARRAY_TASK_ID]}.gtf >> gene_name.map
awk -v prefix=${prefix[$SLURM_ARRAY_TASK_ID]} '{if($1~/chr5/&&$3~/gene/){n+=1;printf $9"\t"$prefix"5G%06d\n", n*100}}' ${label[$SLURM_ARRAY_TASK_ID]}.gtf >> gene_name.map
awk -v prefix=${prefix[$SLURM_ARRAY_TASK_ID]} '{if($1~/chr6/&&$3~/gene/){n+=1;printf $9"\t"$prefix"6G%06d\n", n*100}}' ${label[$SLURM_ARRAY_TASK_ID]}.gtf >> gene_name.map
awk -v prefix=${prefix[$SLURM_ARRAY_TASK_ID]} '{if($1~/chr7/&&$3~/gene/){n+=1;printf $9"\t"$prefix"7G%06d\n", n*100}}' ${label[$SLURM_ARRAY_TASK_ID]}.gtf >> gene_name.map
awk -v prefix=${prefix[$SLURM_ARRAY_TASK_ID]} '{if($1~/chr8/&&$3~/gene/){n+=1;printf $9"\t"$prefix"8G%06d\n", n*100}}' ${label[$SLURM_ARRAY_TASK_ID]}.gtf >> gene_name.map
awk -v prefix=${prefix[$SLURM_ARRAY_TASK_ID]} '{if($1~/^tig/&&$3~/gene/){n+=1;printf $9"\t"$prefix"NG%06d\n", n*100}}' ${label[$SLURM_ARRAY_TASK_ID]}.gtf >> gene_name.map

array=()
while read line; do   array+=("$line");   done < gene_name.map
cut -f1 gene_name.map > tmp1
gene=()
while read line; do   gene+=("$line");   done < tmp1
cut -f2 gene_name.map > tmp2
name=()
while read line; do   name+=("$line");   done < tmp2

for a in "${!array[@]}"
do
	ge=${gene[$a]}
	na=${name[$a]}
	awk -v gene="$ge" -v name="$na" '{if($3~/gene/&&$9~gene"$"){print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\tgene_id \""name"\";"}}' ${label[$SLURM_ARRAY_TASK_ID]}.gtf >> ${label[$SLURM_ARRAY_TASK_ID]}.named.gtf
	awk -v gene="$ge" -v name="$na" '{if($0~gene".t"){gsub(gene,name); gsub(/\.t/,"."); if($3~/transcript/){print $1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\ttranscript_id \""$9"\";"}else{print} }}' ${label[$SLURM_ARRAY_TASK_ID]}.gtf >> ${label[$SLURM_ARRAY_TASK_ID]}.named.gtf
done

rm tmp1 tmp2

