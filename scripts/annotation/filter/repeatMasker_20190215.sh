#!/bin/bash
#SBATCH --job-name=repeatMasker
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 4
#SBATCH -a 0-3 
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load RepeatMasker/4.0.6
module unload perl
module load seqtk/1.2

workingDir=/home/CAM/qlin/Mimulus_Genomes/results/annotation
label=(LF10 CE10 Mpar MvBL)

cd $workingDir/${label[$SLURM_ARRAY_TASK_ID]}

mkdir repeatMasker_20190215

RepeatMasker braker/Sp*/${label[$SLURM_ARRAY_TASK_ID]}.codingseq.fa -dir ./repeatMasker_20190215 -lib /home/CAM/qlin/Mimulus_Genomes/results/TE/Ml_all_repeats_20190215.ch.fasta -pa 4 -nolow

perl ~/scripts/Nstat.pl repeatMasker_20190215/${label[$SLURM_ARRAY_TASK_ID]}.codingseq.fa.masked > Nstat.out
awk '$2>0.3{print $1}' Nstat.out > N_percent_0.3_1.txt
awk '$2>0.5{print $1}' Nstat.out > N_percent_0.5_1.txt
awk '$2>0.7{print $1}' Nstat.out > N_percent_0.7_1.txt

rm Nstat.out

cat N_percent_0.5_1.txt braker/Sp*/txt/augustus.hints.txt |sort|uniq -c|awk '$1==1{print $2}' > N_percent_0_0.5.txt
seqtk subseq braker/Sp*/augustus.hints.aa N_percent_0_0.5.txt > ${label[$SLURM_ARRAY_TASK_ID]}.protein.fa
seqtk subseq braker/Sp*/${label[$SLURM_ARRAY_TASK_ID]}.codingseq.fa N_percent_0_0.5.txt > ${label[$SLURM_ARRAY_TASK_ID]}.codingseq.fa




