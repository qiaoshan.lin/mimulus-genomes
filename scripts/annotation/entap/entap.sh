#!/bin/bash
#SBATCH --job-name=entap
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH -a 0-3  
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load diamond/0.9.19
module load interproscan

protein=(/home/CAM/qlin/resource/LF10/LF10g_v2.0.protein.fa /home/CAM/qlin/resource/CE10/CE10g_v2.0.protein.fa /home/CAM/qlin/resource/Mpar/Mparg_v2.0.protein.fa /home/CAM/qlin/resource/MvBL/MvBLg_v2.0.protein.fa)

label=(LF10 CE10 Mpar MvBL)

workingDir=~/Mimulus_Genomes/results/annotation/${label[$SLURM_ARRAY_TASK_ID]}
cd $workingDir

/labs/Wegrzyn/EnTAP/EnTAP --config
/labs/Wegrzyn/EnTAP/EnTAP --runP \
 --ontology 0 --ontology 1 \
 --protein pfam \
 --out-dir entap \
 -i ${protein[$SLURM_ARRAY_TASK_ID]} \
 -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd -d /isg/shared/databases/Diamond/RefSeq/plant.protein.faa.94.dmnd -d /home/CAM/qlin/resource/Mgut/Mguttatus_256_v2.0.protein.0.9.19.dmnd \
 -t 8


