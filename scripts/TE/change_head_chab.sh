#!/bin/bash
#SBATCH --job-name=changeHead
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=1G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o changeHead2_%j.out
#SBATCH -e changeHead2_%j.err

infile=Ml_all_repeats_20190215.fasta
outfile=Ml_all_repeats_20190215.cha.fasta
outfile2=Ml_all_repeats_20190215.chb.fasta

cp $infile tmp.fa

awk '/^>MLE[0-9]+/{print $0"#SINE/Alu"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/^>Stowaway[0-9]+/{print $0"#SINE/MIR"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/^>CACTA[0-9]+/{print $0"#SINE"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/^>CACTA_na[0-9]+/{print $0"#LINE/L1"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/^>Harbinger[0-9]+/{print $0"#LINE/L2"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/^>Tourist[0-9]+/{print $0"#LINE/CR1"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/^>hAT[0-9]+/{print $0"#LINE"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/^>hAT_na[0-9]+/{print $0"#Small_RNA"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/^>MULE[0-9]+/{print $0"#Satellite"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/^>MULE_na[0-9]+/{print $0"#Simple_repeat"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/^>Helitron[0-9]+/{print $0"#DNA/hAT-Charlie"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/^>Helitron_na[0-9]+/{print $0"#DNA/TcMar-Tigger"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/^>Gypsy[0-9]+/{print $0"#LTR/ERVL"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/^>Tandem_Repeat[0-9]+/{print $0"#LTR/ERVL-MaLR"; next}{print}' tmp.fa > $outfile
rm tmp.fa

cp $infile tmp.fa

awk '/^>Copia_na[0-9]+/{print $0"#SINE/Alu"; next}{print}' tmp.fa > $outfile2
mv $outfile2 tmp.fa
awk '/^>Copia_Solo[0-9]+/{print $0"#SINE/MIR"; next}{print}' tmp.fa > $outfile2
mv $outfile2 tmp.fa
awk '/^>Gypsy[0-9]+/{print $0"#SINE"; next}{print}' tmp.fa > $outfile2
mv $outfile2 tmp.fa
awk '/^>Gypsy_na[0-9]+/{print $0"#LINE/L1"; next}{print}' tmp.fa > $outfile2
mv $outfile2 tmp.fa
awk '/^>Gypsy_Solo[0-9]+/{print $0"#LINE/L2"; next}{print}' tmp.fa > $outfile2
mv $outfile2 tmp.fa
awk '/^>SINE[0-9]+/{print $0"#LINE/CR1"; next}{print}' tmp.fa > $outfile2
mv $outfile2 tmp.fa
awk '/^>Tandem_Repeat[0-9]+/{print $0"#LINE"; next}{print}' tmp.fa > $outfile2
mv $outfile2 tmp.fa
awk '/^>MlrDNA[0-9]+/{print $0"#Small_RNA"; next}{print}' tmp.fa > $outfile2
mv $outfile2 tmp.fa
awk '/^>MlrDNA_5S/{print $0"#Satellite"; next}{print}' tmp.fa > $outfile2
mv $outfile2 tmp.fa
awk '/^>CenR[0-9]+/{print $0"#Simple_repeat"; next}{print}' tmp.fa > $outfile2
mv $outfile2 tmp.fa
awk '/^>CACTA[0-9]+/{print $0"#DNA/hAT-Charlie"; next}{print}' tmp.fa > $outfile2
mv $outfile2 tmp.fa
awk '/^>hAT[0-9]+/{print $0"#DNA/TcMar-Tigger"; next}{print}' tmp.fa > $outfile2
mv $outfile2 tmp.fa
awk '/^>LINE[0-9]+/{print $0"#LTR/ERVL"; next}{print}' tmp.fa > $outfile2
mv $outfile2 tmp.fa
awk '/^>Copia[0-9]+/{print $0"#LTR/ERVL-MaLR"; next}{print}' tmp.fa > $outfile2
rm tmp.fa

