#!/bin/bash
#SBATCH --job-name=add
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=1G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o addFamily_%j.out
#SBATCH -e addFamily_%j.err

infile=Ml_all_repeats_20190215.fasta
outfile=Ml_all_repeats_20190215.m.fasta

cp $infile tmp.fa
sed -i -E 's/#(.+)$//' tmp.fa
awk '/>Helitron/{print $0"#Helitron"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/>hAT/{print $0"#hAT"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/>MLE/{print $0"#MLE"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/>Stowaway/{print $0"#Stowaway"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/>MULE/{print $0"#MULE"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/>CACTA/{print $0"#CACTA"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/>Harbinger/{print $0"#Harbinger"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/>Tourist/{print $0"#Tourist"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/>LINE/{print $0"#LINE"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/>SINE/{print $0"#SINE"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/>Copia/{print $0"#Copia"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/>Gypsy/{print $0"#Gypsy"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/>LTR_na/{print $0"#LTR_na"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/>LTR_Solo/{print $0"#LTR_Solo"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/>Tandem_Repeat/{print $0"#Tandem_Repeat"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/>MlrDNA1/{print $0"#MlrDNA1"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/>MlrDNA_5S/{print $0"#MlrDNA_5S"; next}{print}' tmp.fa > $outfile
mv $outfile tmp.fa
awk '/>CenR/{print $0"#CenR"; next}{print}' tmp.fa > $outfile
rm tmp.fa


