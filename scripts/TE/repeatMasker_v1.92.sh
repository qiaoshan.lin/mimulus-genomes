#!/bin/bash
#SBATCH --job-name=repeatMasker
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH -a 0-3
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load RepeatMasker/4.0.6
module unload perl

genomeDir=/home/CAM/qlin/Mimulus_Genomes/results/assembly
label=(LF10 CE10 Mpar MvBL)

workingDir=~/Mimulus_Genomes/results/TE

cd $workingDir/${label[$SLURM_ARRAY_TASK_ID]}

mkdir repeatMasker_20190215_soft_a_v1.92
RepeatMasker $genomeDir/${label[$SLURM_ARRAY_TASK_ID]}/${label[$SLURM_ARRAY_TASK_ID]}g_v1.92.fa -dir ./repeatMasker_20190215_soft_a_v1.92 -lib $workingDir/Ml_all_repeats_20190215.cha.fasta -xsmall -pa 8 -nolow

mkdir repeatMasker_20190215_soft_b_v1.92
RepeatMasker $genomeDir/${label[$SLURM_ARRAY_TASK_ID]}/${label[$SLURM_ARRAY_TASK_ID]}g_v1.92.fa -dir ./repeatMasker_20190215_soft_b_v1.92 -lib $workingDir/Ml_all_repeats_20190215.chb.fasta -xsmall -pa 8 -nolow


