#!/bin/bash
#SBATCH --job-name=NumberEstimate
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH -a 0-3
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load bowtie2/2.3.1 
module load samtools/1.3.1
module load bamtools/2.4.1

bamDir=/home/CAM/qlin/Mimulus_Genomes/results/genome_size
workingDir=/home/CAM/qlin/Mimulus_Genomes/results/statistics/mt_cp_num

cd $workingDir

label=(LF10 CE10 Mpar MvBL)

samtools sort -o ${label[$SLURM_ARRAY_TASK_ID]}.sort.bam -T ${label[$SLURM_ARRAY_TASK_ID]}_tmp -O bam -@ 8 $bamDir/${label[$SLURM_ARRAY_TASK_ID]}.bam

samtools depth -aa ${label[$SLURM_ARRAY_TASK_ID]}.sort.bam > ${label[$SLURM_ARRAY_TASK_ID]}.depth

# The estimated number of cpDNA
# awk '$1~/cpDNA/{s+=$3;n+=1}END{print s/n}' ${label[$SLURM_ARRAY_TASK_ID]}.depth

# The estimated number of mtDNA
# awk '$1~/LF10-MCv1/{s+=$3;n+=1}END{print s/n}' LF10.depth
# awk '$1~/CE10-MCv1/{s+=$3;n+=1}END{print s/n}' CE10.depth
# awk '$1~/^c/{s+=$3;n+=1}END{print s/n}' Mpar.depth
# awk '$1~/^c/{s+=$3;n+=1}END{print s/n}' MvBL.depth



