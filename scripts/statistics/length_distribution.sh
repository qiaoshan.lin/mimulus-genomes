#!/bin/bash
#SBATCH --job-name=length_distribution
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-3
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=1G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

label=(LF10 CE10 Mpar MvBL)

inputDir=~/Mimulus_Genomes/results/annotation/${label[$SLURM_ARRAY_TASK_ID]}

outputDir=~/Mimulus_Genomes/results/statistics/length_distribution/${label[$SLURM_ARRAY_TASK_ID]}

cd $outputDir

awk '$3~/intron/{print $5-$4+1}' $inputDir/${label[$SLURM_ARRAY_TASK_ID]}.gtf |sort -n|uniq -c|awk '{print $2"\t"$1}' > intron_length_distribution.txt
sed -i '1s/^/intron_length\tcount\n/' intron_length_distribution.txt 
awk '$3~/exon/{print $5-$4+1}' $inputDir/${label[$SLURM_ARRAY_TASK_ID]}.gtf |sort -n|uniq -c|awk '{print $2"\t"$1}' > exon_length_distribution.txt
sed -i '1s/^/exon_length\tcount\n/' txt/exon_length_distribution.txt

perl ~/scripts/lengthFilter4fa.pl $inputDir/${label[$SLURM_ARRAY_TASK_ID]}.codingseq.fa 0 ${label[$SLURM_ARRAY_TASK_ID]}_tmp.fa |tr -d '>' > transcript_length.txt
rm ${label[$SLURM_ARRAY_TASK_ID]}_tmp.fa
cut -f2 transcript_length.txt |sort -n|uniq -c|awk '{print $2"\t"$1}' > transcript_length_distribution.txt 
sed -i '1s/^/transcript_length\tcount\n/' transcript_length_distribution.txt

