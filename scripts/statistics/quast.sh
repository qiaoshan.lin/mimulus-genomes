#!/bin/bash
#SBATCH --job-name=quast
#SBATCH -c 4
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-3
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load quast

genome=(/home/CAM/qlin/resource/CE10/CE10g_v2.0_nucleus.fa /home/CAM/qlin/resource/LF10/LF10g_v2.0_nucleus.fa /home/CAM/qlin/resource/Mpar/Mparg_v2.0_nucleus.fa /home/CAM/qlin/resource/MvBL/MvBLg_v2.0_nucleus.fa)
label=(CE10 LF10 Mpar MvBL)
gff=(/home/CAM/qlin/resource/CE10/CE10g_v2.0.gff2 /home/CAM/qlin/resource/LF10/LF10g_v2.0.gff2 /home/CAM/qlin/resource/Mpar/Mparg_v2.0.gff2 /home/CAM/qlin/resource/MvBL/MvBLg_v2.0.gff2)
est=(448174393 423111902 380772658 452571273)

quast.py \
 -o ~/Mimulus_Genomes/results/statistics/quast/${label[$SLURM_ARRAY_TASK_ID]}\
 -g ${gff[$SLURM_ARRAY_TASK_ID]} -t 4 -s -e -k --rna-finding --est-ref-size ${est[$SLURM_ARRAY_TASK_ID]} --plots-format eps ${genome[$SLURM_ARRAY_TASK_ID]}



