#!/bin/bash
#SBATCH --job-name=single_copy_gene
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH -a 0-3
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load bowtie2/2.3.1 
module load samtools/1.7
module load bamtools/2.4.1

workingDir=/home/CAM/qlin/Mimulus_Genomes/results/statistics/single_copy_gene
cd $workingDir

label=(LF10 CE10 Mpar MvBL)

genes=(LF10_single_copy_genes.fa CE10_single_copy_genes.fa Mpar_single_copy_genes.fa MvBL_single_copy_genes.fa)

reads1=(/home/CAM/qlin/Mimulus_Genomes/results/assembly/LF10/LF10_3_500bp_L8_1_paired.fq.gz /home/CAM/qlin/Mimulus_Genomes/results/assembly/CE10/CE10_HFGJFALXX_L6_1_paired.fq.gz /home/CAM/qlin/Mimulus_Genomes/results/assembly/Mpar/Mpar_HFGJFALXX_L6_1_paired.fq.gz /home/CAM/qlin/Mimulus_Genomes/results/assembly/MvBL/MVBL_HFGJFALXX_L6_1_paired.fq.gz)
reads2=(/home/CAM/qlin/Mimulus_Genomes/results/assembly/LF10/LF10_3_500bp_L8_2_paired.fq.gz /home/CAM/qlin/Mimulus_Genomes/results/assembly/CE10/CE10_HFGJFALXX_L6_2_paired.fq.gz /home/CAM/qlin/Mimulus_Genomes/results/assembly/Mpar/Mpar_HFGJFALXX_L6_2_paired.fq.gz /home/CAM/qlin/Mimulus_Genomes/results/assembly/MvBL/MVBL_HFGJFALXX_L6_2_paired.fq.gz)

ref=${genes[$SLURM_ARRAY_TASK_ID]}

bowtie2-build --quiet --threads 8 $ref ${label[$SLURM_ARRAY_TASK_ID]}_ref

bowtie2 --threads 8 --phred33 -x ${label[$SLURM_ARRAY_TASK_ID]}_ref -1 ${reads1[$SLURM_ARRAY_TASK_ID]} -2 ${reads2[$SLURM_ARRAY_TASK_ID]} -S ${label[$SLURM_ARRAY_TASK_ID]}.sam

samtools view -bT $ref -@ 8 ${label[$SLURM_ARRAY_TASK_ID]}.sam > ${label[$SLURM_ARRAY_TASK_ID]}.bam
rm ${label[$SLURM_ARRAY_TASK_ID]}.sam

samtools sort -T ${label[$SLURM_ARRAY_TASK_ID]}_tmp -@ 8 -o ${label[$SLURM_ARRAY_TASK_ID]}.sort.bam ${label[$SLURM_ARRAY_TASK_ID]}.bam
rm ${label[$SLURM_ARRAY_TASK_ID]}.bam

samtools depth -aa ${label[$SLURM_ARRAY_TASK_ID]}.sort.bam > ${label[$SLURM_ARRAY_TASK_ID]}.depth
rm ${label[$SLURM_ARRAY_TASK_ID]}.sort.bam

awk '{s+=$3}END{print s/NR}' ${label[$SLURM_ARRAY_TASK_ID]}.depth > ${label[$SLURM_ARRAY_TASK_ID]}.txt




