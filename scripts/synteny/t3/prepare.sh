awk '$3~/transcript/{print $1,$4,$5,$10,$7}' OFS='\t' LF10g_v2.0beta.gtf |tr -d '"'|tr -d ';'|tr '.' '_'|awk '{print $1,$2,$3,$4,".",$5}' OFS='\t' > LF10.bed
awk '$3~/transcript/{print $1,$4,$5,$10,$7}' OFS='\t' CE10g_v2.0beta.gtf |tr -d '"'|tr -d ';'|tr '.' '_'|awk '{print $1,$2,$3,$4,".",$5}' OFS='\t' > CE10.bed
awk '$3~/transcript/{print $1,$4,$5,$10,$7}' OFS='\t' Mparg_v2.0beta.gtf |tr -d '"'|tr -d ';'|tr '.' '_'|awk '{print $1,$2,$3,$4,".",$5}' OFS='\t' > Mpar.bed
awk '$3~/transcript/{print $1,$4,$5,$10,$7}' OFS='\t' MvBLg_v2.0beta.gtf |tr -d '"'|tr -d ';'|tr '.' '_'|awk '{print $1,$2,$3,$4,".",$5}' OFS='\t' > MvBL.bed
perl -lane 'if( !/^#/ && $F[2] eq "mRNA" ){$F[8]=~/^ID=(.*?).v2.0;.*/;$tmp=$1;$tmp=~s/\./_/g;print "$F[0]\t$F[3]\t$F[4]\t$tmp\t.\t$F[6]"}' Mguttatus_256_v2.0.gene.gff3 > Mgut.bed

cat CE10g_v2.0beta.longest_isoform.codingseq.fa | perl -lane 'if (/^>/) {$_=~s/\./_/} print' > CE10.cds
cat LF10g_v2.0beta.longest_isoform.codingseq.fa | perl -lane 'if (/^>/) {$_=~s/\./_/} print' > LF10.cds
cat Mparg_v2.0beta.longest_isoform.codingseq.fa | perl -lane 'if (/^>/) {$_=~s/\./_/} print' > Mpar.cds
cat MvBLg_v2.0beta.longest_isoform.codingseq.fa | perl -lane 'if (/^>/) {$_=~s/\./_/} print' > MvBL.cds
cat Mguttatus_256_v2.0.cds_primaryTranscriptOnly.fa | perl -lane 'if (/^>/) {$_=~s/\s.*$//; $_=~s/\./_/g} print' > Mgut.cds

python -m jcvi.compara.catalog ortholog Mpar MvBL --cscore=.99
python -m jcvi.compara.catalog ortholog LF10 CE10 --cscore=.99
python -m jcvi.compara.catalog ortholog CE10 Mpar --cscore=.99
python -m jcvi.compara.catalog ortholog LF10 Mgut --cscore=.99
python -m jcvi.compara.synteny screen --minspan=30 --simple LF10.CE10.anchors LF10.CE10.anchors.new
python -m jcvi.compara.synteny screen --minspan=30 --simple CE10.Mpar.anchors CE10.Mpar.anchors.new
python -m jcvi.compara.synteny screen --minspan=30 --simple Mpar.MvBL.anchors Mpar.MvBL.anchors.new
python -m jcvi.compara.synteny screen --minspan=30 --simple LF10.Mgut.anchors LF10.Mgut.anchors.new

