ml1=FF9519
mc1=C06DEB
mp1=5DC500
mv1=41A3F0
mg1=FF6A5F

######################################################## mark LF10 specific inversions start ########################################################

awk -v color="$ml1" '{if ($0~/ML3G231500_1\tML3G240300_1\tMC3G240000_1\tMC3G247700_1/) print "#"color"*"$0;else print}' LF10.CE10.anchors.simple > tmp && mv tmp LF10.CE10.anchors.simple
awk -v color="$ml1" '{if ($0~/ML3G253800_1\tML3G259100_1\tMC3G261700_1\tMC3G268200_1/) print "#"color"*"$0;else print}' LF10.CE10.anchors.simple > tmp && mv tmp LF10.CE10.anchors.simple
awk -v color="$ml1" '{if ($0~/ML4G264700_1\tML4G284300_1\tMC4G266400_1\tMC4G284400_1/) print "#"color"*"$0;else print}' LF10.CE10.anchors.simple > tmp && mv tmp LF10.CE10.anchors.simple
awk -v color="$ml1" '{if ($0~/ML4G118200_1\tML4G123600_1\tMC4G129700_1\tMC4G134900_1/) print "#"color"*"$0;else print}' LF10.CE10.anchors.simple > tmp && mv tmp LF10.CE10.anchors.simple
awk -v color="$ml1" '{if ($0~/ML5G101100_1\tML5G110600_1\tMC5G097700_1\tMC5G107700_1/) print "#"color"*"$0;else print}' LF10.CE10.anchors.simple > tmp && mv tmp LF10.CE10.anchors.simple
awk -v color="$ml1" '{if ($0~/ML5G132500_1\tML5G137400_1\tMC5G127300_1\tMC5G133300_1/) print "#"color"*"$0;else print}' LF10.CE10.anchors.simple > tmp && mv tmp LF10.CE10.anchors.simple
awk -v color="$ml1" '{if ($0~/ML6G362300_1\tML6G397300_1\tMC6G359500_1\tMC6G395800_1/) print "#"color"*"$0;else print}' LF10.CE10.anchors.simple > tmp && mv tmp LF10.CE10.anchors.simple
awk -v color="$ml1" '{if ($0~/ML6G130300_1\tML6G137400_1\tMC6G126500_1\tMC6G134300_1/) print "#"color"*"$0;else print}' LF10.CE10.anchors.simple > tmp && mv tmp LF10.CE10.anchors.simple
awk -v color="$ml1" '{if ($0~/ML7G230300_1\tML7G242100_1\tMC7G381900_1\tMC7G392700_1/) print "#"color"*"$0;else print}' LF10.CE10.anchors.simple > tmp && mv tmp LF10.CE10.anchors.simple
awk -v color="$ml1" '{if ($0~/ML7G182700_1\tML7G187800_1\tMC7G331500_1\tMC7G335800_1/) print "#"color"*"$0;else print}' LF10.CE10.anchors.simple > tmp && mv tmp LF10.CE10.anchors.simple
awk -v color="$ml1" '{if ($0~/ML8G164300_1\tML8G176400_2\tMC8G284900_1\tMC8G298700_1/) print "#"color"*"$0;else print}' LF10.CE10.anchors.simple > tmp && mv tmp LF10.CE10.anchors.simple
awk -v color="$ml1" '{if ($0~/ML8G133300_1\tML8G141700_1\tMC8G252300_1\tMC8G261100_1/) print "#"color"*"$0;else print}' LF10.CE10.anchors.simple > tmp && mv tmp LF10.CE10.anchors.simple

######################################################## mark LF10 specific inversions end ########################################################



######################################################## mark CE10 specific inversions start ########################################################
awk -v color="$mc1" '{if ($0~/ML1G176300_1\tML1G192500_1\tMC1G174900_1\tMC1G192700_1/) print "#"color"*"$0;else print}' LF10.CE10.anchors.simple > tmp && mv tmp LF10.CE10.anchors.simple
awk -v color="$mc1" '{if ($0~/ML1G192800_1\tML1G202000_1\tMC1G204500_1\tMC1G213000_1/) print "#"color"*"$0;else print}' LF10.CE10.anchors.simple > tmp && mv tmp LF10.CE10.anchors.simple
awk -v color="$mc1" '{if ($0~/ML1G204800_2\tML1G208000_1\tMC1G169600_1\tMC1G173100_2/) print "#"color"*"$0;else print}' LF10.CE10.anchors.simple > tmp && mv tmp LF10.CE10.anchors.simple
awk -v color="$mc1" '{if ($0~/ML2G250200_1\tML2G256700_1\tMC2G274700_2\tMC2G281900_1/) print "#"color"*"$0;else print}' LF10.CE10.anchors.simple > tmp && mv tmp LF10.CE10.anchors.simple
awk -v color="$mc1" '{if ($0~/ML4G176600_1\tML4G180500_1\tMC4G160700_1\tMC4G165400_1/) print "#"color"*"$0;else print}' LF10.CE10.anchors.simple > tmp && mv tmp LF10.CE10.anchors.simple
awk -v color="$mc1" '{if ($0~/ML7G215900_1\tML7G227700_1\tMC7G366800_1\tMC7G380300_1/) print "#"color"*"$0;else print}' LF10.CE10.anchors.simple > tmp && mv tmp LF10.CE10.anchors.simple

awk -v color="$mc1" '{if ($0~/MC1G204500_1\tMC1G213000_1\tMP1G179900_1\tMP1G187300_1/) print "#"color"*"$0;else print}' CE10.Mpar.anchors.simple > tmp && mv tmp CE10.Mpar.anchors.simple
awk -v color="$mc1" '{if ($0~/MC1G173400_1\tMC1G192100_1\tMP1G161400_1\tMP1G178600_1/) print "#"color"*"$0;else print}' CE10.Mpar.anchors.simple > tmp && mv tmp CE10.Mpar.anchors.simple
awk -v color="$mc1" '{if ($0~/MC1G168600_1\tMC1G173100_2\tMP1G190600_2\tMP1G194200_2/) print "#"color"*"$0;else print}' CE10.Mpar.anchors.simple > tmp && mv tmp CE10.Mpar.anchors.simple
awk -v color="$mc1" '{if ($0~/MC2G274700_2\tMC2G281900_1\tMP2G240500_1\tMP2G248300_2/) print "#"color"*"$0;else print}' CE10.Mpar.anchors.simple > tmp && mv tmp CE10.Mpar.anchors.simple
awk -v color="$mc1" '{if ($0~/MC4G160700_1\tMC4G164500_1\tMP4G169000_1\tMP4G172800_1/) print "#"color"*"$0;else print}' CE10.Mpar.anchors.simple > tmp && mv tmp CE10.Mpar.anchors.simple
awk -v color="$mc1" '{if ($0~/MC7G366800_1\tMC7G380300_1\tMP7G202600_1\tMP7G214700_1/) print "#"color"*"$0;else print}' CE10.Mpar.anchors.simple > tmp && mv tmp CE10.Mpar.anchors.simple

######################################################## mark CE10 specific inversions end ########################################################



######################################################## mark Mpar specific inversions start ########################################################

awk -v color="$mp1" '{if ($0~/MC1G307900_1\tMC1G313700_1\tMP1G277800_1\tMP1G282200_1/)print "#"color"*"$0;else print}' CE10.Mpar.anchors.simple > tmp && mv tmp CE10.Mpar.anchors.simple
awk -v color="$mp1" '{if ($0~/MC2G461100_1\tMC2G466400_1\tMP2G416800_1\tMP2G422500_1/)print "#"color"*"$0;else print}' CE10.Mpar.anchors.simple > tmp && mv tmp CE10.Mpar.anchors.simple
awk -v color="$mp1" '{if ($0~/MC3G246000_1\tMC3G252200_1\tMP3G235400_1\tMP3G242600_1/)print "#"color"*"$0;else print}' CE10.Mpar.anchors.simple > tmp && mv tmp CE10.Mpar.anchors.simple
awk -v color="$mp1" '{if ($0~/MC5G113200_1\tMC5G119300_1\tMP5G110700_1\tMP5G117100_1/)print "#"color"*"$0;else print}' CE10.Mpar.anchors.simple > tmp && mv tmp CE10.Mpar.anchors.simple
awk -v color="$mp1" '{if ($0~/MC5G207800_1\tMC5G215500_1\tMP5G213200_1\tMP5G218700_1/)print "#"color"*"$0;else print}' CE10.Mpar.anchors.simple > tmp && mv tmp CE10.Mpar.anchors.simple
awk -v color="$mp1" '{if ($0~/MC8G217600_1\tMC8G229900_1\tMP8G200100_1\tMP8G212100_1/)print "#"color"*"$0;else print}' CE10.Mpar.anchors.simple > tmp && mv tmp CE10.Mpar.anchors.simple
awk -v color="$mp1" '{if ($0~/MC8G211600_1\tMC8G217400_1\tMP8G216500_1\tMP8G221500_1/)print "#"color"*"$0;else print}' CE10.Mpar.anchors.simple > tmp && mv tmp CE10.Mpar.anchors.simple

awk -v color="$mp1" '{if ($0~/MP1G277800_1\tMP1G286300_1\tMV1G297100_1\tMV1G305300_1/)print "#"color"*"$0;else print}' Mpar.MvBL.anchors.simple > tmp && mv tmp Mpar.MvBL.anchors.simple
awk -v color="$mp1" '{if ($0~/MP2G416900_1\tMP2G422500_1\tMV2G430300_1\tMV2G436300_1/)print "#"color"*"$0;else print}' Mpar.MvBL.anchors.simple > tmp && mv tmp Mpar.MvBL.anchors.simple
awk -v color="$mp1" '{if ($0~/MP3G235500_1\tMP3G242600_1\tMV3G234800_1\tMV3G241000_1/)print "#"color"*"$0;else print}' Mpar.MvBL.anchors.simple > tmp && mv tmp Mpar.MvBL.anchors.simple
awk -v color="$mp1" '{if ($0~/MP5G110200_2\tMP5G117000_1\tMV5G113100_1\tMV5G118900_1/)print "#"color"*"$0;else print}' Mpar.MvBL.anchors.simple > tmp && mv tmp Mpar.MvBL.anchors.simple
awk -v color="$mp1" '{if ($0~/MP5G214400_1\tMP5G218700_1\tMV5G208500_1\tMV5G211500_2/)print "#"color"*"$0;else print}' Mpar.MvBL.anchors.simple > tmp && mv tmp Mpar.MvBL.anchors.simple
awk -v color="$mp1" '{if ($0~/MP8G202100_1\tMP8G207900_1\tMV8G213100_2\tMV8G218000_1/)print "#"color"*"$0;else print}' Mpar.MvBL.anchors.simple > tmp && mv tmp Mpar.MvBL.anchors.simple
awk -v color="$mp1" '{if ($0~/MP8G216800_1\tMP8G221500_1\tMV8G209100_1\tMV8G212900_1/)print "#"color"*"$0;else print}' Mpar.MvBL.anchors.simple > tmp && mv tmp Mpar.MvBL.anchors.simple

######################################################## mark Mpar specific inversions end ########################################################



######################################################## mark MvBL specific inversions start ########################################################
awk -v color="$mv1" '{if ($0~/MP1G032400_1\tMP1G045500_1\tMV1G034500_1\tMV1G053100_1/) print "#"color"*"$0;else print}' Mpar.MvBL.anchors.simple > tmp && mv tmp Mpar.MvBL.anchors.simple
awk -v color="$mv1" '{if ($0~/MP2G407600_1\tMP2G411300_1\tMV2G420200_1\tMV2G424500_1/) print "#"color"*"$0;else print}' Mpar.MvBL.anchors.simple > tmp && mv tmp Mpar.MvBL.anchors.simple
awk -v color="$mv1" '{if ($0~/MP2G117300_1\tMP2G122100_1\tMV2G123800_1\tMV2G128800_1/) print "#"color"*"$0;else print}' Mpar.MvBL.anchors.simple > tmp && mv tmp Mpar.MvBL.anchors.simple
awk -v color="$mv1" '{if ($0~/MP4G216300_1\tMP4G222300_1\tMV4G225300_1\tMV4G232100_1/) print "#"color"*"$0;else print}' Mpar.MvBL.anchors.simple > tmp && mv tmp Mpar.MvBL.anchors.simple
awk -v color="$mv1" '{if ($0~/MP5G228100_1\tMP5G240200_1\tMV5G222800_1\tMV5G235800_2/) print "#"color"*"$0;else print}' Mpar.MvBL.anchors.simple > tmp && mv tmp Mpar.MvBL.anchors.simple
awk -v color="$mv1" '{if ($0~/MP5G219300_1\tMP5G228700_1\tMV5G212800_1\tMV5G220800_1/) print "#"color"*"$0;else print}' Mpar.MvBL.anchors.simple > tmp && mv tmp Mpar.MvBL.anchors.simple
awk -v color="$mv1" '{if ($0~/MP7G124600_1\tMP7G135600_1\tMV7G265600_1\tMV7G276300_1/) print "#"color"*"$0;else print}' Mpar.MvBL.anchors.simple > tmp && mv tmp Mpar.MvBL.anchors.simple

######################################################## mark MvBL specific inversions start ########################################################

python -m jcvi.graphics.karyotype seqids layout


