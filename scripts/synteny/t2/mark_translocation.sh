cut -f1 LF10.CE10.anchors.simple > tmp1
cut -f2 LF10.CE10.anchors.simple > tmp2
cut -f3 LF10.CE10.anchors.simple > tmp3
cut -f4 LF10.CE10.anchors.simple > tmp4
array=()
while read line; do   array+=("$line");   done < tmp1 
for a in "${array[@]}"; do grep $a LF10.bed |head -1>> tmp_out1; done
array=()
while read line; do   array+=("$line");   done < tmp2
for a in "${array[@]}"; do grep $a LF10.bed |head -1>> tmp_out2; done
array=()
while read line; do   array+=("$line");   done < tmp3
for a in "${array[@]}"; do grep $a CE10.bed |head -1>> tmp_out3; done
array=()
while read line; do   array+=("$line");   done < tmp4
for a in "${array[@]}"; do grep $a CE10.bed |head -1>> tmp_out4; done
cut -f1 tmp_out1 > tmp1 
cut -f1 tmp_out2 > tmp2 
cut -f1 tmp_out3 > tmp3 
cut -f1 tmp_out4 > tmp4 
paste tmp1 tmp2 tmp3 tmp4 > tmp
perl -lane '@block = ( $_ =~ /chr(\d+)/g ); print $. if $block[0]!=$block[2]' tmp > line
rm tmp*
array=()
while read line; do   array+=("$line");   done < line
for a in "${array[@]}"; do sed -n "$a"p LF10.CE10.anchors.simple >> tmp; done
cp LF10.CE10.anchors.simple tmpp
for a in "${array[@]}"; do awk -v var="$a" '{if(NR==var) print "#FF9519*"$0;else print}' tmpp > tmppp; mv tmppp tmpp;done
mv tmpp LF10.CE10.anchors.simple

cut -f1 LF10.Mpar.anchors.simple > tmp1
cut -f2 LF10.Mpar.anchors.simple > tmp2
cut -f3 LF10.Mpar.anchors.simple > tmp3
cut -f4 LF10.Mpar.anchors.simple > tmp4
array=()
while read line; do   array+=("$line");   done < tmp1 
for a in "${array[@]}"; do grep $a LF10.bed |head -1>> tmp_out1; done
array=()
while read line; do   array+=("$line");   done < tmp2
for a in "${array[@]}"; do grep $a LF10.bed |head -1>> tmp_out2; done
array=()
while read line; do   array+=("$line");   done < tmp3
for a in "${array[@]}"; do grep $a Mpar.bed |head -1>> tmp_out3; done
array=()
while read line; do   array+=("$line");   done < tmp4
for a in "${array[@]}"; do grep $a Mpar.bed |head -1>> tmp_out4; done
cut -f1 tmp_out1 > tmp1 
cut -f1 tmp_out2 > tmp2 
cut -f1 tmp_out3 > tmp3 
cut -f1 tmp_out4 > tmp4 
paste tmp1 tmp2 tmp3 tmp4 > tmp
perl -lane '@block = ( $_ =~ /chr(\d+)/g ); print $. if $block[0]!=$block[2]' tmp > line
rm tmp*
array=()
while read line; do   array+=("$line");   done < line
for a in "${array[@]}"; do sed -n "$a"p LF10.Mpar.anchors.simple >> tmp; done
cp LF10.Mpar.anchors.simple tmpp
for a in "${array[@]}"; do awk -v var="$a" '{if(NR==var) print "#FF9519*"$0;else print}' tmpp > tmppp; mv tmppp tmpp;done
mv tmpp LF10.Mpar.anchors.simple

cut -f1 CE10.Mpar.anchors.simple > tmp1
cut -f2 CE10.Mpar.anchors.simple > tmp2
cut -f3 CE10.Mpar.anchors.simple > tmp3
cut -f4 CE10.Mpar.anchors.simple > tmp4
array=()
while read line; do   array+=("$line");   done < tmp1 
for a in "${array[@]}"; do grep $a CE10.bed |head -1>> tmp_out1; done
array=()
while read line; do   array+=("$line");   done < tmp2
for a in "${array[@]}"; do grep $a CE10.bed |head -1>> tmp_out2; done
array=()
while read line; do   array+=("$line");   done < tmp3
for a in "${array[@]}"; do grep $a Mpar.bed |head -1>> tmp_out3; done
array=()
while read line; do   array+=("$line");   done < tmp4
for a in "${array[@]}"; do grep $a Mpar.bed |head -1>> tmp_out4; done
cut -f1 tmp_out1 > tmp1 
cut -f1 tmp_out2 > tmp2 
cut -f1 tmp_out3 > tmp3 
cut -f1 tmp_out4 > tmp4 
paste tmp1 tmp2 tmp3 tmp4 > tmp
perl -lane '@block = ( $_ =~ /chr(\d+)/g ); print $. if $block[0]!=$block[2]' tmp > line
rm tmp*
array=()
while read line; do   array+=("$line");   done < line
for a in "${array[@]}"; do sed -n "$a"p CE10.Mpar.anchors.simple >> tmp; done
cp CE10.Mpar.anchors.simple tmpp
for a in "${array[@]}"; do awk -v var="$a" '{if(NR==var) print "#C06DEB*"$0;else print}' tmpp > tmppp; mv tmppp tmpp;done
mv tmpp CE10.Mpar.anchors.simple

cut -f1 CE10.MvBL.anchors.simple > tmp1
cut -f2 CE10.MvBL.anchors.simple > tmp2
cut -f3 CE10.MvBL.anchors.simple > tmp3
cut -f4 CE10.MvBL.anchors.simple > tmp4
array=()
while read line; do   array+=("$line");   done < tmp1 
for a in "${array[@]}"; do grep $a CE10.bed |head -1>> tmp_out1; done
array=()
while read line; do   array+=("$line");   done < tmp2
for a in "${array[@]}"; do grep $a CE10.bed |head -1>> tmp_out2; done
array=()
while read line; do   array+=("$line");   done < tmp3
for a in "${array[@]}"; do grep $a MvBL.bed |head -1>> tmp_out3; done
array=()
while read line; do   array+=("$line");   done < tmp4
for a in "${array[@]}"; do grep $a MvBL.bed |head -1>> tmp_out4; done
cut -f1 tmp_out1 > tmp1 
cut -f1 tmp_out2 > tmp2 
cut -f1 tmp_out3 > tmp3 
cut -f1 tmp_out4 > tmp4 
paste tmp1 tmp2 tmp3 tmp4 > tmp
perl -lane '@block = ( $_ =~ /chr(\d+)/g ); print $. if $block[0]!=$block[2]' tmp > line
rm tmp*
array=()
while read line; do   array+=("$line");   done < line
for a in "${array[@]}"; do sed -n "$a"p CE10.MvBL.anchors.simple >> tmp; done
cp CE10.MvBL.anchors.simple tmpp
for a in "${array[@]}"; do awk -v var="$a" '{if(NR==var) print "#C06DEB*"$0;else print}' tmpp > tmppp; mv tmppp tmpp;done
mv tmpp CE10.MvBL.anchors.simple

cut -f1 Mpar.MvBL.anchors.simple > tmp1
cut -f2 Mpar.MvBL.anchors.simple > tmp2
cut -f3 Mpar.MvBL.anchors.simple > tmp3
cut -f4 Mpar.MvBL.anchors.simple > tmp4
array=()
while read line; do   array+=("$line");   done < tmp1 
for a in "${array[@]}"; do grep $a Mpar.bed |head -1>> tmp_out1; done
array=()
while read line; do   array+=("$line");   done < tmp2
for a in "${array[@]}"; do grep $a Mpar.bed |head -1>> tmp_out2; done
array=()
while read line; do   array+=("$line");   done < tmp3
for a in "${array[@]}"; do grep $a MvBL.bed |head -1>> tmp_out3; done
array=()
while read line; do   array+=("$line");   done < tmp4
for a in "${array[@]}"; do grep $a MvBL.bed |head -1>> tmp_out4; done
cut -f1 tmp_out1 > tmp1 
cut -f1 tmp_out2 > tmp2 
cut -f1 tmp_out3 > tmp3 
cut -f1 tmp_out4 > tmp4 
paste tmp1 tmp2 tmp3 tmp4 > tmp
perl -lane '@block = ( $_ =~ /chr(\d+)/g ); print $. if $block[0]!=$block[2]' tmp > line
rm tmp*
array=()
while read line; do   array+=("$line");   done < line
for a in "${array[@]}"; do sed -n "$a"p Mpar.MvBL.anchors.simple >> tmp; done
cp Mpar.MvBL.anchors.simple tmpp
for a in "${array[@]}"; do awk -v var="$a" '{if(NR==var) print "#41A3F0*"$0;else print}' tmpp > tmppp; mv tmppp tmpp;done
mv tmpp Mpar.MvBL.anchors.simple

cut -f1 MvBL.LF10.anchors.simple > tmp1
cut -f2 MvBL.LF10.anchors.simple > tmp2
cut -f3 MvBL.LF10.anchors.simple > tmp3
cut -f4 MvBL.LF10.anchors.simple > tmp4
array=()
while read line; do   array+=("$line");   done < tmp1
for a in "${array[@]}"; do grep $a MvBL.bed |head -1>> tmp_out1; done
array=()
while read line; do   array+=("$line");   done < tmp2
for a in "${array[@]}"; do grep $a MvBL.bed |head -1>> tmp_out2; done
array=()
while read line; do   array+=("$line");   done < tmp3
for a in "${array[@]}"; do grep $a LF10.bed |head -1>> tmp_out3; done
array=()
while read line; do   array+=("$line");   done < tmp4
for a in "${array[@]}"; do grep $a LF10.bed |head -1>> tmp_out4; done
cut -f1 tmp_out1 > tmp1
cut -f1 tmp_out2 > tmp2
cut -f1 tmp_out3 > tmp3
cut -f1 tmp_out4 > tmp4
paste tmp1 tmp2 tmp3 tmp4 > tmp
perl -lane '@block = ( $_ =~ /chr(\d+)/g ); print $. if $block[0]!=$block[2]' tmp > line
rm tmp*
array=()
while read line; do   array+=("$line");   done < line
for a in "${array[@]}"; do sed -n "$a"p MvBL.LF10.anchors.simple >> tmp; done
cp MvBL.LF10.anchors.simple tmpp
for a in "${array[@]}"; do awk -v var="$a" '{if(NR==var) print "#41A3F0*"$0;else print}' tmpp > tmppp; mv tmppp tmpp;done
mv tmpp MvBL.LF10.anchors.simple

python -m jcvi.graphics.karyotype seqids layout

rm line tmp
