perl -le 'print "synteny region of target 1\t\t\t\t\t\t\t\t\t\tsynteny region of target 2\t\t\t\t\t\t\t\t\t\tsynteny score\tsynteny direction\nleft boundary\t\t\t\t\tright boundary\t\t\t\t\tleft boundary\t\t\t\t\tright boundary\t\t\t\t\t\t\n"' > head.tmp
for pair in LF10.CE10 LF10.Mpar MvBL.LF10 CE10.Mpar CE10.MvBL Mpar.MvBL
do
	A="$(echo "$pair" | cut -d'.' -f1)"
	B="$(echo "$pair" | cut -d'.' -f2)"

	cut -f1 $pair.anchors.simple > tmp1
	cut -f2 $pair.anchors.simple > tmp2
	cut -f3 $pair.anchors.simple > tmp3
	cut -f4 $pair.anchors.simple > tmp4
	cut -f5 $pair.anchors.simple > out5
	cut -f6 $pair.anchors.simple > out6

	array=()
	while read line; do   array+=("$line");   done < tmp1 
	for a in "${array[@]}"; do grep $a $A.bed |head -1|cut -f1-4,6 >> out1; done
	array=()
	while read line; do   array+=("$line");   done < tmp2
	for a in "${array[@]}"; do grep $a $A.bed |head -1|cut -f1-4,6 >> out2; done
	array=()
	while read line; do   array+=("$line");   done < tmp3
	for a in "${array[@]}"; do grep $a $B.bed |head -1|cut -f1-4,6 >> out3; done
	array=()
	while read line; do   array+=("$line");   done < tmp4
	for a in "${array[@]}"; do grep $a $B.bed |head -1|cut -f1-4,6 >> out4; done

	paste out1 out2 out3 out4 out5 out6 > $pair.txt
	rm out* tmp*

	cat head.tmp $pair.txt > tmp
	mv tmp $pair.txt
done

rm head.tmp

