awk '$3~/transcript/{print $1,$4,$5,$10,$7}' OFS='\t' LF10g_v2.0beta.gtf |tr -d '"'|tr -d ';'|tr '.' '_'|awk '{print $1,$2,$3,$4,".",$5}' OFS='\t' > LF10.bed
awk '$3~/transcript/{print $1,$4,$5,$10,$7}' OFS='\t' CE10g_v2.0beta.gtf |tr -d '"'|tr -d ';'|tr '.' '_'|awk '{print $1,$2,$3,$4,".",$5}' OFS='\t' > CE10.bed
awk '$3~/transcript/{print $1,$4,$5,$10,$7}' OFS='\t' Mparg_v2.0beta.gtf |tr -d '"'|tr -d ';'|tr '.' '_'|awk '{print $1,$2,$3,$4,".",$5}' OFS='\t' > Mpar.bed
awk '$3~/transcript/{print $1,$4,$5,$10,$7}' OFS='\t' MvBLg_v2.0beta.gtf |tr -d '"'|tr -d ';'|tr '.' '_'|awk '{print $1,$2,$3,$4,".",$5}' OFS='\t' > MvBL.bed

cat CE10g_v2.0beta.longest_isoform.codingseq.fa | perl -lane 'if (/^>/) {$_=~s/\./_/} print' > CE10.cds
cat LF10g_v2.0beta.longest_isoform.codingseq.fa | perl -lane 'if (/^>/) {$_=~s/\./_/} print' > LF10.cds
cat Mparg_v2.0beta.longest_isoform.codingseq.fa | perl -lane 'if (/^>/) {$_=~s/\./_/} print' > Mpar.cds
cat MvBLg_v2.0beta.longest_isoform.codingseq.fa | perl -lane 'if (/^>/) {$_=~s/\./_/} print' > MvBL.cds

python -m jcvi.compara.catalog ortholog LF10 CE10 --cscore=.99
python -m jcvi.compara.catalog ortholog CE10 Mpar --cscore=.99
python -m jcvi.compara.catalog ortholog Mpar MvBL --cscore=.99
python -m jcvi.compara.catalog ortholog MvBL LF10 --cscore=.99

python -m jcvi.compara.catalog ortholog LF10 Mpar --cscore=.99
python -m jcvi.compara.catalog ortholog CE10 MvBL --cscore=.99

python -m jcvi.compara.synteny screen --minspan=30 --simple LF10.CE10.anchors LF10.CE10.anchors.new
python -m jcvi.compara.synteny screen --minspan=30 --simple CE10.Mpar.anchors CE10.Mpar.anchors.new
python -m jcvi.compara.synteny screen --minspan=30 --simple Mpar.MvBL.anchors Mpar.MvBL.anchors.new
python -m jcvi.compara.synteny screen --minspan=30 --simple MvBL.LF10.anchors MvBL.LF10.anchors.new
python -m jcvi.compara.synteny screen --minspan=30 --simple LF10.Mpar.anchors LF10.Mpar.anchors.new
python -m jcvi.compara.synteny screen --minspan=30 --simple CE10.MvBL.anchors CE10.MvBL.anchors.new

