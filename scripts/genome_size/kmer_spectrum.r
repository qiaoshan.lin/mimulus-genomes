library(ggplot2)
library(gridExtra)
directory <- "/Volumes/GoogleDrive/My\ Drive/Qiaoshan/Genome_assembly_annotation/genome_size_estimation/"
setwd(directory)

df <- read.table("lewisii_kmer_histo/LF10_33mer_out.histo")
f1 <- ggplot(data=df, aes(x=V1, y=V2)) +
  geom_line() + 
  scale_x_continuous(trans='log10') +
  scale_y_continuous(trans='log10') +
  theme_bw() +
  ggtitle("lewisii genome 33-mer spectrum") +
  xlab("lg kmer freq") + 
  ylab("lg kmer count")

df <- read.table("cardinalis_kmer_histo/CE10_33mer_out.histo")
f2 <- ggplot(data=df, aes(x=V1, y=V2)) +
  geom_line() + 
  scale_x_continuous(trans='log10') +
  scale_y_continuous(trans='log10') +
  theme_bw() +
  ggtitle("cardinalis genome 33-mer spectrum") +
  xlab("lg kmer freq") + 
  ylab("lg kmer count")

df <- read.table("parishii_kmer_histo/Mpar_33mer_out.histo")
f3 <- ggplot(data=df, aes(x=V1, y=V2)) +
  geom_line() + 
  scale_x_continuous(trans='log10') +
  scale_y_continuous(trans='log10') +
  theme_bw() +
  ggtitle("parishii genome 33-mer spectrum") +
  xlab("lg kmer freq") + 
  ylab("lg kmer count")

df <- read.table("verbenaceous_kmer_histo/MvBL_33mer_out.histo")
f4 <- ggplot(data=df, aes(x=V1, y=V2)) +
  geom_line() + 
  scale_x_continuous(trans='log10') +
  scale_y_continuous(trans='log10') +
  theme_bw() +
  ggtitle("verbenaceous genome 33-mer spectrum") +
  xlab("lg kmer freq") + 
  ylab("lg kmer count")

grid.arrange(f1, f2, f3, f4, nrow = 2)

df <- read.table("lewisii.histo", sep = "\t")
f1 <- ggplot(data=df) +
  geom_line(aes(x = V1, y = V2, color = V3)) + 
  scale_x_continuous(trans='log10') +
  scale_y_continuous(trans='log10') +
  theme_bw() +
  theme(legend.title = element_blank())+
  theme(legend.position = c(0.7, 0.8))+
  ggtitle("lewisii genome 33-mer spectrum") +
  xlab("lg kmer freq") + 
  ylab("lg kmer count")
df <- read.table("cardinalis.histo", sep = "\t")
f2 <- ggplot(data=df) +
  geom_line(aes(x = V1, y = V2, color = V3)) + 
  scale_x_continuous(trans='log10') +
  scale_y_continuous(trans='log10') +
  theme_bw() +
  ggtitle("cardinalis genome 33-mer spectrum") +
  theme(legend.title = element_blank())+
  theme(legend.position = c(0.7, 0.8))+
  xlab("lg kmer freq") + 
  ylab("lg kmer count")
df <- read.table("parishii.histo", sep = "\t")
f3 <- ggplot(data=df) +
  geom_line(aes(x = V1, y = V2, color = V3)) + 
  scale_x_continuous(trans='log10') +
  scale_y_continuous(trans='log10') +
  theme_bw() +
  theme(legend.title = element_blank())+
  theme(legend.position = c(0.7, 0.8))+
  ggtitle("parishii genome 33-mer spectrum") +
  xlab("lg kmer freq") + 
  ylab("lg kmer count")
df <- read.table("verbenaceous.histo", sep = "\t")
f4 <- ggplot(data=df) +
  geom_line(aes(x = V1, y = V2, color = V3)) + 
  scale_x_continuous(trans='log10') +
  scale_y_continuous(trans='log10') +
  theme_bw() +
  theme(legend.title = element_blank())+
  theme(legend.position = c(0.7, 0.8))+
  ggtitle("verbenaceous genome 33-mer spectrum") +
  xlab("lg kmer freq") + 
  ylab("lg kmer count")
grid.arrange(f1, f2, f3, f4, nrow = 2)
