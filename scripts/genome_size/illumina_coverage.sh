#!/bin/bash
#SBATCH --job-name=illumina_coverage
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH -a 0-3
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load bowtie2
module load samtools
module load bamtools

cd /home/CAM/qlin/Mimulus_Genomes/results/genome_size

label=(CE10 LF10 Mpar MvBL)

genome=(/home/CAM/qlin/resource/CE10/CE10g_v2.0.fa /home/CAM/qlin/resource/LF10/LF10g_v2.0.fa /home/CAM/qlin/resource/Mpar/Mparg_v2.0.fa /home/CAM/qlin/resource/MvBL/MvBLg_v2.0.fa)

reads1=(/home/CAM/qlin/Mimulus_Genomes/results/assembly/CE10/CE10_HFGJFALXX_L6_1_paired.fq.gz /home/CAM/qlin/Mimulus_Genomes/results/assembly/LF10/LF10_3_500bp_L8_1_paired.fq.gz /home/CAM/qlin/Mimulus_Genomes/results/assembly/Mpar/Mpar_HFGJFALXX_L6_1_paired.fq.gz /home/CAM/qlin/Mimulus_Genomes/results/assembly/MvBL/MVBL_HFGJFALXX_L6_1_paired.fq.gz)

reads2=(/home/CAM/qlin/Mimulus_Genomes/results/assembly/CE10/CE10_HFGJFALXX_L6_2_paired.fq.gz /home/CAM/qlin/Mimulus_Genomes/results/assembly/LF10/LF10_3_500bp_L8_2_paired.fq.gz /home/CAM/qlin/Mimulus_Genomes/results/assembly/Mpar/Mpar_HFGJFALXX_L6_2_paired.fq.gz /home/CAM/qlin/Mimulus_Genomes/results/assembly/MvBL/MVBL_HFGJFALXX_L6_2_paired.fq.gz)

bowtie2-build --quiet --threads 8 ${genome[$SLURM_ARRAY_TASK_ID]} ${label[$SLURM_ARRAY_TASK_ID]}_ref

bowtie2 --threads 8 --phred33 -x ${label[$SLURM_ARRAY_TASK_ID]}_ref -1 ${reads1[$SLURM_ARRAY_TASK_ID]} -2 ${reads2[$SLURM_ARRAY_TASK_ID]} -S ${label[$SLURM_ARRAY_TASK_ID]}.sam

samtools view -bT ${genome[$SLURM_ARRAY_TASK_ID]} -@ 8 ${label[$SLURM_ARRAY_TASK_ID]}.sam > ${label[$SLURM_ARRAY_TASK_ID]}.bam
rm ${label[$SLURM_ARRAY_TASK_ID]}.sam

samtools sort -o ${label[$SLURM_ARRAY_TASK_ID]}.sort.bam -T ${label[$SLURM_ARRAY_TASK_ID]}_tmp -@ 8 ${label[$SLURM_ARRAY_TASK_ID]}.bam
rm ${label[$SLURM_ARRAY_TASK_ID]}.bam

samtools stats ${label[$SLURM_ARRAY_TASK_ID]}.sort.bam > ${label[$SLURM_ARRAY_TASK_ID]}.stats
rm ${label[$SLURM_ARRAY_TASK_ID]}.sort.bam

rm ${label[$SLURM_ARRAY_TASK_ID]}_ref*


