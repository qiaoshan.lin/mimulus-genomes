#!/bin/bash
#SBATCH --job-name=jellyfish
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH -a 0-3
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=200G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load jellyfish/2.2.6
module load R/3.3.1

cd /home/CAM/qlin/Mimulus_Genomes/results/genome_size

label=(LF10 CE10 Mpar MvBL)

kmer=(21 25 29 33)

for k in ${kmer[@]}
do
	jellyfish count -t 8 -C -m $k -s 10G --min-qual-char=? -o ${label[$SLURM_ARRAY_TASK_ID]}_$k\mer_out ${label[$SLURM_ARRAY_TASK_ID]}.unmapped.fq
	jellyfish histo -h 100000000 -o ${label[$SLURM_ARRAY_TASK_ID]}_$k\mer_out.histo ${label[$SLURM_ARRAY_TASK_ID]}_$k\mer_out
	jellyfish dump ${label[$SLURM_ARRAY_TASK_ID]}_$k\mer_out > ${label[$SLURM_ARRAY_TASK_ID]}_$k\mer_out.fa
done


