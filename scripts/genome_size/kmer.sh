#!/bin/bash
#SBATCH --job-name=kmer
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-3
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=15G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

ml=/home/CAM/qlin/resource/LF10/LF10g_v2.0_nucleus.fa
mc=/home/CAM/qlin/resource/CE10/CE10g_v2.0_nucleus.fa  
mp=/home/CAM/qlin/resource/Mpar/Mparg_v2.0_nucleus.fa
mv=/home/CAM/qlin/resource/MvBL/MvBLg_v2.0_nucleus.fa

genome=($ml $mc $mp $mv)
label=(LF10 CE10 Mpar MvBL)

cd /home/CAM/qlin/Mimulus_Genomes/results/genome_size

perl /home/CAM/qlin/Mimulus_Genomes/scripts/genome_size/kmerGenerate.pl ${genome[$SLURM_ARRAY_TASK_ID]} 21 ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.fa

grep -v '>' ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.fa |sort|uniq -c |sort -nr > ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count

awk -F '[A-Za-z]+$' '{print $1}' ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count |uniq -c > ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count.freq

rm ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.fa

