#!/bin/bash
#SBATCH --job-name=bowtie2
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH -a 0-3
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load bowtie2/2.3.1 
module load samtools/1.3.1
module load bamtools/2.4.1

cd /home/CAM/qlin/Mimulus_Genomes/results/genome_size

label=(LF10 CE10 Mpar MvBL)

cpDNA=(/home/CAM/qlin/resource/LF10/LF10g_v1.9_cpDNA.fa /home/CAM/qlin/resource/CE10/CE10g_v1.9_cpDNA.fa /home/CAM/qlin/resource/Mpar/Mparg_v1.9_cpDNA.fa /home/CAM/qlin/resource/MvBL/MvBLg_v1.9_cpDNA.fa)

mtDNA=(/home/CAM/qlin/resource/LF10/LF10g_v1.9_mtDNA_MCv1.fa /home/CAM/qlin/resource/CE10/CE10g_v1.9_mtDNA_MCv1.fa /home/CAM/qlin/resource/Mpar/Mparg_v1.9_mtDNA.fa /home/CAM/qlin/resource/MvBL/MvBLg_v1.9_mtDNA.fa)

reads1=(/home/CAM/qlin/LF10_Genome/02_PE_reads_trimming/t3/LF10_3_500bp_L8_1_paired.fq.gz /home/CAM/qlin/CE10_Genome/02_PE_reads_trimming/t1/trimmed_CE10_HFGJFALXX_L6_1_paired.fq.gz /home/CAM/qlin/Mpar_Genome/02_PE_reads_trimming/t1/trimmed_Mpar_HFGJFALXX_L6_1_paired.fq.gz /home/CAM/qlin/MvBL_Genome/02_PE_reads_trimming/t1/trimmed_MVBL_HFGJFALXX_L6_1_paired.fq.gz)

reads2=(/home/CAM/qlin/LF10_Genome/02_PE_reads_trimming/t3/LF10_3_500bp_L8_2_paired.fq.gz /home/CAM/qlin/CE10_Genome/02_PE_reads_trimming/t1/trimmed_CE10_HFGJFALXX_L6_2_paired.fq.gz /home/CAM/qlin/Mpar_Genome/02_PE_reads_trimming/t1/trimmed_Mpar_HFGJFALXX_L6_2_paired.fq.gz /home/CAM/qlin/MvBL_Genome/02_PE_reads_trimming/t1/trimmed_MVBL_HFGJFALXX_L6_2_paired.fq.gz)

cat ${cpDNA[$SLURM_ARRAY_TASK_ID]} ${mtDNA[$SLURM_ARRAY_TASK_ID]} > ${label[$SLURM_ARRAY_TASK_ID]}_cpDNA_mtDNA.fa

ref=${label[$SLURM_ARRAY_TASK_ID]}_cpDNA_mtDNA.fa

bowtie2-build --quiet --threads 8 $ref ${label[$SLURM_ARRAY_TASK_ID]}_ref

bowtie2 --threads 8 --phred33 -x ${label[$SLURM_ARRAY_TASK_ID]}_ref -1 ${reads1[$SLURM_ARRAY_TASK_ID]} -2 ${reads2[$SLURM_ARRAY_TASK_ID]} -S ${label[$SLURM_ARRAY_TASK_ID]}.sam

samtools view -bT $ref -@ 8 ${label[$SLURM_ARRAY_TASK_ID]}.sam > ${label[$SLURM_ARRAY_TASK_ID]}.bam
rm ${label[$SLURM_ARRAY_TASK_ID]}.sam

samtools view -bT $ref -f 4 -@ 8 ${label[$SLURM_ARRAY_TASK_ID]}.bam > ${label[$SLURM_ARRAY_TASK_ID]}.unmapped.bam

samtools view ${label[$SLURM_ARRAY_TASK_ID]}.unmapped.bam |awk '{print "@"$1"\n"$10"\n+\n"$11}' > ${label[$SLURM_ARRAY_TASK_ID]}.unmapped.fq

rm $ref

rm ${label[$SLURM_ARRAY_TASK_ID]}.unmapped.bam

