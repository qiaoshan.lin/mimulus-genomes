library(ggplot2)
dir <- "/Volumes/GoogleDrive/My Drive/Qiaoshan/Mimulus_Genomes/genome_size/"
setwd(dir)

p <- read.table(file = "all.repeatcov.txt", sep = "\t", header = TRUE)
p$species <- factor(p$species, levels = c("LF10","CE10","Mpar","MvBL"))
pFig <-ggplot(data = p, aes(coverage, length, color=species))+
  geom_line()+
  scale_x_log10()+
  scale_y_continuous(limits = c(0,17000000))+
  theme_minimal()+
  theme(panel.grid = element_blank(),axis.line.y = element_line(colour = "black"),axis.line.x = element_line(colour = "black"),panel.grid.major.y = element_line(color = "gray",linetype = 3))
pFig
ggsave("all.repeatcov.png", device = png(), width = 12, height = 6, units = "cm")

