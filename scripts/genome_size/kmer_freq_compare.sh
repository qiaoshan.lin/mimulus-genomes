#!/bin/bash
#SBATCH --job-name=kmer_freq_compare
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-3
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=200G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

cd /home/CAM/qlin/Mimulus_Genomes/results/genome_size

label=(LF10 CE10 Mpar MvBL)
cov=(25 21 26 20)
echo ${label[$SLURM_ARRAY_TASK_ID]}

awk '{print $2"\t"$1}' ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count|sort -k1b,1 > ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count.tmp
mv ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count.tmp ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count

cat ${label[$SLURM_ARRAY_TASK_ID]}_21mer_out.fa|paste - - |tr -d '>'|awk -v cov="${cov[$SLURM_ARRAY_TASK_ID]}" '{print $2"\t"$1/cov}' |sort -k1b,1 > ${label[$SLURM_ARRAY_TASK_ID]}.reads.21mer.count

join -j 1 ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count ${label[$SLURM_ARRAY_TASK_ID]}.reads.21mer.count |awk '{print $1"\t"$2"\t"$3}'> ${label[$SLURM_ARRAY_TASK_ID]}.21mer.compare.count

sharedForward=`wc -l ${label[$SLURM_ARRAY_TASK_ID]}.21mer.compare.count|awk '{print $1}'`
sharedSizeForward=`awk '{s+=$2}END{print s}' ${label[$SLURM_ARRAY_TASK_ID]}.21mer.compare.count`
sharedEstSizeForward=`awk '{s+=$3}END{print s}' ${label[$SLURM_ARRAY_TASK_ID]}.21mer.compare.count`

join -j 1 -v 1 ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count ${label[$SLURM_ARRAY_TASK_ID]}.reads.21mer.count|awk '{print $1"\t"$2}'> ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count.left

join -j 1 -v 2 ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count ${label[$SLURM_ARRAY_TASK_ID]}.reads.21mer.count|awk '{print $1"\t"$2}'> ${label[$SLURM_ARRAY_TASK_ID]}.reads.21mer.count.left

# when jellyfish count kmer, it counts kmer and its reverse complement as one kmer. For example, when there are 10 AAAATT and 20 AATTTT, jellyfish will generate count=30 for AAAATT if AAAATT occurs first. But the kmer generated from the genome assembly has only one strand direction. So here we need to reverse complement LF10_reads.21mer.count.left and compare it with LF10_reads.21mer.count again.

perl -lane '$revcomp = reverse $F[0];$revcomp =~ tr/ATGCatgc/TACGtacg/;print "$revcomp\t$F[1]"' ${label[$SLURM_ARRAY_TASK_ID]}.reads.21mer.count.left|sort -k1b,1 > ${label[$SLURM_ARRAY_TASK_ID]}.reads.21mer.count.left.rc

join -j 1 ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count ${label[$SLURM_ARRAY_TASK_ID]}.reads.21mer.count.left.rc|awk '{print $1"\t"$2"\t"$3}'> ${label[$SLURM_ARRAY_TASK_ID]}.21mer.compare.count2

sharedReverse=`wc -l ${label[$SLURM_ARRAY_TASK_ID]}.21mer.compare.count2|awk '{print $1}'`
sharedSizeReverse=`awk '{s+=$2}END{print s}' ${label[$SLURM_ARRAY_TASK_ID]}.21mer.compare.count2`
sharedEstSizeReverse=`awk '{s+=$3}END{print s}' ${label[$SLURM_ARRAY_TASK_ID]}.21mer.compare.count2`

perl -se 'print "Number of 21mers shared by Illumina short reads and PacBio assembly\t",$f+$r,"\n"' -- -f=$sharedForward -r=$sharedReverse
perl -se 'print "Corresponding size of assembly to the 21mers shared by Illumina short reads and PacBio assembly\t",$f+$r,"\n"' -- -f=$sharedSizeForward -r=$sharedSizeReverse
perl -se 'print "Corresponding estimated size of assembly to the 21mers shared by Illumina short reads and PacBio assembly\t",$f+$r,"\n"' -- -f=$sharedEstSizeForward -r=$sharedEstSizeReverse

join -j 1 -v 1 ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count ${label[$SLURM_ARRAY_TASK_ID]}.reads.21mer.count.left.rc|awk '{print $1"\t"$2}'> ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count.left2

join -j 1 -v 2 ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count ${label[$SLURM_ARRAY_TASK_ID]}.reads.21mer.count.left.rc|awk '{print $1"\t"$2}'> ${label[$SLURM_ARRAY_TASK_ID]}.reads.21mer.count.left.rc.left

sort -k1b,1 ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count.left > ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count.left.sort
sort -k1b,1 ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count.left2 > ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count.left2.sort

join -j 1 ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count.left.sort ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count.left2.sort|awk '{print $1"\t"$2"\t"$3}'>${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count.left3

sizeNoAlign=`awk '{s+=$3}END{print s}' ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count.left3`
perl -se 'print "Size of assembly not aligned by 21mers from Illumina short reads\t",$s,"\n"' -- -s=$sizeNoAlign

sizeNotIn=`awk '{s+=$2}END{print s}' ${label[$SLURM_ARRAY_TASK_ID]}.reads.21mer.count.left.rc.left`
perl -se 'print "Estimated size of sequence not in PacBio assembly but indicated by 21mers from Illumina short reads\t",$s,"\n"' -- -s=$sizeNotIn

rm ${label[$SLURM_ARRAY_TASK_ID]}.reads.21mer.count
rm ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count.left
rm ${label[$SLURM_ARRAY_TASK_ID]}.reads.21mer.count.left
rm ${label[$SLURM_ARRAY_TASK_ID]}.reads.21mer.count.left.rc
rm ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count.left2
rm ${label[$SLURM_ARRAY_TASK_ID]}.reads.21mer.count.left.rc.left
rm ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count.left.sort
rm ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count.left2.sort
rm ${label[$SLURM_ARRAY_TASK_ID]}.genome.21mer.count.left3

