#!/usr/bin/perl

if (not defined($ARGV[0]) or not defined($ARGV[1]) or not defined($ARGV[2])) {
 	print "----------------------------------------------------------------\n";
 	print "This script is used for generating kmers from a fasta file.\n";
 	print "----------------------------------------------------------------\n";
 	print "Usage:\n";
 	print "perl ~/scripts/kmerGenerate.pl [fasta_file] [kmer length] [output_file]\n\n";
 	exit;
} 

open FA, "$ARGV[0]";
open OUT, ">$ARGV[2]";

$head = "";
$body = "";

while (<FA>) {
	if ($_ =~ /^>/) {
		kmerGenerate($head,$body);
#		print $head, ":", length($body), "\n";
		chomp($_);
		$head = $_;
		$body = "";
	}else{	
		chomp($_);
		$body .= $_;
	}
}
kmerGenerate($head,$body);

close FA;
close OUT;

sub kmerGenerate {
	my $head = @_[0];
	my $body = @_[1];
	for (my $i = 0; $i < length($body)-$ARGV[1]+1; $i++) {
		my $kmer = substr $body, $i, $ARGV[1];
		print OUT $head, "_", $i+1, "\n$kmer\n"; 
	}
}
