library(ggplot2)
library(gridExtra)

dir<-"/Volumes/GoogleDrive/My\ Drive/Qiaoshan/Mimulus_Genomes/genome_size/"
setwd(dir)

df<-read.table(file = "genomeSize.txt", header = TRUE, sep = "\t")
df$species <- factor(df$species, levels = c("LF10","CE10","Mpar","MvBL"))

ggplot(data = df, aes(group, size, fill=type))+
  geom_bar(stat = "identity", position = "stack")+
  facet_grid(. ~ species)+
  theme_minimal()+
  theme(panel.grid = element_blank(),axis.line.y = element_line(colour = "black"))
dev.copy(png, "genomeSizeFig1.png", width=900, height=300, res=105)
dev.off()

df<-subset(df,grepl("final|genomeScope",df$group))
df$group <- factor(df$group, levels = c("genomeScope", "final"))

p1<-ggplot(data = df, aes(group, size, fill=type))+
  geom_bar(stat = "identity", position = "stack")+
  facet_grid(. ~ species)+
  theme_minimal()+
  theme(panel.grid = element_blank(),axis.line.y = element_line(colour = "black"))
p2<-ggplot(data = df, aes(group, size, fill=type))+
  geom_bar(stat = "identity", position = "fill")+
  facet_grid(. ~ species)+
  theme_minimal()+
  theme(panel.grid = element_blank(),axis.line.y = element_line(colour = "black"))

grid.arrange(p1,p2, nrow=2)
dev.copy(png, "genomeSizeFig3.png", width=900, height=600, res=135)
dev.off()
