#!/bin/bash
#SBATCH --job-name=mummer_analysis
#SBATCH -c 2
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-3
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

cd /home/CAM/qlin/Mimulus_Genomes/results/genome_size

label=(LF10 CE10 Mpar MvBL)

mc=/home/CAM/qlin/resource/CE10/CE10g_v2.0_nucleus.fa  
mp=/home/CAM/qlin/resource/Mpar/Mparg_v2.0_nucleus.fa
ml=/home/CAM/qlin/resource/LF10/LF10g_v2.0_nucleus.fa  
mv=/home/CAM/qlin/resource/MvBL/MvBLg_v2.0_nucleus.fa

genome=($ml $mc $mp $mv)

awk '$7<100{print $12"\t"$1"\t"$2}' ${label[$SLURM_ARRAY_TASK_ID]}.coords |tail -n +4 |sort -k1,1 -k2,2n > ${label[$SLURM_ARRAY_TASK_ID]}.bed

module load bedtools

bedtools merge -i ${label[$SLURM_ARRAY_TASK_ID]}.bed > ${label[$SLURM_ARRAY_TASK_ID]}.merge.bed

perl ~/scripts/lengthFilter4fa.pl ${genome[$SLURM_ARRAY_TASK_ID]} 0 ${label[$SLURM_ARRAY_TASK_ID]}_tmp|tr -d '>' > ${label[$SLURM_ARRAY_TASK_ID]}.genome
rm ${label[$SLURM_ARRAY_TASK_ID]}_tmp
bedtools genomecov -i ${label[$SLURM_ARRAY_TASK_ID]}.bed -g ${label[$SLURM_ARRAY_TASK_ID]}.genome > ${label[$SLURM_ARRAY_TASK_ID]}.genomecov

grep '^genome' ${label[$SLURM_ARRAY_TASK_ID]}.genomecov |cut -f2-3 > ${label[$SLURM_ARRAY_TASK_ID]}.repeatcov.txt
genomelen=`awk '{len+=$2}END{print len}' ${label[$SLURM_ARRAY_TASK_ID]}.repeatcov.txt`
nocovlen=`awk '$1==0{print $2}' ${label[$SLURM_ARRAY_TASK_ID]}.repeatcov.txt`
alignlen=`echo "$(($genomelen-$nocovlen))"`
alignRat=`echo "$(($alignlen*100/$genomelen))%"`
echo "Length of aligned regions: $alignlen"
echo "Ratio of aligned regions: $alignRat"
sed -i '1s/^/coverage\tlength\n/' ${label[$SLURM_ARRAY_TASK_ID]}.repeatcov.txt

#bedtools genomecov -i ${label[$SLURM_ARRAY_TASK_ID]}.bed -g ${label[$SLURM_ARRAY_TASK_ID]}.genome -bg > ${label[$SLURM_ARRAY_TASK_ID]}.genomecov.d

bedtools intersect -a ~/LF10_Genome/versions/chromosomes/${label[$SLURM_ARRAY_TASK_ID]}.bed -b ${label[$SLURM_ARRAY_TASK_ID]}.merge.bed > ${label[$SLURM_ARRAY_TASK_ID]}.repeat.gene.intersect.bed
gnum=`cut -f4 ${label[$SLURM_ARRAY_TASK_ID]}.repeat.gene.intersect.bed |sort|uniq|wc -l`
echo "Number of genes overlapped with aligned regions: $gnum"
len=`awk '{sum+=($3-$2+1)}END{print sum}' ${label[$SLURM_ARRAY_TASK_ID]}.repeat.gene.intersect.bed`
echo "Length of these aligned regions overlapped with genes: $len"

#paste ~/LF10_Genome/15_orthoFinder/t3/genomes/Results_Jun18/LF10_dup_genes.bed ~/LF10_Genome/15_orthoFinder/t3/genomes/Results_Jun18/LF10_dup_genes.txt > LF10_dup_genes.bed
#sed -i 's/^LF10_//' LF10_dup_genes.bed
#bedtools intersect -a LF10_dup_genes.bed -b LF10.merge.bed > LF10.repeat.dupgene.intersect.bed
#gnum=`cut -f4 LF10.repeat.dupgene.intersect.bed |sort|uniq|wc -l`
#echo "Number of duplicated genes overlapped with aligned regions: $gnum"
#len=`awk '{sum+=($3-$2+1)}END{print sum}' LF10.repeat.dupgene.intersect.bed`
#echo "Length of these aligned regions overlapped with duplicated genes: $len"

bedtools intersect -a ~/${label[$SLURM_ARRAY_TASK_ID]}_Genome/circos/${label[$SLURM_ARRAY_TASK_ID]}.TE.bed -b ${label[$SLURM_ARRAY_TASK_ID]}.merge.bed > ${label[$SLURM_ARRAY_TASK_ID]}.repeat.TE.intersect.bed
len=`awk '{sum+=($3-$2+1)}END{print sum}' ${label[$SLURM_ARRAY_TASK_ID]}.repeat.TE.intersect.bed`
echo "Length of these aligned regions overlapped with TEs \(on chromosomes\): $len" 


