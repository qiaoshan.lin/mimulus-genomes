#!/bin/bash
#SBATCH --job-name=mummer
#SBATCH -c 2
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-3
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load MUMmer/3.23 

cd /home/CAM/qlin/Mimulus_Genomes/results/genome_size

label=(LF10 CE10 Mpar MvBL)
 
mc=/home/CAM/qlin/resource/CE10/CE10g_v2.0_nucleus.fa  
mp=/home/CAM/qlin/resource/Mpar/Mparg_v2.0_nucleus.fa
ml=/home/CAM/qlin/resource/LF10/LF10g_v2.0_nucleus.fa  
mv=/home/CAM/qlin/resource/MvBL/MvBLg_v2.0_nucleus.fa

genome=($ml $mc $mp $mv)

nucmer --maxmatch -c 2000 -l 100 -p ${label[$SLURM_ARRAY_TASK_ID]} --nosimplify ${genome[$SLURM_ARRAY_TASK_ID]} ${genome[$SLURM_ARRAY_TASK_ID]}

show-coords -T -r -o -I 95 -L 2000 -c -d ${label[$SLURM_ARRAY_TASK_ID]}.delta > ${label[$SLURM_ARRAY_TASK_ID]}.coords


