#!/opt/perl/bin/perl
#use warnings;

die "usage: PacBio_Read_Stat.pl <XXX.fa> <genome_size>\n" unless @ARGV == 2;
$inputfile = $ARGV[0];
$estimatedgenomesize = $ARGV[1];
open (FILE, $inputfile); 

$contig_length = 0;
$raw_read_number = 0;

while (<FILE>) {
	if (/^>/) {
		$raw_read_number++;
		if ($contig_length > 0) {
			push (@contig_length, $contig_length);
			$total_size += $contig_length;
		}
		$contig_length = 0;
	} else {
		chomp $_;
		$contig_length += length($_);
	}
}

close FILE;

push (@contig_length, $contig_length);
$total_size += $contig_length;
$contig_number = scalar @contig_length;
$average_length = int($total_size/$contig_number);
$coverage = $total_size/$estimatedgenomesize;
@contig_length = sort {$b<=>$a} @contig_length;

if ($contig_number % 2 == 0) {
	$median = int( ($contig_length[$contig_number/2 - 1] + $contig_length[$contig_number/2])/2 );
} else {
	$median = $contig_length[($contig_number-1)/2];
}
print "Number of original raw reads	:\t$raw_read_number\n";
print "Number of reads > 0 base :\t$contig_number\n";
print "Number of bases :\t$total_size\n";
print "Estimatd Coverage :\t$coverage\n";	
print "Median :\t$median\n";
print "Mean :\t$average_length\n";
print "Minimum :\t$contig_length[-1]\n";
print "Maximum :\t$contig_length[0]\n";

exit;
