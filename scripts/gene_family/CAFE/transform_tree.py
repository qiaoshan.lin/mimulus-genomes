# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

from ete3 import Tree
t = Tree('((MVBL_augustus.hints:0.0688124,Mpar_augustus.hints:0.0440774)0.417506:0.0182324,(CE10_augustus.hints:0.0408002,LF10_augustus.hints:0.0482191)0.417506:0.0182324);')
t.set_outgroup('MVBL_augustus.hints')
print('Non-ultrametric rooted tree:')
print(t.write())
t.convert_to_ultrametric()
print('Ultrametric rooted tree:')
print(t.write())