#!/isg/shared/apps/CAFE/4.1/cafe
tree (MVBL:18.825,(Mpar:12.55,(LF10:6.275,CE10:6.275):6.275):6.275)
load -i filtered_cafe_input.txt -t 10 -l reports/run8_caferror_files/cafe_final_log.txt
errormodel -model reports/run8_caferror_files/cafe_errormodel_0.039453125.txt -sp LF10
errormodel -model reports/run8_caferror_files/cafe_errormodel_0.039453125.txt -sp Mpar
errormodel -model reports/run8_caferror_files/cafe_errormodel_0.039453125.txt -sp MVBL
errormodel -model reports/run8_caferror_files/cafe_errormodel_0.039453125.txt -sp CE10
lambda -s -t (1,(1,(1,1)1)1)
report reports/run8_caferror_files/cafe_final_report