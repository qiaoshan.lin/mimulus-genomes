#!cafe
#CAFE/4.1

load -i filtered_cafe_input.txt -t 8 -l reports/log_run9.txt -p 0.01
tree (MVBL:18.825,(Mpar:12.55,(LF10:6.275,CE10:6.275):6.275):6.275)
errormodel -model reports/run8_caferror_files/cafe_errormodel_0.039453125.txt -all
lambda -s -t (1,(1,(1,1)1)1)
report reports/report_run9

