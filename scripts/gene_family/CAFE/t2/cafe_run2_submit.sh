#!/bin/bash
#SBATCH --job-name=cafe
#SBATCH -c 1
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH -o cafe_%j.out
#SBATCH -e cafe_%j.err

module load CAFE/4.1
module load python/2.7.9
module load R/3.4.3

cafe cafe_run2.sh 

python ../python_scripts/cafetutorial_report_analysis.py -i reports/report_run2.cafe -o summary/summary_run2


