#!/bin/bash
#SBATCH --job-name=cafe
#SBATCH -c 8
#SBATCH -N 1
#SBATCH -n 1
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH -o cafe_%j.out
#SBATCH -e cafe_%j.err

module load CAFE/4.1
module load python
module load R/3.4.3

#awk '{print "(null)\t"$1"\t"$2"\t"$3"\t"$4"\t"$5}' ~/LF10_Genome/15_orthoFinder/t2/genomes/Results_Apr08/Orthogroups.GeneCount.csv > unfiltered_cafe_input.txt
#cp unfiltered_cafe_input.txt filtered_cafe_input.txt 
#modify filtered_cafe_input.txt manually as there is only one line has a group >=100

#cafe cafe_shell_scripts/cafe_run1.sh
#python python_scripts/cafetutorial_report_analysis.py -i reports/report_run1.cafe -o summary/summary_run1

#stop here and add lambda value to shell script run2,5,6
#exit 0

#cafe cafe_shell_scripts/cafe_run2.sh
#python python_scripts/cafetutorial_report_analysis.py -i reports/report_run2.cafe -o summary/summary_run2

#cafe cafe_shell_scripts/cafe_run3.sh
#python python_scripts/cafetutorial_report_analysis.py -i reports/report_run3.cafe -o summary/summary_run3

#cafe cafe_shell_scripts/cafe_run4.sh
#python python_scripts/cafetutorial_report_analysis.py -i reports/report_run4.cafe -o summary/summary_run4

#stop here and add log-likelihoods values to below
#exit 0

#cafe cafe_shell_scripts/cafe_run5.sh
#cut -f 2,4 reports/lhtest_results_run5.txt > reports/run5_1k_diffs.txt
#Rscript lhtest.R reports/run5_1k_diffs.txt -76153.698796 -74751.265766
#mv reports/lk_null.pdf reports/lk_null5.pdf

#cafe cafe_shell_scripts/cafe_run6.sh
#cut -f 2,4 reports/lhtest_results_run6.txt > reports/run6_1k_diffs.txt
#Rscript lhtest.R reports/run6_1k_diffs.txt -76153.698796 -75070.775866
#mv reports/lk_null.pdf reports/lk_null6.pdf

#cafe cafe_shell_scripts/cafe_run7.sh
#python python_scripts/cafetutorial_report_analysis.py -i reports/report_run7.cafe -o summary/summary_run7

python caferror.py -i cafe_shell_scripts/cafe_run8.sh -d reports/run8_caferror_files -v 0 -f 1

#stop here and modify
exit 0

sort -k2 -n caferror_default_output.txt |tail -n +2|head -1|cut -f1
cafe cafe_shell_scripts/cafe_run9.sh
python python_scripts/cafetutorial_report_analysis.py -i reports/report_run9.cafe -o summary/summary_run9

#use the following commands to count contraction and expansion in each branch
#sed -n '3p' xxx_fams.txt |grep -oP 'OG\d+\[\+\d+\*\]'|wc -l
#sed -n '3p' xxx_fams.txt |grep -oP 'OG\d+\[\-\d+\*\]'|wc -l
