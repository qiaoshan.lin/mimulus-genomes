#!/bin/bash
#SBATCH --job-name=busco
#SBATCH -a 0-3
#SBATCH -n 4
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

# before running this script, download eudicotyledons_odb10 into the working directory
workingdir=/home/CAM/qlin/Mimulus_Genomes/results/assessment/BUSCO

label=(CE10 LF10 Mpar MvBL)

protome=(/home/CAM/qlin/resource/CE10/CE10g_v2.0.protein.fa /home/CAM/qlin/resource/LF10/LF10g_v2.0.protein.fa /home/CAM/qlin/resource/Mpar/Mparg_v2.0.protein.fa /home/CAM/qlin/resource/MvBL/MvBLg_v2.0.protein.fa)

genome=(/home/CAM/qlin/resource/CE10/CE10g_v2.0.fa /home/CAM/qlin/resource/LF10/LF10g_v2.0.fa /home/CAM/qlin/resource/Mpar/Mparg_v2.0.fa /home/CAM/qlin/resource/MvBL/MvBLg_v2.0.fa)

module load busco/3.0.2b

cd $workingdir

python /isg/shared/apps/busco/3.0.2b/scripts/run_BUSCO.py \
 -i ${protome[$SLURM_ARRAY_TASK_ID]} -o ${label[$SLURM_ARRAY_TASK_ID]}_busco_prot -l eudicotyledons_odb10 -m prot \
 -c 4 -t ./${label[$SLURM_ARRAY_TASK_ID]}_tmp

module unload augustus/3.2.3
PATH=/home/CAM/qlin/augustus/3.2.3/bin:/home/CAM/qlin/augustus/3.2.3/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/augustus/3.2.3/config/

python /isg/shared/apps/busco/3.0.2b/scripts/run_BUSCO.py \
 -i ${genome[$SLURM_ARRAY_TASK_ID]} -o ${label[$SLURM_ARRAY_TASK_ID]}_busco_geno -l eudicotyledons_odb10 -m geno \
 -c 4 -t ./${label[$SLURM_ARRAY_TASK_ID]}_tmp

