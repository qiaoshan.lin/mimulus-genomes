#!/bin/bash
#SBATCH --job-name=rnaseq_identity_mv
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-5
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load samtools

fileDir=/home/CAM/qlin/Mimulus_Genomes/results/annotation/MvBL
file=(trimmed_MvBL_FB10 trimmed_MvBL_FB16 trimmed_MvBL_FB30 trimmed_MvBL_FB5 trimmed_MvBL_TinyFB trimmed_MvBL_YoungLeaf)

# remember to make a working directory before running the script
workingDir=/home/CAM/qlin/Mimulus_Genomes/results/assessment/rnaseq_identity/MvBL
cd $workingDir

# calculate the coverage of RNAseq reads
samtools stats $fileDir/${file[$SLURM_ARRAY_TASK_ID]}.sort.bam > ${file[$SLURM_ARRAY_TASK_ID]}.stats

# error rate can be found in stats files
# grep 'error rate' *stats

