#!/bin/bash
#SBATCH --job-name=rnaseq_identity_ml
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=20G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load samtools

fileDir=/home/CAM/qlin/Mimulus_Genomes/results/annotation/LF10
file=(trimmed_LF10_10FB_ATCACG_L003 trimmed_LF10_15FB_CGATGT_L003 trimmed_LF10_15mm_Corolla trimmed_LF10_15NG_GTGAAA_L001 trimmed_LF10_15PL_GTCCGC_L001 trimmed_LF10_20FB_TTAGGC_L003 trimmed_LF10_2FB_ACTGAT_L002 trimmed_LF10L_CGATGT_L008 trimmed_LF10_5FB_ATTCCT_L002)

# remember to make a working directory before running the script
workingDir=/home/CAM/qlin/Mimulus_Genomes/results/assessment/rnaseq_identity/LF10
cd $workingDir

# calculate the coverage of RNAseq reads
samtools stats $fileDir/${file[$SLURM_ARRAY_TASK_ID]}.sort.bam > ${file[$SLURM_ARRAY_TASK_ID]}.stats

# error rate can be found in stats files
# grep 'error rate' *stats

