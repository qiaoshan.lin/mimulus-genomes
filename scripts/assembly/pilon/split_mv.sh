#!/bin/bash
#SBATCH --job-name=split_mv
#SBATCH -c 8
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

workingDir=/home/CAM/qlin/Mimulus_Genomes/results/assembly/MvBL
# run this command before running the script
# cd $workingDir
# grep '^>' mvbl.contigs.fasta |cut -f1 -d " "|cut -f2 -d ">" > contigs.txt

module load samtools/1.7

contigs=mvbl.contigs.fasta
trimmed=(Mverb_NoIndex_L004 MVBL_HFGJFALXX_L6)

cd $workingDir

array=()
while read line; do   array+=($line);   done < contigs.txt
for contig in ${array[@]}
do
	samtools view -bST $contigs -@ 8 ${trimmed[$SLURM_ARRAY_TASK_ID]}.sorted.bam $contig > ${trimmed[$SLURM_ARRAY_TASK_ID]}.$contig\.bam
	samtools index ${trimmed[$SLURM_ARRAY_TASK_ID]}.$contig\.bam
done

# run these commands after running the script
# cd $workingDir
# mkdir bam
# mv *.bam bam/
# mv *.bai bam/

# perl /home/CAM/qlin/Mimulus_Genomes/scripts/assembly/pilon/splitSeq4fa.pl mvbl.contigs.fasta
# mkdir contigs
# mv *.tig*.fasta contigs/

# split -l 100 /home/CAM/qlin/Mimulus_Genomes/results/assembly/MvBL/contigs.txt contigs-



