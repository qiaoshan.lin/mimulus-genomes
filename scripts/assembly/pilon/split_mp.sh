#!/bin/bash
#SBATCH --job-name=split_mp
#SBATCH -c 8
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

workingDir=/home/CAM/qlin/Mimulus_Genomes/results/assembly/Mpar
# run this command before running the script
# cd $workingDir
# grep '^>' mpar.contigs.fasta |cut -f1 -d " "|cut -f2 -d ">" > contigs.txt

module load samtools/1.7

contigs=mpar.contigs.fasta
trimmed=(Mpari_NoIndex_L003)

cd $workingDir

array=()
while read line; do   array+=($line);   done < contigs.txt
for contig in ${array[@]}
do
	samtools view -bST $contigs -@ 8 ${trimmed[$SLURM_ARRAY_TASK_ID]}.sorted.bam $contig > ${trimmed[$SLURM_ARRAY_TASK_ID]}.$contig\.bam
	samtools index ${trimmed[$SLURM_ARRAY_TASK_ID]}.$contig\.bam
done

# run these commands after running the script
# cd $workingDir
# mkdir bam
# mv *.bam bam/
# mv *.bai bam/

# perl /home/CAM/qlin/Mimulus_Genomes/scripts/assembly/pilon/splitSeq4fa.pl mpar.contigs.fasta
# mkdir contigs
# mv *.tig*.fasta contigs/

# split -l 100 /home/CAM/qlin/Mimulus_Genomes/results/assembly/Mpar/contigs.txt contigs-



