#!/bin/bash
#SBATCH --job-name=split_mc
#SBATCH -c 8
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

workingDir=/home/CAM/qlin/Mimulus_Genomes/results/assembly/CE10
# run this command before running the script
# cd $workingDir
# grep '^>' ce10_uni.contigs.fasta |cut -f1 -d " "|cut -f2 -d ">" > contigs.txt

module load samtools/1.7

contigs=ce10_uni.contigs.fasta
trimmed=(trimmed_CE10_HFGJFALXX_L6)

cd $workingDir

array=()
while read line; do   array+=($line);   done < contigs.txt
for contig in ${array[@]}
do
	samtools view -bST $contigs -@ 8 ${trimmed[$SLURM_ARRAY_TASK_ID]}.sorted.bam $contig > ${trimmed[$SLURM_ARRAY_TASK_ID]}.$contig\.bam
	samtools index ${trimmed[$SLURM_ARRAY_TASK_ID]}.$contig\.bam
done

# run these commands after running the script
# cd $workingDir
# mkdir bam
# mv *.bam bam/
# mv *.bai bam/

# perl /home/CAM/qlin/Mimulus_Genomes/scripts/assembly/pilon/splitSeq4fa.pl ce10_uni.contigs.fasta
# mkdir contigs
# mv *.tig*.fasta contigs/

# split -l 100 /home/CAM/qlin/Mimulus_Genomes/results/assembly/CE10/contigs.txt contigs-



