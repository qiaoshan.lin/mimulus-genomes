#!/bin/bash
#SBATCH --job-name=pilon_mp
#SBATCH -c 16
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-8
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=30G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load pilon/1.22

workingDir=/home/CAM/qlin/Mimulus_Genomes/results/assembly/Mpar

cd $workingDir

array=()
contigs=(contigs-*)
while read line; do   array+=($line);   done < ${contigs[$SLURM_ARRAY_TASK_ID]}

for contig in ${array[@]}
do
	java -Xmx6G -jar /isg/shared/apps/pilon/1.22/pilon-1.22.jar --genome contigs/mpar.contigs.$contig\.fasta --frags bam/Mpari_NoIndex_L003.$contig\.bam --changes --vcfqe --vcf --tracks --mindepth 30 --threads 16 --output pilon_$contig --outdir pilon_$contig
done

# rm *bam *bai
# rm -r bam/
# rm -r contigs/

# array=()
# while read line; do   array+=($line);   done < contigs.txt
# for contig in ${array[@]}
# do
# 	cat pilon_$contig/pilon_$contig\.changes >> pilon_all.changes
# 	cat pilon_$contig/pilon_$contig\.fasta >> pilon_all.fasta
# 	rm -r pilon_$contig
# done


