#!/bin/bash
#SBATCH --job-name=split_ml
#SBATCH -c 8
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-6
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

workingDir=/home/CAM/qlin/Mimulus_Genomes/results/assembly/LF10
# run this command before running the script
# cd $workingDir
# grep '^>' ../../LF10.contigs.fasta |cut -f1 -d " "|cut -f2 -d ">" > contigs.txt

module load samtools/1.7

contigs=LF10.contigs.fasta
trimmed=(trimmed_LF10_3_500bp_L2 trimmed_LF10_3_500bp_L8 trimmed_LF10_3_800bp_L1 trimmed_LF10-3_DSW06851_L2 trimmed_LF10-3_DSW06851_L6 trimmed_LF10-3_DSW06852_L3 trimmed_LF10-3_DSW06852_L5)

cd $workingDir

array=()
while read line; do   array+=($line);   done < contigs.txt
for contig in ${array[@]}
do
	samtools view -bST $contigs -@ 8 ${trimmed[$SLURM_ARRAY_TASK_ID]}.sorted.bam $contig > ${trimmed[$SLURM_ARRAY_TASK_ID]}.$contig\.bam
	samtools index ${trimmed[$SLURM_ARRAY_TASK_ID]}.$contig\.bam
done

# run these commands after running the script
# cd $workingDir
# mkdir bam
# mv *.bam bam/
# mv *.bai bam/

# perl /home/CAM/qlin/Mimulus_Genomes/scripts/assembly/pilon/splitSeq4fa.pl LF10.contigs.fasta
# mkdir contigs
# mv *.tig*.fasta contigs/

# split -l 100 /home/CAM/qlin/Mimulus_Genomes/results/assembly/LF10/contigs.txt contigs-



