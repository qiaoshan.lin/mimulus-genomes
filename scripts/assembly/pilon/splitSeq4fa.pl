#!/usr/bin/perl

if (not defined($ARGV[0])) {
 	print "---------------------------------------------------------------------------------------\n";
 	print "This script is used for spliting sequences from a fasta file into files named by heads.\n";
 	print "---------------------------------------------------------------------------------------\n";
 	print "Usage:\n";
 	print "perl ~/scripts/splitSeq4fa.pl [fasta_file]\n\n";
 	exit;
} 


open FA, "$ARGV[0]";
$ARGV[0] =~ s/\.((fasta)||(fa))$//;
$ARGV[0] =~ s/^(.*)\///;

while (<FA>) {
    if ($_ =~ /^>/) {
	close OUT;
	$contig = $_;
	$contig =~ s/\ (.*)$//;
	$contig =~ s/^>//;
	chomp $contig;
	open OUT, ">$ARGV[0].$contig.fasta";
	print OUT "$_";
    }else {
        print OUT "$_";
    }
}
close FA;

