#!/bin/bash
#SBATCH --job-name=trimmomatic_ml
#SBATCH -c 8
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-6
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load Trimmomatic/0.36

workingDir=~/Mimulus_Genomes/results/assembly/LF10/
cd $workingDir

cp /linuxshare/users/qlin/DNA/LF10/Beijing_PE/LF10* ./
lib=(LF10_3_500bp_L2 LF10_3_500bp_L8 LF10_3_800bp_L1 LF10-3_DSW06851_L2 LF10-3_DSW06851_L6 LF10-3_DSW06852_L3 LF10-3_DSW06852_L5)

java -jar $Trimmomatic PE \
 -phred33\
 -threads 8\
 ${lib[$SLURM_ARRAY_TASK_ID]}_1_clean.fq.gz ${lib[$SLURM_ARRAY_TASK_ID]}_2_clean.fq.gz\
 ${lib[$SLURM_ARRAY_TASK_ID]}_1_paired.fq.gz ${lib[$SLURM_ARRAY_TASK_ID]}_1_unpaired.fq.gz ${lib[$SLURM_ARRAY_TASK_ID]}_2_paired.fq.gz ${lib[$SLURM_ARRAY_TASK_ID]}_2_unpaired.fq.gz\
 LEADING:20\
 TRAILING:20\
 SLIDINGWINDOW:4:25\
 MINLEN:50 

rm ${lib[$SLURM_ARRAY_TASK_ID]}_?_clean.fq.gz



