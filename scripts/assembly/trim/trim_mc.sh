#!/bin/bash
#SBATCH --job-name=trimmomatic_mc
#SBATCH -c 8
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0-2
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load Trimmomatic/0.36

workingDir=~/Mimulus_Genomes/results/assembly/CE10/
cd $workingDir

cp /linuxshare/users/qlin/DNA/CE10/*fq.gz ./
cp /linuxshare/users/qlin/DNA/CE10/*txt ./

read1=(CE10_HFGJFALXX_L6_1.clean.fq.gz s_4_pair1_sequence.txt s_5_pair1_sequence.txt)
read2=(CE10_HFGJFALXX_L6_2.clean.fq.gz s_4_pair2_sequence.txt s_5_pair2_sequence.txt)
prefix=(CE10_HFGJFALXX_L6 s_4 s_5)

java -jar $Trimmomatic PE \
 -phred33\
 -threads 8\
 ${read1[$SLURM_ARRAY_TASK_ID]} ${read2[$SLURM_ARRAY_TASK_ID]}\
 ${prefix[$SLURM_ARRAY_TASK_ID]}_1_paired.fq.gz ${prefix[$SLURM_ARRAY_TASK_ID]}_1_unpaired.fq.gz ${prefix[$SLURM_ARRAY_TASK_ID]}_2_paired.fq.gz ${prefix[$SLURM_ARRAY_TASK_ID]}_2_unpaired.fq.gz\
 LEADING:20\
 TRAILING:20\
 SLIDINGWINDOW:4:25\
 MINLEN:50 

rm ${read1[$SLURM_ARRAY_TASK_ID]} ${read2[$SLURM_ARRAY_TASK_ID]}


