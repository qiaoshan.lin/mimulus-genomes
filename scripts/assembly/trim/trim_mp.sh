#!/bin/bash
#SBATCH --job-name=trimmomatic_mp
#SBATCH -c 8
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -a 0
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load Trimmomatic/0.36

workingDir=~/Mimulus_Genomes/results/assembly/Mpar/
cd $workingDir

cp /linuxshare/users/qlin/DNA/Mpar/*gz ./

read1=(Mpar_HFGJFALXX_L6_1.clean.fq.gz Mpari_NoIndex_L003_R1_001.fastq.gz)
read2=(Mpar_HFGJFALXX_L6_2.clean.fq.gz Mpari_NoIndex_L003_R2_001.fastq.gz)
prefix=(Mpar_HFGJFALXX_L6 Mpari_NoIndex_L003)

java -jar $Trimmomatic PE \
 -phred33\
 -threads 8\
 ${read1[$SLURM_ARRAY_TASK_ID]} ${read2[$SLURM_ARRAY_TASK_ID]}\
 ${prefix[$SLURM_ARRAY_TASK_ID]}_1_paired.fq.gz ${prefix[$SLURM_ARRAY_TASK_ID]}_1_unpaired.fq.gz ${prefix[$SLURM_ARRAY_TASK_ID]}_2_paired.fq.gz ${prefix[$SLURM_ARRAY_TASK_ID]}_2_unpaired.fq.gz\
 LEADING:20\
 TRAILING:20\
 SLIDINGWINDOW:4:25\
 MINLEN:50 

rm ${read1[$SLURM_ARRAY_TASK_ID]} ${read2[$SLURM_ARRAY_TASK_ID]}



