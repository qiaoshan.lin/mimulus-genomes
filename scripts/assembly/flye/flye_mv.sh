#!/bin/bash
#SBATCH --job-name=flye
#SBATCH -c 32
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=250G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o flye_%j.out
#SBATCH -e flye_%j.err

module load flye 

flye --pacbio-corr /home/CAM/yyuan/Mimulus_PacBio_assembly/MVBL_t1/MVBL.correctedReads.fasta.gz --genome-size 430m --out-dir out_1 --threads 32

