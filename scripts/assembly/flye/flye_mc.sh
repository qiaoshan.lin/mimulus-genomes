#!/bin/bash
#SBATCH --job-name=flye
#SBATCH -c 8
#SBATCH -n 4
#SBATCH -N 1
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=250G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o flye_%j.out
#SBATCH -e flye_%j.err

module load flye 

flye --pacbio-corr /home/CAM/yyuan/Mimulus_PacBio_assembly/CE10_t1/CE10.correctedReads.fasta.gz --genome-size 430m --out-dir out_1 --threads 32

