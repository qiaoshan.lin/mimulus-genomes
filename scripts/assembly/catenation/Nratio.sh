#!/bin/bash
#SBATCH --job-name=Nratio
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 2
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=10G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o Nratio_%j.out
#SBATCH -e Nratio_%j.err

module load samtools

input=exp.filter.coords
ref=~/parisii_Genome/versions/1.9.1/repeatMasker/Mparg_v1.91.fa.masked
boundary=50000

awk '{print $18":"$1"-"$2}' $input > exp.pos.txt
array=()
while read line; do   array+=($line);   done < exp.pos.txt
for pos in ${array[@]}
do
	n=`samtools faidx $ref $pos|grep -v '>'|tr -cd 'N'|wc -c`;
	m=`samtools faidx $ref $pos|grep -v '>'|tr -cd 'ATCGN'|wc -c`;
#	echo -n "$pos " >> exp.out.txt; 
	echo "$n*100/$m"|bc >> exp.out.txt;
done
paste $input exp.out.txt |awk '$20<80{print}'|awk '!($1>$boundary&&$2<($12-$boundary)&&$4>$boundary&&$5<($13-$boundary)){print}'

rm exp.out.txt exp.pos.txt

