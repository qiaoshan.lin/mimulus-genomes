#!/bin/bash
#SBATCH --job-name=contig_linker
#SBATCH -a 0-3
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=1G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o contig_linker_%A_%a.out
#SBATCH -e contig_linker_%A_%a.err

workingDir=/home/CAM/qlin/Mimulus_Genomes/results/assembly
label=(LF10 CE10 Mpar MvBL)

cd $workingDir/${label[$SLURM_ARRAY_TASK_ID]}/versions/2.0

module load samtools

awk '/chr1/{print $2"\t"$3}' chr_all > chr1
awk '/chr2/{print $2"\t"$3}' chr_all > chr2
awk '/chr3/{print $2"\t"$3}' chr_all > chr3
awk '/chr4/{print $2"\t"$3}' chr_all > chr4
awk '/chr5/{print $2"\t"$3}' chr_all > chr5
awk '/chr6/{print $2"\t"$3}' chr_all > chr6
awk '/chr7/{print $2"\t"$3}' chr_all > chr7
awk '/chr8/{print $2"\t"$3}' chr_all > chr8

perl contig_linker.pl ../1.9.3/${label[$SLURM_ARRAY_TASK_ID]}g_v1.93.fa chr1 100 > chr1.fa
perl contig_linker.pl ../1.9.3/${label[$SLURM_ARRAY_TASK_ID]}g_v1.93.fa chr2 100 > chr2.fa
perl contig_linker.pl ../1.9.3/${label[$SLURM_ARRAY_TASK_ID]}g_v1.93.fa chr3 100 > chr3.fa
perl contig_linker.pl ../1.9.3/${label[$SLURM_ARRAY_TASK_ID]}g_v1.93.fa chr4 100 > chr4.fa
perl contig_linker.pl ../1.9.3/${label[$SLURM_ARRAY_TASK_ID]}g_v1.93.fa chr5 100 > chr5.fa
perl contig_linker.pl ../1.9.3/${label[$SLURM_ARRAY_TASK_ID]}g_v1.93.fa chr6 100 > chr6.fa
perl contig_linker.pl ../1.9.3/${label[$SLURM_ARRAY_TASK_ID]}g_v1.93.fa chr7 100 > chr7.fa
perl contig_linker.pl ../1.9.3/${label[$SLURM_ARRAY_TASK_ID]}g_v1.93.fa chr8 100 > chr8.fa

cat chr1.fa chr2.fa chr3.fa chr4.fa chr5.fa chr6.fa chr7.fa chr8.fa > all_chr.fa
cat chr1 chr2 chr3 chr4 chr5 chr6 chr7 chr8 |cut -f1 > all_chr

rm chr?.fa chr?

grep '>' ../1.9.3/${label[$SLURM_ARRAY_TASK_ID]}g_v1.93.fa|tr -d '>'|cat - all_chr |awk '{print $1}'|sort|uniq -c|awk '$1==1{print $2}'> remaining.txt
perl fetchSeq4fa2.pl ../1.9.3/${label[$SLURM_ARRAY_TASK_ID]}g_v1.93.fa remaining.txt remaining.fa
cat all_chr.fa remaining.fa > ${label[$SLURM_ARRAY_TASK_ID]}g_v2.0.fa

rm all_chr*
rm remaining.*


