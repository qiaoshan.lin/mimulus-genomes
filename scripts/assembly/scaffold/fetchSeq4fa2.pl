#!/usr/bin/perl

if (not defined($ARGV[0]) or not defined($ARGV[1]) or not defined($ARGV[2])) {
 	print "---------------------------------------------------------------------------------------\n";
 	print "This script is used for fetching sequences from a fasta file by giving heads in a file.\n";
 	print "                       (Only accept clean transcript name list)                        \n";
 	print "---------------------------------------------------------------------------------------\n";
 	print "Usage:\n";
 	print "perl ~/scripts/fetchSeq4fa2.pl [fasta_file] [gene_name_file] [output_file]\n\n";
 	exit;
} 

open LIST, "$ARGV[1]";
while (<LIST>) {
	$gene{">$_"}++;
}
close LIST;

open OUT, ">$ARGV[2]";
open FA, "$ARGV[0]";
while (<FA>) {
	if ($_ =~ /^>/ and exists $gene{$_}) {
        print OUT "$seq" if (defined($seq));
        print OUT "$_";
        $seq = "";
        $mark = 1;
        $count++;
    }elsif ($_ =~ /^>/) {
        $mark = 0;
    }elsif ($mark==1) {
        $seq.=$_;
    }
}
print OUT "$seq";
close FA;
close OUT;
