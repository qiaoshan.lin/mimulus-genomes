#!/usr/bin/perl

if (not defined($ARGV[0]) or not defined($ARGV[1]) or not defined($ARGV[2])) {
 	print "----------------------------------------------------------------\n";
 	print "This script is to link contigs by Ns in a given order.\n";
 	print "----------------------------------------------------------------\n";
 	print "Usage:\n";
	print "module load samtools\n";
 	print "perl ~/scripts/contig_linker.pl [fasta_file] [contig_order_file] [number_of_Ns]\n\n";
 	exit;
} 

open ORD, "$ARGV[1]";
while (<ORD>) {
	chomp $_;
	my @row = split(/\t/, $_);
	$seq = `samtools faidx $ARGV[0] $row[0]`;
	my @seq = split(/\n/, $seq);
	$tmpseq = "";
        for (my $l = 1; $l < scalar @seq; $l++) {
                $tmpseq .= $seq[$l];
        }
	if ($row[1] eq "-") {
		$rc = reverse $tmpseq;
		$rc =~ tr/ATGCatgc/TACGtacg/;
		$tmpseq = $rc;
	}
	push @tmpseq, "$tmpseq";
}
close ORD;

$n = "N" x $ARGV[2];
$seq = join($n,@tmpseq);
$header = $ARGV[1];
$header =~ s/\.(.+)$//;
print ">$header\n$seq\n";

