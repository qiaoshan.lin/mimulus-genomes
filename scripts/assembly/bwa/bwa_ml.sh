#!/bin/bash
#SBATCH --job-name=bwa_ml
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH -a 0-6
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=200G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load bwa/0.7.17
module load samtools/1.7

contigs=../../LF10.contigs.fasta

trimDir=/home/CAM/qlin/LF10_Genome_Assembly/trimmomatic/t2
trimmed=(trimmed_LF10_3_500bp_L2 trimmed_LF10_3_500bp_L8 trimmed_LF10_3_800bp_L1 trimmed_LF10-3_DSW06851_L2 trimmed_LF10-3_DSW06851_L6 trimmed_LF10-3_DSW06852_L3 trimmed_LF10-3_DSW06852_L5)

workingDir=/home/CAM/qlin/Mimulus_Genomes/results/assembly/LF10

cd $workingDir

bwa index -p ref $contigs

bwa mem -t 8 ref $trimDir/${trimmed[$SLURM_ARRAY_TASK_ID]}_1_paired.fq.gz $trimDir/${trimmed[$SLURM_ARRAY_TASK_ID]}_2_paired.fq.gz > ${trimmed[$SLURM_ARRAY_TASK_ID]}.sam

samtools view -bST $contigs -@ 8 ${trimmed[$SLURM_ARRAY_TASK_ID]}.sam > ${trimmed[$SLURM_ARRAY_TASK_ID]}.bam
rm ${trimmed[$SLURM_ARRAY_TASK_ID]}.sam
samtools sort -o ${trimmed[$SLURM_ARRAY_TASK_ID]}.sorted.bam -T tmp -O bam -@ 8 ${trimmed[$SLURM_ARRAY_TASK_ID]}.bam
rm ${trimmed[$SLURM_ARRAY_TASK_ID]}.bam

samtools index ${trimmed[$SLURM_ARRAY_TASK_ID]}.sorted.bam



