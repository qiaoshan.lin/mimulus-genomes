#!/bin/bash
#SBATCH --job-name=bwa_mp
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 8
#SBATCH -a 0-1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=200G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o %x_%A_%a.out
#SBATCH -e %x_%A_%a.err

module load bwa/0.7.17
module load samtools/1.7

contigs=/home/CAM/yyuan/Mimulus_PacBio_assembly/mpar.contigs.fasta

trimDir=/home/CAM/qlin/raw/DNA/Mpar
trimmed=(Mpari_NoIndex_L003 trimmed_Mpar_HFGJFALXX_L6)

workingDir=/home/CAM/qlin/Mimulus_Genomes/results/assembly/Mpar

cd $workingDir

bwa index -p ref $contigs

bwa mem -t 8 ref $trimDir/${trimmed[$SLURM_ARRAY_TASK_ID]}_1_paired.fq.gz $trimDir/${trimmed[$SLURM_ARRAY_TASK_ID]}_2_paired.fq.gz > ${trimmed[$SLURM_ARRAY_TASK_ID]}.sam

samtools view -bST $contigs -@ 8 ${trimmed[$SLURM_ARRAY_TASK_ID]}.sam > ${trimmed[$SLURM_ARRAY_TASK_ID]}.bam
rm ${trimmed[$SLURM_ARRAY_TASK_ID]}.sam
samtools sort -o ${trimmed[$SLURM_ARRAY_TASK_ID]}.sorted.bam -T tmp -O bam -@ 8 ${trimmed[$SLURM_ARRAY_TASK_ID]}.bam
rm ${trimmed[$SLURM_ARRAY_TASK_ID]}.bam

samtools index ${trimmed[$SLURM_ARRAY_TASK_ID]}.sorted.bam



