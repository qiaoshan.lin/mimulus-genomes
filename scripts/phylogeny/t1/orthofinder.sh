#!/bin/bash
#SBATCH --job-name=orthofinder
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o orthofinder_%j.out
#SBATCH -e orthofinder_%j.err

module load OrthoFinder/2.2.7

cp /linuxshare/users/qlin/RNA/M_bicolor_transcriptome/MB* ./

module load Trimmomatic/0.36

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 16 \
 MBDOR_CGTACG_L002_R1_001.fastq.gz MBDOR_CGTACG_L002_R2_001.fastq.gz \
 trimmed_MBDOR_1_paired.fq.gz trimmed_MBDOR_1_unpaired.fq.gz \
 trimmed_MBDOR_2_paired.fq.gz trimmed_MBDOR_2_unpaired.fq.gz \
 ILLUMINACLIP:/home/CAM/qlin/resource/adapters.fa:2:20:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:20 \
 MINLEN:50

java -jar $Trimmomatic PE \
 -phred33 \
 -threads 16 \
 MBLAT_GAGTGG_L002_R1_001.fastq.gz MBLAT_GAGTGG_L002_R2_001.fastq.gz \
 trimmed_MBLAT_1_paired.fq.gz trimmed_MBLAT_1_unpaired.fq.gz \
 trimmed_MBLAT_2_paired.fq.gz trimmed_MBLAT_2_unpaired.fq.gz \
 ILLUMINACLIP:/home/CAM/qlin/resource/adapters.fa:2:20:10 \
 LEADING:20 \
 TRAILING:20 \
 SLIDINGWINDOW:4:20 \
 MINLEN:50

rm MBDOR_CGTACG_L002_R1_001.fastq.gz MBLAT_GAGTGG_L002_R1_001.fastq.gz MBDOR_CGTACG_L002_R2_001.fastq.gz MBLAT_GAGTGG_L002_R2_001.fastq.gz

module load hisat2/2.0.5

ref=/home/CAM/qlin/resource/LF10/LF10g_v2.0.fa

hisat2-build -f -p 16 $ref ./LF10

hisat2 -q --phred33 -p 16 \
 -x ./LF10 \
 -1 trimmed_MBDOR_1_paired.fq.gz \
 -2 trimmed_MBDOR_2_paired.fq.gz \
 -U trimmed_MBDOR_1_unpaired.fq.gz,trimmed_MBDOR_2_unpaired.fq.gz \
 -S trimmed_MBDOR.sam

hisat2 -q --phred33 -p 16 \
 -x ./LF10 \
 -1 trimmed_MBLAT_1_paired.fq.gz \
 -2 trimmed_MBLAT_2_paired.fq.gz \
 -U trimmed_MBLAT_1_unpaired.fq.gz,trimmed_MBLAT_2_unpaired.fq.gz \
 -S trimmed_MBLAT.sam

module load samtools

samtools view -bST $ref -@ 16 trimmed_MBDOR.sam > trimmed_MBDOR.bam
rm trimmed_MBDOR.sam

samtools view -bST $ref -@ 16 trimmed_MBLAT.sam > trimmed_MBLAT.bam
rm trimmed_MBLAT.sam

samtools merge MB.bam trimmed_MBDOR.bam trimmed_MBLAT.bam
samtools sort -o MB.sort.bam -T MB_tmp -O bam -@ 16 MB.bam

module load bcftools

bcftools mpileup -Ou -f $ref --threads 16 MB.sort.bam | bcftools call -mv --threads 16 -Oz -o bicolor.vcf.gz
bcftools norm -f $ref --threads 16 bicolor.vcf.gz -Ob -o bicolor.norm.bcf
bcftools filter -i'%QUAL>20' --threads 16 bicolor.norm.bcf -Oz -o bicolor.norm.flt.vcf.gz
bcftools index bicolor.norm.flt.vcf.gz
cat $ref | bcftools consensus bicolor.norm.flt.vcf.gz > bicolor_consensus.fa

module load RepeatMasker/4.0.6
module unload perl

mkdir repeatMasker_20190215_soft
RepeatMasker bicolor_consensus.fa -dir ./repeatMasker_20190215_soft -lib /home/CAM/qlin/LF10_Genome/repeatLibrary/Ml_all_repeats_20190215.ch.fasta -xsmall -pa 16 -nolow

ref=bicolor_consensus.fa

hisat2-build -f -p 16 $ref ./bicolor

hisat2 -q --phred33 -p 16 \
 -x ./bicolor \
 -1 trimmed_MBDOR_1_paired.fq.gz \
 -2 trimmed_MBDOR_2_paired.fq.gz \
 -U trimmed_MBDOR_1_unpaired.fq.gz,trimmed_MBDOR_2_unpaired.fq.gz \
 -S trimmed_MBDOR_2MB.sam

hisat2 -q --phred33 -p 16 \
 -x ./bicolor \
 -1 trimmed_MBLAT_1_paired.fq.gz \
 -2 trimmed_MBLAT_2_paired.fq.gz \
 -U trimmed_MBLAT_1_unpaired.fq.gz,trimmed_MBLAT_2_unpaired.fq.gz \
 -S trimmed_MBLAT_2MB.sam

samtools view -bST $ref -@ 16 trimmed_MBDOR_2MB.sam > trimmed_MBDOR_2MB.bam
rm trimmed_MBDOR_2MB.sam
samtools sort -o trimmed_MBDOR_2MB.sort.bam -T trimmed_MBDOR_2MB_tmp -O bam -@ 16 trimmed_MBDOR_2MB.bam

samtools view -bST $ref -@ 16 trimmed_MBLAT_2MB.sam > trimmed_MBLAT_2MB.bam
rm trimmed_MBLAT_2MB.sam
samtools sort -o trimmed_MBLAT_2MB.sort.bam -T trimmed_MBLAT_2MB_tmp -O bam -@ 16 trimmed_MBLAT_2MB.bam

module load BRAKER/2.0.5
module load bamtools/2.4.1
PATH=/home/CAM/qlin/augustus/3.2.3/bin:/home/CAM/qlin/augustus/3.2.3/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=$HOME/augustus/3.2.3/config/
export GENEMARK_PATH=/labs/Wegrzyn/local_software/gm_et_linux_64/gmes_petap/
export BLAST_PATH=/isg/shared/apps/blast/ncbi-blast-2.7.1+/bin/
module unload perl/5.28.1
module load perl/5.24.0
export PERL5LIB=/labs/Wegrzyn/perl5/lib/perl5/

braker.pl \
 --cores=16 \
 --genome=repeatMasker_20190215_soft/bicolor_consensus.fa.masked \
 --softmasking 1 \
 --bam=trimmed_MBDOR_2MB.sort.bam,trimmed_MBLAT_2MB.sort.bam \
 --prot_seq=/home/CAM/qlin/resource/guttatus/v2.0/annotation/Mguttatus_256_v2.0.protein.fa \
 --prg=gth

mkdir genomes
cp ~/resource/*/*g_v2.0.protein* ./genomes/
cp braker/Sp*/augustus.hints.aa ./genomes/bicolor.fa
/isg/shared/apps/OrthoFinder/2.2.7/orthofinder -f genomes -t 16




