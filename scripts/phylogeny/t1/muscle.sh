#!/bin/bash
#SBATCH --job-name=muscle
#SBATCH -a 0-746
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=1G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o muscle_%A_%a.out
#SBATCH -e muscle_%A_%a.err

module load muscle
module load seqkit
module load samtools

orthogroup=~/Mimulus_Genomes/results/phylogeny/t1/genomes/Results_Jun29/Orthogroups.csv
split -l 20 ~/Mimulus_Genomes/results/phylogeny/t1/genomes/Results_Jun29/SingleCopyOrthogroups.txt singleOG
file=(singleOG*)
while read line; do   array+=($line);   done < ${file[$SLURM_ARRAY_TASK_ID]}
for og in ${array[@]}
do
	mc=`grep $og $orthogroup | cut -f2`
	ml=`grep $og $orthogroup | cut -f3`
	mp=`grep $og $orthogroup | cut -f4`
	mV=`grep $og $orthogroup | cut -f5`
	mb=`grep $og $orthogroup | cut -f6 | sed -r 's/\s*$//'`

	samtools faidx ~/Mimulus_Genomes/results/phylogeny/t1/genomes/LF10g_v2.0.protein.fa $ml >> $og\.prot.fa
	samtools faidx ~/Mimulus_Genomes/results/phylogeny/t1/genomes/CE10g_v2.0.protein.fa $mc >> $og\.prot.fa
	samtools faidx ~/Mimulus_Genomes/results/phylogeny/t1/genomes/MvBLg_v2.0.protein.fa $mV >> $og\.prot.fa
	samtools faidx ~/Mimulus_Genomes/results/phylogeny/t1/genomes/Mparg_v2.0.protein.fa $mp >> $og\.prot.fa
	samtools faidx ~/Mimulus_Genomes/results/phylogeny/t1/genomes/bicolor.fa $mb >> $og\.prot.fa
	muscle -in $og\.prot.fa -out $og\.prot.aln

	samtools faidx /home/CAM/qlin/resource/LF10/LF10g_v2.0.codingseq.fa $ml >> $og\.nuc.fa
	samtools faidx /home/CAM/qlin/resource/CE10/CE10g_v2.0.codingseq.fa $mc >> $og\.nuc.fa 
	samtools faidx /home/CAM/qlin/resource/mvbl/MvBLg_v2.0.codingseq.fa $mV >> $og\.nuc.fa 
	samtools faidx /home/CAM/qlin/resource/parisii/Mparg_v2.0.codingseq.fa $mp >> $og\.nuc.fa 
	samtools faidx bicolor.codingseq.fa $mb >> $og\.nuc.fa

	/isg/shared/apps/seqkit/0.10.0/seqkit sort -n $og\.prot.aln -o $og\.prot.sort.aln
	/isg/shared/apps/seqkit/0.10.0/seqkit sort -n $og\.nuc.fa -o $og\.nuc.sort.fa

	perl ~/local/apps/pal2nal.v14/pal2nal.pl $og\.prot.sort.aln $og\.nuc.sort.fa -output fasta > $og\.fa

#	gap=`cat $og\.fa |tr -cd '-'|wc -c`
#	if [ "$gap" -eq "0" ]; then
#		rm $og\.fa;
#	fi
	rm $og\.prot.fa $og\.nuc.fa $og\.prot.aln $og\.nuc.sort.fa $og\.prot.sort.aln
done
#cat *paml > all.paml.fa
#rm singleOG*

mkdir SingleCopyOrthologAlignment_bicolor
array=()
while read line; do   array+=($line);   done < filteredOGlist.txt
for og in ${array[@]}
do 
	mv $og\.fa SingleCopyOrthologAlignment_bicolor/
done

