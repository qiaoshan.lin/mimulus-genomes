import re,sys,glob,os,subprocess,json,math

paup = '/home/CAM/qlin/local/apps/paup4a166_centos64'
fadir = './SingleCopyOrthologAlignment_bicolor_filteredByGaps'
nexdir = 'nexus'

#nexpath = os.path.join('.',nexdir)
#if not os.path.exists(nexpath):
#    os.mkdir(nexpath)

paup_block = '''
begin paup;
  log file=pauplog.txt start replace;
  exe concat.nex;
  outgroup guttatus;
  SVDQuartets evalQuartets=all;
  roottrees rootmethod=outgroup;
  set criterion=likelihood;
  lset nst=6 rmatrix=estimate basefreq=estimate rate=gamma shape=estimate clock;
  lscores 1;
  savetrees file=svgq.tre brlens replace;
  [SVDQuartets evalQuartets=all bootstrap=standard nreps=100 treefile=svdqboot.tre;]
  log stop;
  quit;
end;'''

fasta_files = glob.glob('./%s/OG*.fa' % fadir)
nfiles = len(fasta_files)
print 'Found %d fasta files in directory "%s"' % (nfiles, fadir)

#files_in_nexdir = glob.glob('./%s/*' % nexdir)
#if len(files_in_nexdir) > 0:
#    answer = raw_input('OK to remove all nexus files from directory "%s"? (y/n)' % nexdir)
#    if answer == 'y':
#        print 'Removing existing nexus files'
#        for fn in files_in_nexdir:
#            os.remove(fn)
#    else:
#        sys.exit('Aborting because you did not answer y')
                
def writeNexusFile(datafn, ntax, nchar, mask, taxa, sequences, paup, paupfn):
    if os.path.exists(datafn):
        os.rename(datafn, '%s.bak' % datafn)
    longest = max([len(t) for t in taxa])
    taxonfmt = '  %%%ds' % longest
    f = open(datafn, 'w')
    f.write('#nexus\n\n')
    f.write('begin data;\n')
    f.write('  dimensions ntax=%d nchar=%d;\n' % (ntax, nchar))
    f.write('  format datatype=dna gap=-;\n')
    f.write('  matrix\n')
    if mask is not None:
        f.write(taxonfmt % ' ')
        f.write('[%s]\n' % mask)
    for t in taxa:
        taxon_name = re.sub('\s+', '_', t)
        f.write(taxonfmt % taxon_name)
        f.write(' %s\n' % sequences[t])
    f.write('  ;\n')
    f.write('end;\n')
    f.close()
    
    if paup is not None:
        f = open(paupfn, 'w')
        f.write('#nexus\n\n')
        f.write('%s\n' % paup)
        f.close()
        
# Read in and store data from all fasta files
alignment = {'lewisii':'', 'cardinalis':'', 'parishii':'', 'verbenacea':'', 'guttatus':''}
for fn in fasta_files:
    dir,base = os.path.split(fn)
    
    m = re.match('(\S+)[.]fa', base)
    assert m is not None
    orthogroup = int(m.group(1)[2:])
    
    stuff = open(fn,'r').read()
    parts = stuff.split('>')
    nparts = len(parts)
    if nparts != 6:
        print '*** File "%s" has %d sequences, but I was expecting 5' % (base,nparts-1)
        continue

    sequences = {'lewisii':'', 'cardinalis':'', 'parishii':'', 'verbenacea':'', 'guttatus':''}
    for i in range(1,nparts):  
        tmp = parts[i].strip()
        m = re.match('(\S+)\s+(.+)', tmp, re.M | re.S)
        assert m is not None

        # Extract chromosome and gene info from taxon name
        taxon_abbr = m.group(1)[:2].upper()
        taxon_label = 'sinnombre'
        if taxon_abbr[0] == 'G':
            taxon_label = 'guttatus'
        elif taxon_abbr[0] == 'M':
            mm = re.match('(\S+)(\d+)G([0-9.]+)', m.group(1))
            if mm is None:
                print 'regex fail: %s' % m.group(1)
            else:
                taxon = mm.group(1).upper()
                if taxon == 'MC':
                    taxon_label = 'cardinalis'
                elif taxon == 'ML':
                    taxon_label = 'lewisii'
                elif taxon == 'MP':
                    taxon_label = 'parishii'
                elif taxon == 'MV':
                    taxon_label = 'verbenacea'
        else:
            print 'fn =',fn
            print 'taxon name =',m.group(1)
            assert(False)
            
        # Extract sequence
        if (taxon_label == 'lewisii') or (taxon_label == 'cardinalis') or (taxon_label == 'parishii') or (taxon_label == 'verbenacea') or (taxon_label == 'guttatus'):
            sequence = re.sub('\s+', '', m.group(2)).upper()
            sequences[taxon_label] += sequence

    seqlen = len(sequences['lewisii'])
    ok = seqlen > 0
    if ok:
        ok = len(sequences['cardinalis']) == seqlen
    if ok:
        ok = len(sequences['parishii']) == seqlen
    if ok:
        ok = len(sequences['verbenacea']) == seqlen
    if ok:
        ok = len(sequences['guttatus']) == seqlen
    if ok:
        alignment['lewisii']    += sequences['lewisii']     
        alignment['cardinalis'] += sequences['cardinalis']     
        alignment['parishii']   += sequences['parishii']     
        alignment['verbenacea'] += sequences['verbenacea']
        alignment['guttatus']   += sequences['guttatus']

############# PAUP ANALYSES #############
# Create nexus file for concatenated data
ntax = 5
nchar = len(alignment['lewisii'])
taxon_names = ['lewisii', 'cardinalis', 'parishii', 'verbenacea', 'guttatus']
mask = None #'-'*nchar
writeNexusFile('concat.nex', ntax, nchar, mask, taxon_names, alignment, paup_block, 'paup.nex') 

# Run PAUP* on concatenated data   
print 'Running PAUP*...'     
o,e = subprocess.Popen([paup,'paup.nex'], stdout = subprocess.PIPE, stderr = subprocess.PIPE).communicate()
print 'Done.'
    

