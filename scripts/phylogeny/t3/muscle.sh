#!/bin/bash
#SBATCH --job-name=muscle
#SBATCH -a 0-689
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=1G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o muscle_%A_%a.out
#SBATCH -e muscle_%A_%a.err

module load muscle
module load seqkit
module load samtools/1.7

workingDir=~/Mimulus_Genomes/results/phylogeny/t3
cd $workingDir

orthogroup=~/Mimulus_Genomes/results/orthofinder/t2/Results_Oct24/Orthogroups.csv
#split -l 20 ~/Mimulus_Genomes/results/orthofinder/t2/Results_Oct24/SingleCopyOrthogroups.txt singleOG
#perl -lane 'if(/(^>.*?.p)\s/){print $1}else{print}' ~/resource/Mgut/v2.0/annotation/Mguttatus_256_v2.0.protein_primaryTranscriptOnly.fa > Mguttatus_256_v2.0.protein_primaryTranscriptOnly.m.fa
#perl -lane 'if(/(^>.*?)\s/){print $1,".p"}else{print}' ~/resource/Mgut/v2.0/annotation/Mguttatus_256_v2.0.cds_primaryTranscriptOnly.fa > Mguttatus_256_v2.0.cds_primaryTranscriptOnly.m.fa

file=(singleOG*)
while read line; do   array+=($line);   done < ${file[$SLURM_ARRAY_TASK_ID]}
for og in ${array[@]}
do
	mc=`grep $og $orthogroup | cut -f2`
	ml=`grep $og $orthogroup | cut -f3`
	mg=`grep $og $orthogroup | cut -f4`
	mp=`grep $og $orthogroup | cut -f5`
	mV=`grep $og $orthogroup | cut -f6 | sed -r 's/\s*$//'`

	samtools faidx /home/CAM/qlin/Mimulus_Genomes/results/annotation/CE10/CE10g_v2.0beta.longest_isoform.protein.fa $mc >> $og\.prot.fa
	samtools faidx /home/CAM/qlin/Mimulus_Genomes/results/annotation/LF10/LF10g_v2.0beta.longest_isoform.protein.fa $ml >> $og\.prot.fa
	samtools faidx /home/CAM/qlin/Mimulus_Genomes/results/annotation/Mpar/Mparg_v2.0beta.longest_isoform.protein.fa $mp >> $og\.prot.fa
	samtools faidx /home/CAM/qlin/Mimulus_Genomes/results/annotation/MvBL/MvBLg_v2.0beta.longest_isoform.protein.fa $mV >> $og\.prot.fa

	samtools faidx Mguttatus_256_v2.0.protein_primaryTranscriptOnly.m.fa $mg >> $og\.prot.fa

	muscle -in $og\.prot.fa -out $og\.prot.aln

	samtools faidx /home/CAM/qlin/Mimulus_Genomes/results/annotation/CE10/CE10g_v2.0beta.longest_isoform.codingseq.fa $mc >> $og\.nuc.fa
	samtools faidx /home/CAM/qlin/Mimulus_Genomes/results/annotation/LF10/LF10g_v2.0beta.longest_isoform.codingseq.fa $ml >> $og\.nuc.fa
	samtools faidx /home/CAM/qlin/Mimulus_Genomes/results/annotation/Mpar/Mparg_v2.0beta.longest_isoform.codingseq.fa $mp >> $og\.nuc.fa
	samtools faidx /home/CAM/qlin/Mimulus_Genomes/results/annotation/MvBL/MvBLg_v2.0beta.longest_isoform.codingseq.fa $mV >> $og\.nuc.fa
	
	samtools faidx Mguttatus_256_v2.0.cds_primaryTranscriptOnly.m.fa $mg >> $og\.nuc.fa

	/isg/shared/apps/seqkit/0.10.0/seqkit sort -n $og\.prot.aln -o $og\.prot.sort.aln
	/isg/shared/apps/seqkit/0.10.0/seqkit sort -n $og\.nuc.fa -o $og\.nuc.sort.fa

	perl ~/local/apps/pal2nal.v14/pal2nal.pl $og\.prot.sort.aln $og\.nuc.sort.fa -output fasta > $og\.fa

#	gap=`cat $og\.fa |tr -cd '-'|wc -c`
#	if [ "$gap" -eq "0" ]; then
#		rm $og\.fa;
#	fi
	rm $og\.prot.fa $og\.nuc.fa $og\.prot.aln $og\.nuc.sort.fa $og\.prot.sort.aln
done
#rm singleOG*
#mkdir alignments
#mv OG00* alignments/

