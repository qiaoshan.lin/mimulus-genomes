import re,sys,glob,os,subprocess,json,math

paup = '/home/CAM/qlin/local/apps/paup4a166_centos64'
fadir = '../../../results/phylogeny/t3/filtered_alignments'
nexdir = 'nexus'

#nexpath = os.path.join('.',nexdir)
#if not os.path.exists(nexpath):
#    os.mkdir(nexpath)

paup_block = '''
begin paup;
  log file=pauplog.txt start replace;
  exe concat.nex;
  outgroup guttatus;
  SVDQuartets evalQuartets=all;
  roottrees rootmethod=outgroup;
  set criterion=likelihood;
  lset nst=6 rmatrix=estimate basefreq=estimate rate=gamma shape=estimate clock;
  lscores 1;
  savetrees file=svgq.tre brlens replace;
  SVDQuartets evalQuartets=all bootstrap=standard nreps=100 treefile=svdqboot.tre;
  log stop;
  quit;
end;'''

fasta_files = glob.glob('./%s/OG*.fa' % fadir)
nfiles = len(fasta_files)
print 'Found %d fasta files in directory "%s"' % (nfiles, fadir)

#files_in_nexdir = glob.glob('./%s/*' % nexdir)
#if len(files_in_nexdir) > 0:
#    answer = raw_input('OK to remove all nexus files from directory "%s"? (y/n)' % nexdir)
#    if answer == 'y':
#        print 'Removing existing nexus files'
#        for fn in files_in_nexdir:
#            os.remove(fn)
#    else:
#        sys.exit('Aborting because you did not answer y')
                
def writeNexusFile(datafn, ntax, nchar, mask, taxa, sequences, paup, paupfn):
    if os.path.exists(datafn):
        os.rename(datafn, '%s.bak' % datafn)
    longest = max([len(t) for t in taxa])
    taxonfmt = '  %%%ds' % longest
    f = open(datafn, 'w')
    f.write('#nexus\n\n')
    f.write('begin data;\n')
    f.write('  dimensions ntax=%d nchar=%d;\n' % (ntax, nchar))
    f.write('  format datatype=dna gap=-;\n')
    f.write('  matrix\n')
    if mask is not None:
        f.write(taxonfmt % ' ')
        f.write('[%s]\n' % mask)
    for t in taxa:
        taxon_name = re.sub('\s+', '_', t)
        f.write(taxonfmt % taxon_name)
        f.write(' %s\n' % sequences[t])
    f.write('  ;\n')
    f.write('end;\n')
    f.close()
    
    if paup is not None:
        f = open(paupfn, 'w')
        f.write('#nexus\n\n')
        f.write('%s\n' % paup)
        f.close()
        
# Read in and store data from all fasta files
alignment = {'lewisii':'', 'cardinalis':'', 'parishii':'', 'verbenaceus':'', 'guttatus':''}
for fn in fasta_files:
    dir,base = os.path.split(fn)
    
    m = re.match('(\S+)[.]fa', base) # extract file name prefix
    assert m is not None
    orthogroup = int(m.group(1)[2:]) # extract orthogroup number
    
    stuff = open(fn,'r').read() # read the whole file into stuff
    parts = stuff.split('>')
    nparts = len(parts) # if there are 5 sequences, there should be 6 parts with the first blank
    if nparts != 6:
        print '*** File "%s" has %d sequences, but I was expecting 8' % (base,nparts-1)
        continue

    sequences = {'lewisii':'', 'cardinalis':'', 'parishii':'', 'verbenaceus':'', 'guttatus':''}
    for i in range(1,nparts):  # for each DNA sequence with name / for each species
        tmp = parts[i].strip() # remove space from head/tail
        m = re.match('(\S+)\s+(.+)', tmp, re.M | re.S) # match gene name and sequence seperately 
        assert m is not None

        # Extract chromosome and gene info from taxon name
        taxon_abbr = m.group(1)[:2].upper() # extract the first two characters of gene names and transform to uppercases
        taxon_label = 'init'
        if taxon_abbr[1] == 'T':
            taxon_label = 'Arabidopsis'
	elif taxon_abbr[1] == 'M':
            taxon_label = 'snapdragon'
        elif taxon_abbr[1] == 'C':
            taxon_label = 'cardinalis'
        elif taxon_abbr[1] == 'L':
            taxon_label = 'lewisii'
        elif taxon_abbr[1] == 'P':
            taxon_label = 'parishii'
        elif taxon_abbr[1] == 'V':
            taxon_label = 'verbenaceus'
        elif taxon_abbr[1] == 'I':
            taxon_label = 'guttatus'
        elif taxon_abbr[1] == 'O':
            taxon_label = 'tomato'
        else:
            print 'fn =',fn # if the name can't match anything defined above, print fasta file name
            print 'taxon name =',m.group(1) # and print the gene name
            assert(False)
            
        # Extract sequence
        if (taxon_label == 'lewisii') or (taxon_label == 'cardinalis') or (taxon_label == 'parishii') or (taxon_label == 'verbenaceus') or (taxon_label == 'guttatus') or (taxon_label == 'snapdragon') or (taxon_label == 'tomato') or (taxon_label == 'Arabidopsis'):
            sequence = re.sub('\s+', '', m.group(2)).upper()
            sequences[taxon_label] += sequence

    seqlen = len(sequences['lewisii'])
    ok = seqlen > 0
    if ok:
        ok = len(sequences['cardinalis']) == seqlen
    if ok:
        ok = len(sequences['parishii']) == seqlen
    if ok:
        ok = len(sequences['verbenaceus']) == seqlen
    if ok:
        ok = len(sequences['guttatus']) == seqlen
    if ok:
        alignment['lewisii']    += sequences['lewisii']     
        alignment['cardinalis'] += sequences['cardinalis']     
        alignment['parishii']   += sequences['parishii']     
        alignment['verbenaceus'] += sequences['verbenaceus']
        alignment['guttatus']   += sequences['guttatus']

############# PAUP ANALYSES #############
# Create nexus file for concatenated data
ntax = 5
nchar = len(alignment['lewisii'])
taxon_names = ['lewisii', 'cardinalis', 'parishii', 'verbenaceus', 'guttatus']
mask = None #'-'*nchar
writeNexusFile('concat.nex', ntax, nchar, mask, taxon_names, alignment, paup_block, 'paup.nex') 

# Run PAUP* on concatenated data   
print 'Running PAUP*...'     
o,e = subprocess.Popen([paup,'paup.nex'], stdout = subprocess.PIPE, stderr = subprocess.PIPE).communicate()
print 'Done.'
    

