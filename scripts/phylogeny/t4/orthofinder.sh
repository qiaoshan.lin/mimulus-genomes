#!/bin/bash
#SBATCH --job-name=orthofinder
#SBATCH -n 1
#SBATCH -N 1
#SBATCH -c 16
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=100G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o orthofinder_%j.out
#SBATCH -e orthofinder_%j.err

workingdir=/home/CAM/qlin/Mimulus_Genomes/results/phylogeny/t4
cd $workingdir
cp ~/Mimulus_Genomes/results/annotation/????/????g_v2.0beta.longest_isoform.protein.fa ./
cp ~/bicolor_annotation/t3/bicolor_v3.protein.fa ./

module load OrthoFinder/2.2.7

/isg/shared/apps/OrthoFinder/2.2.7/orthofinder -f $workingdir -t 16


