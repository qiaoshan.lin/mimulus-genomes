#!/bin/bash
#SBATCH --job-name=filter
#SBATCH -c 1
#SBATCH -n 1
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=1G
#SBATCH --mail-user=qiaoshan.lin@uconn.edu
#SBATCH -o filter_%j.out
#SBATCH -e filter_%j.err

module load muscle
module load seqkit
module load samtools

workingDir=~/Mimulus_Genomes/results/phylogeny/t5
cd $workingDir

mkdir filtered_alignments

file=(alignments/*)
for og in ${file[@]}
do
	f=`perl ~/scripts/Nstat.pl $og - |awk -v og="$og" '$2>0.1{print og}'|uniq`
	if [ "$f" == "" ]; then
		cp $og filtered_alignments/
	fi
done


