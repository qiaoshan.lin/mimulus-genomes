## *Mimulus lewisii* Species Complex Genome Analysis

#### Table of content

[reads](https://gitlab.com/qiaoshan.lin/mimulus-genomes#reads)

[assembly](https://gitlab.com/qiaoshan.lin/mimulus-genomes#assembly)

[annotation](https://gitlab.com/qiaoshan.lin/mimulus-genomes#annotation)

[genome assessment and annotation statistics](https://gitlab.com/qiaoshan.lin/mimulus-genomes#genome-assessment-and-annotation-statistics) 

[genome size analysis](https://gitlab.com/qiaoshan.lin/mimulus-genomes#genome-size-analysis)

[TE analysis](https://gitlab.com/qiaoshan.lin/mimulus-genomes#TE-analysis)

[synteny analysis](https://gitlab.com/qiaoshan.lin/mimulus-genomes#synteny-analysis)

[gene expression analysis](https://gitlab.com/qiaoshan.lin/mimulus-genomes#gene-expression-analysis)

[find orthologues](https://gitlab.com/qiaoshan.lin/mimulus-genomes#find-orthologues)

[construct phylogeny](https://gitlab.com/qiaoshan.lin/mimulus-genomes#construct-phylogeny)

[find positive selections](https://gitlab.com/qiaoshan.lin/mimulus-genomes#find-positive-selections)

de novo gene identification

[gene family gain and loss](https://gitlab.com/qiaoshan.lin/mimulus-genomes#gene-family-gain-and-loss)

structral variants analysis

#### reads

<details>
<summary>1.  FastQC for PE reads</summary>
</details>

<details>
<summary>2.  Statistics for PacBio reads</summary>

> run [reads_stat/PacBio_Read_Stat.pl](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/reads_stat/PacBio_Read_Stat.pl)

</details>

<details>
<summary>3.  FastQC for RNAseq reads</summary>
</details>

#### assembly

<details>
<summary>1.  Trim Illumina PE reads by trimmomatic</summary>

> sbatch [assembly/trim/trim_ml.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/assembly/trim/trim_ml.sh)

> sbatch [assembly/trim/trim_mc.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/assembly/trim/trim_mc.sh)

> sbatch [assembly/trim/trim_mp.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/assembly/trim/trim_mp.sh)

> sbatch [assembly/trim/trim_mv.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/assembly/trim/trim_mv.sh)

</details>

<details>
<summary>2.  Align trimmed reads onto canu-assembled contigs</summary>

> sbatch [assembly/bwa/bwa_ml.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/assembly/bwa/bwa_ml.sh)

> sbatch [assembly/bwa/bwa_mc.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/assembly/bwa/bwa_mc.sh)

> sbatch [assembly/bwa/bwa_mp.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/assembly/bwa/bwa_mp.sh)

> sbatch [assembly/bwa/bwa_mv.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/assembly/bwa/bwa_mv.sh)

</details>

<details>
<summary>3.  Polish contigs by Pilon</summary>

> sbatch [assembly/pilon/split_ml.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/assembly/pilon/split_ml.sh)

> sbatch [assembly/pilon/split_mc.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/assembly/pilon/split_mc.sh)

> sbatch [assembly/pilon/split_mp.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/assembly/pilon/split_mp.sh)

> sbatch [assembly/pilon/split_mv.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/assembly/pilon/split_mv.sh)

> sbatch [assembly/pilon/pilon_ml.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/assembly/pilon/pilon_ml.sh)

> sbatch [assembly/pilon/pilon_mc.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/assembly/pilon/pilon_mc.sh)

> sbatch [assembly/pilon/pilon_mp.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/assembly/pilon/pilon_mp.sh)

> sbatch [assembly/pilon/pilon_mv.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/assembly/pilon/pilon_mv.sh)

</details>

<details>
<summary>4.  Remove contamination</summary>

> Remove contigs of coverage<0 and those aligned to other genomes in NCBI

</details>

<details>
<summary>5.  Seperate mitochondria DNA and chloroplast DNA from nucleus DNA (to produce v1.91)</summary>

> If a contig has 95% identity to chloroplast reference, it is highly possible that this contig is cpDNA. Then manually check the length coverage of this contig against cpDNA. If most of the length is covered by cpDNA, this contig must be cpDNA. If only part of the length is covered, check further by blasting this contig to the other 3 species. If the contig has some parts that are not aligned to cpDNA but can align to other species, there might be a misassembly in this contig. If the parts not aligned to cpDNA poorly align to other species, this might be a contamination. 

</details>

<details>
<summary>6.  Concatenate contigs (to produce v1.92)</summary>

(1) Mask the genomes by repeatMasker

(2) Self-alignment by MUMmer3

(3) Filter the results by [assembly/catenation/Nratio.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/assembly/catenation/Nratio.sh)

(4) Manually concatenate contigs

(5) Remove redundant contigs and add in merged contigs

</details>

<details>
<summary>7.  Correct assembly by synteny (to produce v1.93)</summary>

</details>

<details>
<summary>8.  Scaffold contigs (to produce v2.0)</summary>

(1) Prepare a table of ordered contigs

(2) Link contigs in order

> sbatch [assembly/scaffold/contig_linker.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/assembly/scaffold/contig_linker.sh)

(3) Rename scaffolds/contigs


</details>

<details>
<summary>9.  Assemble genomes by Flye</summary>

> sbatch [assembly/flye/flye_mc.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/assembly/flye/flye_mc.sh)

> sbatch [assembly/flye/flye_mp.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/assembly/flye/flye_mp.sh)

> sbatch [assembly/flye/flye_mv.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/assembly/flye/flye_mv.sh)

</details>

#### annotation

<details>
<summary>1.  Trim RNAseq reads</summary>

> sbatch [annotation/trim/trim_ml.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/trim/trim_ml.sh)

> sbatch [annotation/trim/trim_mc.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/trim/trim_mc.sh)

> sbatch [annotation/trim/trim_mv.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/trim/trim_mv.sh)

</details>

<details>
<summary>2.  Align reads by HISAT2</summary>

>  sbatch [annotation/hisat2/genome_index.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/hisat2/genome_index.sh)

>  sbatch [annotation/hisat2/hisat2_mc_1.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/hisat2/hisat2_mc_1.sh)

>  sbatch [annotation/hisat2/hisat2_mc_2.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/hisat2/hisat2_mc_2.sh)

>  sbatch [annotation/hisat2/hisat2_ml_1.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/hisat2/hisat2_ml_1.sh)

>  sbatch [annotation/hisat2/hisat2_ml_2.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/hisat2/hisat2_ml_2.sh)

>  sbatch [annotation/hisat2/hisat2_mp_1.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/hisat2/hisat2_mp_1.sh)

>  sbatch [annotation/hisat2/hisat2_mp_2.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/hisat2/hisat2_mp_2.sh)

>  sbatch [annotation/hisat2/hisat2_mv_1.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/hisat2/hisat2_mv_1.sh)

>  sbatch [annotation/hisat2/hisat2_mv_2.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/hisat2/hisat2_mv_2.sh)

</details>

<details>
<summary>3.  Annotate gene structures by BRAKER2</summary>

> sbatch [annotation/braker2/braker_mc.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/braker2/braker_mc.sh)

> sbatch [annotation/braker2/braker_ml.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/braker2/braker_ml.sh)

> sbatch [annotation/braker2/braker_mp.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/braker2/braker_mp.sh)

> sbatch [annotation/braker2/braker_mv.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/braker2/braker_mv.sh)

some statistics of braker2 raw results:

> sbatch [annotation/braker2/txt.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/braker2/txt.sh)

</details>

<details>
<summary>4.  Filter genes for generating v2.0</summary>

(1) Filter out genes masked by 50% repeats 

> sbatch [annotation/filter/repeatMasker_20190215.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/filter/repeatMasker_20190215.sh)

(2) Generate a transcriptome version which has redundant isoforms removed

> sbatch [annotation/filter/redundancy_filter.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/filter/redundancy_filter.sh)

(3) Rename the transcriptome and proteome

> sbatch [annotation/filter/gtf_filter.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/filter/gtf_filter.sh)

> sbatch [annotation/filter/name_gtf.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/filter/name_gtf.sh)

> sbatch [annotation/filter/name_codingseq.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/filter/name_codingseq.sh)

> sbatch [annotation/filter/name_protein.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/filter/name_protein.sh)

(4) Generate a transcriptome version of only longest isoforms

> [annotation/filter/longest_isoform.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/filter/longest_isoform.sh)

</details>

<details>
<summary>5.  Functional annotation by EnTAP</summary>

> sbatch [annotation/entap/entap.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/entap/entap.sh)

</details>

<details>
<summary>6.  Further filtering for generating v2.0beta</summary>

> sbatch [annotation/filter/orthofinder_filter.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/filter/orthofinder_filter.sh)

> sbatch [annotation/filter/repeatMasker_20191010.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/filter/repeatMasker_20191010.sh)

> sbatch [annotation/filter/gtf_filter_beta.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/filter/gtf_filter_beta.sh)

> sbatch [annotation/filter/longest_isoform_beta.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/filter/longest_isoform_beta.sh)

This version is going to be the published version while the v2.0 will be kept in Mimubase still. In conclusion, we filtered the genes by the following criteria:
(1) the genes should not be masked more than 50% by TE library
(2) the genes should be assigned to an orthogroup by orthoFinder
(3) if a gene is in an orthogroup with only 2 or less species, its longest isoform should be longer than 450nt or should have at least one intron.

</details>

<details>
<summary>7.  Functional annotation by blasting M. guttatus proteins for v2.0beta</summary>

> sbatch [annotation/blastp/blastp_ml.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/blastp/blastp_ml.sh)

> sbatch [annotation/blastp/blastp_mc.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/blastp/blastp_mc.sh)

> sbatch [annotation/blastp/blastp_mp.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/blastp/blastp_mp.sh)

> sbatch [annotation/blastp/blastp_mv.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/annotation/blastp/blastp_mv.sh)

</details>

#### genome assessment and annotation statistics

<details>
<summary>1.  Run quast for genome assemblies</summary>

> sbatch [statistics/quast.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/statistics/quast.sh)

</details>

<details>
<summary>2.  Run ggstat for both genome assemblies and annotations</summary>

> sh [ggstat.sh](https://github.com/qslin/myTools) [statistics/conf](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/statistics/conf)

> sh [ggstat.sh](https://github.com/qslin/myTools) [statistics/conf_beta](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/statistics/conf_beta)

</details>

<details>
<summary>3.  Draw length distribution of transcripts/exons/introns</summary>

> sbatch [statistics/length_distribution.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/statistics/length_distribution.sh)

</details>

<details>
<summary>4.  Create files for circos</summary>

> run commands in [statistics/circos.txt](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/statistics/circos.txt)

</details>

<details>
<summary>5.  Run busco for both genome assemblies and proteins</summary>

> sh [assessment/busco.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/assessment/busco.sh)

</details>

<details>
<summary>6.  Calculate RNAseq reads identity to genome assemblies</summary>

> sh [assessment/rnaseq_identity_ml.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/assessment/rnaseq_identity_ml.sh)

> sh [assessment/rnaseq_identity_mv.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/assessment/rnaseq_identity_mv.sh)

</details>

<details>
<summary>7.  Estimate number of chloroplasts and mitochondria</summary>

> sbatch [statistics/cp_mt_number.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/statistics/cp_mt_number.sh)

select some single copy genes and create fasta files of these genes for each species

> sbatch [statistics/single_copy_gene_cov.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/statistics/single_copy_gene_cov.sh)

use the average depth of single copy genes as a reference to estimate the number of other elements

</details>

#### genome size analysis

<details>
<summary>1.  Align reads to cpDNA and mtDNA; then extract unmapped reads for genome size estimation</summary>

> sbatch [genome_size/bowtie2.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/genome_size/bowtie2.sh)

</details>

<details>
<summary>2.  Count kmers by Jellyfish</summary>

> sbatch [genome_size/jellyfish.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/genome_size/jellyfish.sh)

</details>

<details>
<summary>3.  Estimate genome size by [GenomeScope](http://qb.cshl.edu/genomescope/) (using 21mer)</summary>

> See results for [LF10](http://genomescope.org/analysis.php?code=ZG5BwvbdkmFblNcwyS2P), [CE10](http://genomescope.org/analysis.php?code=FZZ1rcOrWijoAyIfkSf8), [Mpar](http://genomescope.org/analysis.php?code=7lPN2Zp6HZxijCiM3BGm), [MvBL](http://genomescope.org/analysis.php?code=eKqYOzbaNgi8efj1olnI)

</details>

<details>
<summary>4.  Estimate genome size by myself (using 21,25,29,33mer)</summary>

> See results in [genome_size.gsheet](https://docs.google.com/spreadsheets/d/1jqziGvmFJVleNAwPMPfF6V15MZhQQ1dHpMI9MhAZBtE/edit#gid=0)

</details>

<details>
<summary>5.  Generate 21mers by [kmerGenerate.pl](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/genome_size/kmerGenerate.pl)</summary>

> sbatch [genome_size/kmer.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/genome_size/kmer.sh)

</details>

<details>
<summary>6.  Draw kmer spectrum for reads and genome assemblies</summary>

> run [genome_size/kmer_spectrum.r](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/genome_size/kmer_spectrum.r)

</details>

<details>
<summary>7.  Draw barplots of genome sizes</summary>

> run [genome_size/genome_size_barplots.r](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/genome_size/genome_size_barplots.r)

</details>

<details>
<summary>8.  Compare kmer frequences between reads and genomes</summary>

> sbatch [genome_size/kmer_freq_compare.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/genome_size/kmer_freq_compare.sh)

</details>

<details>
<summary>9. Align genomes to themselves by MUMmer3 to find redundant regions</summary>

> sbatch [genome_size/mummer.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/genome_size/mummer.sh)

</details>

<details>
<summary>10. Analyze the redundant regions and plot the distribution of redundancy</summary>

> sbatch [genome_size/mummer_analysis.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/genome_size/mummer_analysis.sh)

> run [genome_size/repeatcov.r](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/genome_size/repeatcov.r)

</details>

<details>
<summary>11. Map illumina reads back to genome assemblies and see if Mpar has the least coverage</summary>

> sbatch [genome_size/illumina_coverage.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/genome_size/illumina_coverage.sh)

</details>

#### TE analysis

<details>
<summary>1. Change fasta sequences' heads in repeat library for latter statistics</summary>

> sbatch [TE/change_head_chab.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/TE/change_head_chab.sh)

> sbatch [TE/change_head_ch.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/TE/change_head_ch.sh)

</details>

<details>
<summary>2. Mask genomes(v1.92) by repeatMasker</summary>

> sbatch [TE/repeatMasker_v1.92.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/TE/repeatMasker_v1.92.sh)

</details>

<details>
<summary>3. Mask genomes(v2.0) by repeatMasker</summary>

> sbatch [TE/repeatMasker_v2.0.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/TE/repeatMasker_v2.0.sh)

</details>


#### synteny analysis
<details>
<summary>1. 4 species (minspan=30)</summary>

get *g_v2.0beta.longest_isoform.codingseq.fa and *g_v2.0beta.gtf to [synteny/t1](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t1)

> sh [synteny/t1/prepare.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t1/prepare.sh)

> sh [synteny/t1/synteny_block_reference.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t1/synteny_block_reference.sh)

modify [synteny/t1/layout](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t1/layout) and [synteny/t1/seqids](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t1/seqids) to determine figure format

> sh [synteny/t1/mark_translocation.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t1/mark_translocation.sh)

manually copy the lines of inversions to mark from *.txt to *mark, extract the columns of gene names and use them to edit mark_inversion.sh

> sh [synteny/t1/mark_inversion.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t1/mark_inversion.sh)

if you want to remove all the synteny block marks or draw synteny figure without any mark, do: 

> sh [synteny/t1/mark_cleaning.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t1/mark_cleaning.sh)

</details>

<details>

<summary>2. 4 species (minspan=15)</summary>

get *g_v2.0beta.longest_isoform.codingseq.fa and *g_v2.0beta.gtf to [synteny/t2](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t2)

> sh [synteny/t2/prepare.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t2/prepare.sh)

> sh [synteny/t2/synteny_block_reference.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t2/synteny_block_reference.sh)

modify [synteny/t2/layout](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t2/layout) and [synteny/t2/seqids](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t2/seqids) to determine figure format

> sh [synteny/t2/mark_translocation.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t2/mark_translocation.sh)

manually copy the lines of inversions to mark from *.txt to *mark, extract the columns of gene names and use them to edit mark_inversion.sh

> sh [synteny/t2/mark_inversion.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t2/mark_inversion.sh)

if you want to remove all the synteny block marks or draw synteny figure without any mark, do:

> sh [synteny/t2/mark_cleaning.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t2/mark_cleaning.sh)

</details>

<details>

<summary>2. 5 species (minspan=30)</summary>

get *g_v2.0beta.longest_isoform.codingseq.fa and *g_v2.0beta.gtf to [synteny/t3](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t3)
get Mguttatus_256_v2.0.cds_primaryTranscriptOnly.fa and Mguttatus_256_v2.0.gene.gff3 to [synteny/t3](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t3)

> sh [synteny/t3/prepare.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t3/prepare.sh)

> sh [synteny/t3/synteny_block_reference.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t3/synteny_block_reference.sh)

modify [synteny/t3/layout](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t3/layout) and [synteny/t3/seqids](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t3/seqids) to determine figure format

> sh [synteny/t3/mark_translocation.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t3/mark_translocation.sh)

manually copy the lines of inversions to mark from *.txt to *mark, extract the columns of gene names and use them to edit mark_inversion.sh

> sh [synteny/t3/mark_inversion.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t3/mark_inversion.sh)

if you want to remove all the synteny block marks or draw synteny figure without any mark, do:

> sh [synteny/t3/mark_cleaning.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/synteny/t3/mark_cleaning.sh)

</details>



#### gene expression analysis

<details>
<summary>1.  Build index for CDS</summary>

> sbatch [expression/index.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/expression/index.sh)

</details>

<details>
<summary>2.  Quantify the gene expression level in different samples</summary>

> sbatch [expression/salmon_ml.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/expression/salmon_ml.sh)

> sbatch [expression/salmon_mv.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/expression/salmon_mv.sh)

</details>

#### find orthologues

<details>
<summary>1.  Find orthologues among the 4 species of lewisii complex</summary>

> sbatch [orthofinder/orthofinder_t1.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/orthofinder/orthofinder_t1.sh)

</details>

<details>
<summary>2.  Extract locations of genes that have multiple orthologues in one species but only one in the others. And draw the distribution of orthogroup counts across different number of such orthologues.</summary>

> sh [orthofinder/multicopy_genes.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/orthofinder/multicopy_genes.sh)

> sh [orthofinder/orthogroups_distribution.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/orthofinder/orthogroups_distribution.sh)

> run [orthofinder/orthogroups_distribution.r](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/orthofinder/orthogroups_distribution.r)

</details>

<details>
<summary>3.  Find orthologues among the 5 species of lewisii complex and guttatus</summary>

> sbatch [orthofinder/orthofinder_t2.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/orthofinder/orthofinder_t2.sh)

</details>

<details>
<summary>4.  Find orthologues among lewisii, guttatus, snapdragon, tomato, and Arabidopsis</summary>

> sbatch [orthofinder/orthofinder_t4.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/orthofinder/orthofinder_t4.sh)

</details>

<details>
<summary>5.  Draw Venn diagram of the orthologues counts from step 1,3 and 4</summary>

> run [orthofinder/orthogroups_venn_diagram.r](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/orthofinder/orthogroups_venn_diagram.r)

> use the [venn webtool](http://bioinformatics.psb.ugent.be/webtools/Venn/) to draw venn diagram for 5 sets
</details>

<details>
<summary>6.  Find orthologues among the lewisii complex, guttatus, snapdragon, tomato, and Arabidopsis</summary>

> sbatch [orthofinder/orthofinder_t3.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/orthofinder/orthofinder_t3.sh)

</details>

<details>
<summary>7.  Find tandem duplicated genes from step 1</summary>

> perl [scripts/orthofinder/find_tandem_dups.pl](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/orthofinder/find_tandem_dups.pl) Orthogroup.csv > find_tandem_dups.txt

</details>

#### construct phylogeny

<details>
<summary>1.  Use M. bicolor as an outgroup</summary>

(1) Annotate bicolor and find orthologues among the 5 species of lewisii complex and bicolor

> sbatch [phylogeny/t1/orthofinder.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/phylogeny/t1/orthofinder.sh)

(2) Align orthologues

> sbatch [phylogeny/t1/muscle.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/phylogeny/t1/muscle.sh)

(3) Filter alignments by maximum percentage of gaps (0.1)

> sbatch [phylogeny/t1/filterAlign.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/phylogeny/t1/filterAlign.sh)

(4) Build tree

> use python/2.7.9 to run [phylogeny/t1/concatenate.py](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/phylogeny/t1/concatenate.py)

</details>

<details>
<summary>2.  Use Arabidopsis as an outgroup</summary>

(1) Align single copy orthologues from [orthofinder/orthofinder_t3.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/orthofinder/orthofinder_t3.sh)

> sbatch [phylogeny/t2/muscle.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/phylogeny/t2/muscle.sh)

(2) Filter alignments by maximum percentage of gaps (0.1)

> sbatch [phylogeny/t2/filterAlign.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/phylogeny/t2/filterAlign.sh)

(3) Build tree

> use python/2.7.9 to run [phylogeny/t2/concatenate.py](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/phylogeny/t2/concatenate.py)

</details>

<details>
<summary>3.  Use M. gutattus as an outgroup</summary>

(1) Align single copy orthologues from [orthofinder/orthofinder_t2.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/orthofinder/orthofinder_t2.sh)

> sbatch [phylogeny/t3/muscle.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/phylogeny/t3/muscle.sh)

(2) Filter alignments by maximum percentage of gaps (0.1)

> sbatch [phylogeny/t3/filterAlign.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/phylogeny/t3/filterAlign.sh)

(3) Build tree

> use python/2.7.9 to run [phylogeny/t3/concatenate.py](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/phylogeny/t3/concatenate.py)

</details>


<details>
<summary>4. Use M. bicolor (v3) as an outgroup</summary>

(1) Find orthologues among the 5 species of lewisii complex and bicolor (using v3 annotation)

> sbatch [phylogeny/t4/orthofinder.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/phylogeny/t4/orthofinder.sh)

(2) Align single copy orthologues

> sbatch [phylogeny/t4/muscle.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/phylogeny/t4/muscle.sh)

(3) Filter alignments by maximum percentage of gaps (0.1)

> sbatch [phylogeny/t4/filterAlign.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/phylogeny/t4/filterAlign.sh)

(4) Build tree

> use python/2.7.9 to run [phylogeny/t4/concatenate.py](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/phylogeny/t4/concatenate.py)

</details>

<details>
<summary>5. Use M. bicolor (v3) and M. gutattus as outgroups</summary>

(1) Find orthologues among the 6 species of lewisii complex, bicolor (using v3 annotation) and gutattus

> sbatch [phylogeny/t5/orthofinder.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/phylogeny/t5/orthofinder.sh)

(2) Align single copy orthologues

> sbatch [phylogeny/t5/muscle.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/phylogeny/t5/muscle.sh)

(3) Filter alignments by maximum percentage of gaps (0.1)

> sbatch [phylogeny/t5/filterAlign.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/phylogeny/t5/filterAlign.sh)

(4) Build tree

> use python/2.7.9 to run [phylogeny/t5/concatenate.py](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/phylogeny/t5/concatenate.py)

</details>

<details>
<summary>6. Use Arabidopsis as an outgroup (tree of 6 species)</summary>

(1) Align single copy orthologues from [orthofinder/orthofinder_t3.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/orthofinder/orthofinder_t3.sh)

> sbatch [phylogeny/t6/muscle.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/phylogeny/t6/muscle.sh)

(2) Filter alignments by maximum percentage of gaps (0.1)

> sbatch [phylogeny/t6/filterAlign.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/phylogeny/t6/filterAlign.sh)

(3) Build tree

> use python/2.7.9 to run [phylogeny/t6/concatenate.py](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/phylogeny/t6/concatenate.py)

</details>

#### find positive selections

<details>
<summary>1.  align orthologues of the 5 species (lewisii complex and guttatus)</summary>

> sbatch [selection/muscle.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/selection/muscle.sh)

</details>

<details>
<summary>2.  Run PAML's site models (M0/M1a/M2a)</summary>

  (1) Create tree file
  
> [selection/mimulus.tree](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/selection/mimulus.tree)
    
  (2) Test site model M0 (one dN/dS across sites and branches)

> modify [selection/codeml-M0_template.ctl](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/selection/codeml-M0_template.ctl)

> sbatch [selection/codeml-M0.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/selection/codeml-M0.sh)

  (3) Create a table of dN/dS with single copy orthogroups and orthologues (genes)
 
> sbatch [selection/omega.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/selection/omega.sh)

  (4) Draw dN/dS histogram over all single copy orthogroups
 
> run [selection/omega.r](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/selection/omega.r)

  (5) Test site model M1a (neutral selection) and M2a (positive selection at certain sites)

> modify [selection/codeml-M12_template.ctl](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/selection/codeml-M12_template.ctl)

> sbatch [selection/codeml-M12.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/selection/codeml-M12.sh)

  (6) Chi2 test and filter the positive selection candidates by p < 0.05; then create a table for orthologues/gene candidates

> sbatch [selection/chi2stat-M12.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/selection/chi2stat-M12.sh)

  (7) GO/KEGG enrichment test of the gene candidates 
    
> sbatch [selection/enrichment.r](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/selection/enrichment.r)

</details>

#### de novo gene identification

#### gene family gain and loss

<details>
<summary>1. Run [CAFE](https://github.com/hahnlab/CAFE) for the 4 species in lewisii complex</summary>

  (1) Prepare an ultrametric tree

> run [gene_family/CAFE/transform_tree.py](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/gene_family/CAFE/transform_tree.py)

  (2) Prepare orthologues data

```
 awk '{print "(null)\t"$1"\t"$2"\t"$3"\t"$4"\t"$5}' ~/Mimulus_Genomes/results/orthofinder/t1/Results_Oct23/Orthogroups.GeneCount.csv > unfiltered_cafe_input.txt
```

> As there is no large orthogroup in out dataset, we don't need to delete any line. Just modify the first line for species.

```
 mv unfiltered_cafe_input.txt filtered_cafe_input.txt
```

  (3) Run CAFE pipeline

```
module load CAFE/4.1
module load python/2.7.9
module load R/3.4.3

```

> cafe [gene_family/CAFE/t1/cafe_run1.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/gene_family/CAFE/t1/cafe_run1.sh)

> python [gene_family/CAFE/python_scripts/cafetutorial_report_analysis.py](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/gene_family/CAFE/python_scripts/cafetutorial_report_analysis.py) -i reports/report_run1.cafe -o summary/summary_run1

  (4) Draw diagrams

> python [gene_family/CAFE/python_scripts/cafetutorial_draw_tree.py](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/gene_family/CAFE/python_scripts/cafetutorial_draw_tree.py) -i summary_run1_node.txt -t '(MVBL:1882.5,(Mpar:1255,(LF10:627.5,CE10:627.5)1:627.5)1:627.5)' -d '(MVBL<0>,(Mpar<2>,(LF10<4>,CE10<6>)<5>)<3>)<1>' -o summary_run1_tree_rapid.png -y Rapid

Change 'Rapid' to Expansions/Contractions to draw the other two diagrams

</details>

<details>
<summary>2. Run [CAFE](https://github.com/hahnlab/CAFE) for the 5 Mimulus species, snapdragon, tomato and Arabidopsis</summary>

  (1) Prepare an ultrametric tree from the orthofinder t4 results

> run [gene_family/CAFE/transform_tree.py](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/gene_family/CAFE/transform_tree.py)

  (2) Prepare orthologues data

```
 awk '{print "(null)\t"$1"\t"$2"\t"$3"\t"$4"\t"$5"\t"$6"\t"$7"\t"$8"\t"$9}' ~/Mimulus_Genomes/results/orthofinder/t3/Results_Oct24/Orthogroups.GeneCount.csv > unfiltered_cafe_input.txt
```

> Modify the first line for species (should be identical to the names in tree file)


```
 awk '$3+$4+$5+$6+$7+$8+$9+$10>100{print}' unfiltered_cafe_input.txt > large_cafe_input.txt
 awk '$3+$4+$5+$6+$7+$8+$9+$10<=100{print}' unfiltered_cafe_input.txt > filtered_cafe_input.txt
```

  (3) Run CAFE pipeline

> sbatch [gene_family/CAFE/t2/cafe_run1_submit.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/gene_family/CAFE/t2/cafe_run1_submit.sh)

add the lambda value from run1 to [gene_family/CAFE/t2/cafe_run2_submit.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/gene_family/CAFE/t2/cafe_run2_submit.sh)

> sbatch [gene_family/CAFE/t2/cafe_run2_submit.sh](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/gene_family/CAFE/t2/cafe_run2_submit.sh)

  (4) Draw diagrams

> python [gene_family/CAFE/python_scripts/cafetutorial_draw_tree.py](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/gene_family/CAFE/python_scripts/cafetutorial_draw_tree.py) -i summary_run1_node.txt -t '(Atha:497664,(Slyc:426569,((Mgut:284380,(MvBL:213285,(LF10:142190,(Mpar:71095,CE10:71095):71095):71095):71095):71095,Amaj:355475):71095):71095)' -d '(Atha<0>,(Slyc<2>,((Mgut<4>,(MvBL<6>,(LF10<8>,(Mpar<10>,CE10<12>)<11>)<9>)<7>)<5>,Amaj<14>)<13>)<3>)<1>' -o summary_run1_tree_rapid.png -y Rapid

Change 'Rapid' to Expansions/Contractions to draw the other two diagrams

> python [gene_family/CAFE/python_scripts/cafetutorial_draw_tree.py](https://gitlab.com/qiaoshan.lin/mimulus-genomes/tree/master/scripts/gene_family/CAFE/python_scripts/cafetutorial_draw_tree.py) -i summary_run2_node.txt -t '(Atha:497664,(Slyc:426569,((Mgut:284380,(MvBL:213285,(LF10:142190,(Mpar:71095,CE10:71095):71095):71095):71095):71095,Amaj:355475):71095):71095)' -d '(Atha<0>,(Slyc<2>,((Mgut<4>,(MvBL<6>,(LF10<8>,(Mpar<10>,CE10<12>)<11>)<9>)<7>)<5>,Amaj<14>)<13>)<3>)<1>' -o summary_run2_tree_rapid.png -y Rapid

Change 'Rapid' to Expansions/Contractions to draw the other two diagrams

</details>


